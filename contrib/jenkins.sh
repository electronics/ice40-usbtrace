#!/usr/bin/env bash
# jenkins build helper script for ice40-usbtrace. This is how we build on jenkins.osmocom.org.
#
# environment variables:
# * WITH_MANUALS: build manual PDFs if set to "1"
# * PUBLISH: upload manuals after building if set to "1" (ignored without WITH_MANUALS = "1")

set -e
set -x

TOPDIR=$(pwd)
publish="$1"

# we assume that PATH includes the path to the respective toolchain

# firmware build
FW_DIRS="firmware/boot firmware/main"
for d in $FW_DIRS; do
	echo
	echo "=============== $d FIRMWARE  =============="
	make -C $d clean
	make -C $d
done

# The argument '--publish' is used to trigger publication/upload of firmware
if [ "x$publish" = "x--publish" ]; then

	echo
	echo "=============== UPLOAD FIRMWARE =============="

	cat > "/build/known_hosts" <<EOF
[ftp.osmocom.org]:48 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDDgQ9HntlpWNmh953a2Gc8NysKE4orOatVT1wQkyzhARnfYUerRuwyNr1GqMyBKdSI9amYVBXJIOUFcpV81niA7zQRUs66bpIMkE9/rHxBd81SkorEPOIS84W4vm3SZtuNqa+fADcqe88Hcb0ZdTzjKILuwi19gzrQyME2knHY71EOETe9Yow5RD2hTIpB5ecNxI0LUKDq+Ii8HfBvndPBIr0BWYDugckQ3Bocf+yn/tn2/GZieFEyFpBGF/MnLbAAfUKIdeyFRX7ufaiWWz5yKAfEhtziqdAGZaXNaLG6gkpy3EixOAy6ZXuTAk3b3Y0FUmDjhOHllbPmTOcKMry9
[ftp.osmocom.org]:48 ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBPdWn1kEousXuKsZ+qJEZTt/NSeASxCrUfNDW3LWtH+d8Ust7ZuKp/vuyG+5pe5pwpPOgFu7TjN+0lVjYJVXH54=
[ftp.osmocom.org]:48 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK8iivY70EiR5NiGChV39gRLjNpC8lvu1ZdHtdMw2zuX
EOF
	SSH_COMMAND="ssh -o 'UserKnownHostsFile=/build/known_hosts' -p 48"
	rsync --archive --verbose --compress --rsh "$SSH_COMMAND" $TOPDIR/firmware/main/ice40-usbtrace-fw-*.{bin,elf} binaries@ftp.osmocom.org:web-files/ice40-usbtrace/firmware/all/
	rsync --verbose --copy-links --compress --rsh "$SSH_COMMAND" $TOPDIR/firmware/main/ice40-usbtrace-fw.{bin,elf} binaries@ftp.osmocom.org:web-files/ice40-usbtrace/firmware/latest/
fi

# manuals build + optional publication
if [ "$WITH_MANUALS" = "1" ] && [ -d doc/manuals ]; then
	make -C doc/manuals clean all
	if [ "$PUBLISH" = "1" ]; then
		make -C doc/manuals publish
	fi
fi
