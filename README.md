iCE40-usbtrace - full-speed USB tracer
======================================

The iCE40-usbtrace is an Open Source Hardware + Software passive
tracer/sniffer for USB full-speed communication.

It was built as a lower-cost / complexity alternative to 
[OpenVizsla](https://github.com/OpenVizslaTNG/ov_ftdi) v3.x
whose FPGA became virtually impossible (or ridiculously expensive on the
grey market) during the 2020/2021 chipocalypse.

The underlying idea is relatively simple: passively tap the CMOS
logic-level full-speed USB signals and feed them into a
passive/receive-only variant of the
[no2usb](https://github.com/no2fpga/no2usb.git) FPGA softcore of Sylvain
"tnt" Munaut.

NOTE: This project only supports full-speed USB.  Unlike the OpenVizsla,
it supports neither low-speed nor high-speed!

Hardware
--------

The iCE40-usbtrace hardware is form-factor compatible to the OpenVizsla
v3.2 circuit boards in terms of the USB connector types and locations
as well as the PCB outline and mounting holes.  It's hence a
mechanically compatible drop-in replacement.

See the `hardware` directory for the hardware design files.

[sysmocom](https://sysmocom/) has been producing batches of ice40-usbtrace for some
of its customers; at this point it's not an off-the-shelf-product, but you can contact
sales@sysmocom.de in case you're interested in buying fully assembled units.

Gateware
--------

The gateware (FPGA configuration) is based around the OSHW no2
softcores.  They are referenced as git submodules from this repository.

See the `gateware` directory for more details.

The gateware can be upgraded via USB DFU (Device Firmware Upgrade)


Firmware
--------

There's a picoRISCV softcore inside the ice40 FPGA, whose firmware
is provided in the `firmware` directory.

The firmware can be upgraded via USB DFU (Device Firmware Upgrade)


Host Software
-------------

There's a python library and sample application that can run on a USB
host talking to the iCE40-usbtrace device firmware.  You can implement
your own call-back function for handling the captured USB packets.


Building / Repository Structure
-------------------------------

This git repository makes use of git submodules for the various softcores.

You will need to run `git submodule init` and `git submodule update`
to have the respective cores and build either firmware or gateware.


License
-------

All parts of the project are released under Free / Open Source / Hardware licenses:

* The overall *firmware* is released under GPL-3.0-or-later (portions under MIT
  and LGPL-3.0-or-later, check the individual files).
* The *hardware design* is released under CC-BY-SA 3.0
* The *gateware* is released under CERN-OHL-P-2.0
* the *software* is released under LGPL-2.1-or-later
