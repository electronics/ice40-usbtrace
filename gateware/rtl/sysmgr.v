/*
 * sysmgr.v
 *
 * vim: ts=4 sw=4
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module sysmgr (
	// Inputs
	input  wire clk_in,

	// System
	output wire clk_1x,
	output wire clk_2x,
	output wire rst
);

	// Signals
	// -------

	// Misc
	wire     pll_lock;

	// System reset
	reg [3:0] rst_cnt;
	wire      rst_i;


	// System clock
	// ------------

	// PLL
	SB_PLL40_2F_PAD #(
		.FEEDBACK_PATH       ("SIMPLE"),
		.DIVR                (4'b0000),
		.DIVF                (7'b0111111),
		.DIVQ                (3'b011),
		.FILTER_RANGE        (3'b001),
			// We need that particular "weird" setup to make
			// sure clk_2x rising edge is in the middle
			// of the clk_1x so that the QSPI phy works
			// as intended
		.PLLOUT_SELECT_PORTA ("SHIFTREG_90deg"),
		.PLLOUT_SELECT_PORTB ("GENCLK_HALF")
	) pll_I (
		.PACKAGEPIN    (clk_in),
		.PLLOUTGLOBALA (clk_1x),
		.PLLOUTGLOBALB (clk_2x),
		.RESETB        (1'b1),
		.LOCK          (pll_lock)
	);

	// Reset generation
	always @(posedge clk_1x or negedge pll_lock)
		if (~pll_lock)
			rst_cnt <= 4'h8;
		else if (rst_i)
			rst_cnt <= rst_cnt + 1;

	assign rst_i = rst_cnt[3];

	SB_GB rst_gbuf_I (
		.USER_SIGNAL_TO_GLOBAL_BUFFER (rst_i),
		.GLOBAL_BUFFER_OUTPUT         (rst)
	);

endmodule // sysmgr
