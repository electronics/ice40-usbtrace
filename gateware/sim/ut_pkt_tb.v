/*
 * ut_pkt_tb.v
 *
 * vim: ts=4 sw=4
 *
 * Copyright (C) 2019-2021  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none
`timescale 1ns/100ps

module ut_pkt_tb;

	// Signals
	reg rst = 1;
	reg clk_24m  = 0;	// System clock
	reg clk_48m  = 0;	// USB clock
	reg clk_samp = 0;	// Capture samplerate

	wire [31:0] bus_rdata;
	reg  [31:0] bus_wdata;
	reg         bus_we_csr;

	wire        usb_dp;
	wire        usb_dn;

	wire  [7:0] stream_data;
	wire        stream_stb;

	reg   [7:0] in_file_data;
	reg         in_file_valid;
	reg         in_file_done;

	// Setup recording
	initial begin
		$dumpfile("ut_pkt_tb.vcd");
		$dumpvars(0,ut_pkt_tb);
	end

	// Reset pulse
	initial begin
		# 200 rst = 0;
		# 1000000 $finish;
	end

	// Clocks
	always #20.832 clk_24m  = !clk_24m;
	always #10.416 clk_48m  = !clk_48m;
	always #3.247  clk_samp = !clk_samp;

	// DUT
	ut_pkt dut_I (
		.bus_rdata   (bus_rdata),
		.bus_wdata   (bus_wdata),
		.bus_we_csr  (bus_we_csr),
		.tap_dp      (usb_dp),
		.tap_dn      (usb_dn),
		.stream_data (stream_data),
		.stream_stb  (stream_stb),
		.clk_1x      (clk_24m),
		.clk_2x      (clk_48m),
		.rst         (rst)
	);

	// Init
	initial
	begin
		bus_wdata  <= 32'h00000000;
		bus_we_csr <= 1'b0;

		@(negedge rst);
		@(posedge clk_24m);

		bus_wdata  <= 32'h00000001;
		bus_we_csr <= 1'b1;

		@(posedge clk_24m);

		bus_wdata  <= 32'h00000000;
		bus_we_csr <= 1'b0;
	end

	// Read file
	integer fh_in, rv;

	initial
		fh_in = $fopen("data/capture_usb_raw_short.bin", "rb");

	always @(posedge clk_samp)
	begin
		if (rst) begin
			in_file_data  <= 8'h00;
			in_file_valid <= 1'b0;
			in_file_done  <= 1'b0;
		end else begin
			if (!in_file_done) begin
				rv = $fread(in_file_data, fh_in);
				in_file_valid <= (rv == 1);
				in_file_done  <= (rv != 1);
			end else begin
				in_file_data  <= 8'h00;
				in_file_valid <= 1'b0;
				in_file_done  <= 1'b1;
			end
		end
	end

	// Input
	assign usb_dp = in_file_data[1] & in_file_valid;
	assign usb_dn = in_file_data[0] & in_file_valid;

endmodule // ut_pkt_tb
