<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="3" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="14" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="6" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tMark" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bMark" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="position" color="7" fill="9" visible="yes" active="yes"/>
<layer number="100" name="Release" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Heatsink" color="1" fill="7" visible="no" active="no"/>
<layer number="102" name="tMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="103" name="bMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="104" name="tSchirm" color="7" fill="4" visible="no" active="no"/>
<layer number="105" name="bSchirm" color="7" fill="5" visible="no" active="no"/>
<layer number="106" name="shield" color="11" fill="1" visible="no" active="no"/>
<layer number="107" name="ShieldTop" color="12" fill="1" visible="no" active="no"/>
<layer number="108" name="MeasuresWall" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="SYSONDER" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="bBPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="tStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="bStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="bTPoint" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Änderung" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="Laser Bohrungen" color="9" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="Outline-717" color="10" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="TODO" color="38" fill="1" visible="no" active="no"/>
<layer number="120" name="lotbs" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="lotls" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="BOHRFILM" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="_tanames" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="_banames" color="7" fill="1" visible="no" active="no"/>
<layer number="137" name="_taDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="138" name="_baDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="2" fill="9" visible="no" active="no"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="no" active="no"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="no" active="no"/>
<layer number="148" name="Document_mirrored" color="7" fill="1" visible="no" active="no"/>
<layer number="149" name="mReference" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="References" color="4" fill="1" visible="no" active="no"/>
<layer number="151" name="FaceHelp" color="5" fill="1" visible="no" active="no"/>
<layer number="152" name="FaceFrame" color="6" fill="1" visible="no" active="no"/>
<layer number="153" name="FacePlate" color="2" fill="1" visible="no" active="no"/>
<layer number="154" name="FaceLabel" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="156" name="gesam-Maß" color="1" fill="7" visible="no" active="no"/>
<layer number="157" name="FaceMchng" color="3" fill="1" visible="no" active="no"/>
<layer number="158" name="FaceMMeas" color="3" fill="1" visible="no" active="no"/>
<layer number="159" name="Geh-Bear2" color="1" fill="7" visible="no" active="no"/>
<layer number="160" name="Topologie" color="9" fill="1" visible="no" active="no"/>
<layer number="161" name="tomplace2" color="7" fill="1" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="no" active="no"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="no" active="no"/>
<layer number="191" name="mNets" color="7" fill="1" visible="no" active="no"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="no" active="no"/>
<layer number="193" name="mPins" color="7" fill="1" visible="no" active="no"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="no" active="no"/>
<layer number="195" name="mNames" color="7" fill="1" visible="no" active="no"/>
<layer number="196" name="mValues" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Bauteile" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="bBauteile" color="1" fill="7" visible="no" active="no"/>
<layer number="202" name="value" color="7" fill="1" visible="no" active="no"/>
<layer number="203" name="no" color="7" fill="1" visible="no" active="no"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="tPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="bPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="ausheb_u" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="ausheb_o" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="frontpla" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="no" active="no"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Wuerth_Elektronik_eiCan_Communication_Connectors_v6_20150113">
<description>&lt;BR&gt;Würth Elektronik -- Electronic Interconnect &amp; Electromechanical Solutions&lt;br&gt;&lt;Hr&gt;
&lt;BR&gt;&lt;BR&gt; 
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-405&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com/eagle"&gt;http://www.we-online.com/eagle&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:libraries@we-online.com"&gt;libraries@we-online.com&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither CadSoft nor WE-eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;&lt;BR&gt;&lt;br&gt;
&lt;hr&gt;
Version 6,  January 13th 2015
&lt;HR&gt;
Copyright: Würth Elektronik</description>
<packages>
<package name="61400416121">
<description>WR-COM Horizontal USB Type B</description>
<wire x1="-6" y1="-10.28" x2="6" y2="-10.28" width="0.127" layer="21"/>
<wire x1="6" y1="-10.28" x2="6" y2="5.92" width="0.127" layer="51"/>
<wire x1="6" y1="5.92" x2="-6" y2="5.92" width="0.127" layer="21"/>
<wire x1="-6" y1="5.92" x2="-6" y2="-10.28" width="0.127" layer="51"/>
<pad name="Z2" x="6.02" y="0" drill="2.3"/>
<pad name="Z1" x="-6.02" y="0" drill="2.3"/>
<pad name="4" x="1.25" y="2.71" drill="0.92"/>
<pad name="3" x="-1.25" y="2.71" drill="0.92"/>
<pad name="1" x="1.25" y="4.71" drill="0.92"/>
<pad name="2" x="-1.25" y="4.71" drill="0.92"/>
<text x="-13.4144" y="10.8533" size="1.27" layer="25">&gt;NAME</text>
<text x="-13.4144" y="-12.273" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="4_USB">
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="1.27" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="-7.62" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="4GND" x="-10.16" y="-5.08" length="middle" direction="pas"/>
<pin name="3D+" x="-10.16" y="-2.54" length="middle" direction="pas"/>
<pin name="2D-" x="-10.16" y="0" length="middle" direction="pas"/>
<pin name="1VCC" x="-10.16" y="2.54" length="middle" direction="pas"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-8.89" width="0.254" layer="94" style="shortdash"/>
<wire x1="2.54" y1="-8.89" x2="-1.27" y2="-8.89" width="0.254" layer="94" style="shortdash"/>
<wire x1="-1.27" y1="-8.89" x2="-1.27" y2="-10.16" width="0.254" layer="94" style="shortdash"/>
<pin name="GND@1" x="-7.62" y="-10.16" length="middle"/>
<pin name="GND@2" x="-7.62" y="-12.7" length="middle"/>
<wire x1="-1.27" y1="-10.16" x2="-1.27" y2="-12.7" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="-12.7" x2="-1.27" y2="-12.7" width="0.254" layer="94" style="shortdash"/>
<wire x1="-1.27" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="94" style="shortdash"/>
<text x="1.905" y="-3.4925" size="1.778" layer="94" rot="R90">USB</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="61400416121" prefix="K">
<description>&lt;b&gt;WR-COM Horizontal USB Type B&lt;/b&gt;=&gt;Code : Con_I-O_COM_USB-2.0_B_61400416121
&lt;br&gt;&lt;a href="http://katalog.we-online.de/media/images/eican/Con_I-O_COM_USB-2.0_B_61400416121_pf2.jpg" title="Enlarge picture"&gt;
&lt;img src="http://katalog.we-online.de/media/thumbs2/eican/thb_Con_I-O_COM_USB-2.0_B_61400416121_pf2.jpg" width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="http://katalog.we-online.de/en/em/614_004_161_21"&gt;http://katalog.we-online.de/en/em/614_004_161_21&lt;/a&gt;&lt;p&gt;
Created 2014-07-09, Karrer Zheng&lt;br&gt;
2014 (C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="4_USB" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="61400416121">
<connects>
<connect gate="G$1" pin="1VCC" pad="1"/>
<connect gate="G$1" pin="2D-" pad="2"/>
<connect gate="G$1" pin="3D+" pad="3"/>
<connect gate="G$1" pin="4GND" pad="4"/>
<connect gate="G$1" pin="GND@1" pad="Z1"/>
<connect gate="G$1" pin="GND@2" pad="Z2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="laforge">
<description>&lt;b&gt;PCB Matrix Packages&lt;/b&gt;&lt;p&gt;</description>
<packages>
<package name="USB_A_VERT">
<description>Amphenol UE27-AEX4-X0X</description>
<pad name="P$1" x="-6.57" y="-1.355" drill="2.3" diameter="3.81"/>
<pad name="P$2" x="6.57" y="-1.355" drill="2.3" diameter="3.81"/>
<pad name="1" x="-3.5" y="1.355" drill="0.9"/>
<pad name="3" x="1" y="1.355" drill="0.9"/>
<pad name="2" x="-1" y="1.355" drill="0.9"/>
<pad name="4" x="3.5" y="1.355" drill="0.9"/>
<wire x1="-7.25" y1="2.86" x2="7.25" y2="2.86" width="0.127" layer="21"/>
<wire x1="-7.25" y1="-2.86" x2="7.25" y2="-2.86" width="0.127" layer="21"/>
<wire x1="-7.25" y1="2.86" x2="-7.25" y2="-2.86" width="0.127" layer="21"/>
<wire x1="7.25" y1="-2.86" x2="7.25" y2="2.86" width="0.127" layer="21"/>
<text x="0" y="-4.3" size="1.27" layer="25" align="center">&gt;NAME</text>
</package>
<package name="USB_A_HORIZ">
<description>Würth Elektronik  614 004 160 21</description>
<pad name="P$1" x="-6.57" y="0" drill="2.3" diameter="3.81"/>
<pad name="P$2" x="6.57" y="0" drill="2.3" diameter="3.81"/>
<pad name="1" x="-3.5" y="2.7" drill="0.9"/>
<pad name="3" x="1" y="2.7" drill="0.9"/>
<pad name="2" x="-1" y="2.7" drill="0.9"/>
<pad name="4" x="3.5" y="2.7" drill="0.9"/>
<wire x1="-7.25" y1="3.82" x2="7.25" y2="3.82" width="0.127" layer="21"/>
<wire x1="-7.25" y1="-10.28" x2="7.25" y2="-10.28" width="0.127" layer="21"/>
<wire x1="-7.25" y1="3.82" x2="-7.25" y2="-10.28" width="0.127" layer="21"/>
<wire x1="7.25" y1="-10.28" x2="7.25" y2="3.82" width="0.127" layer="21"/>
<text x="0" y="-3.03" size="1.27" layer="25" align="center">&gt;NAME</text>
</package>
<package name="USB-A-MALE">
<description>USB A male plug THT, Molex Drawing SD-48037-001</description>
<pad name="S1" x="-5.7" y="0" drill="1" diameter="2.2" shape="long" rot="R90"/>
<pad name="S2" x="5.7" y="0" drill="1" diameter="2.2" shape="long" rot="R90"/>
<wire x1="-5.7" y1="1.25" x2="-5.7" y2="-1.25" width="1" layer="46"/>
<wire x1="5.7" y1="-1.25" x2="5.7" y2="1.25" width="1" layer="46"/>
<hole x="-2.25" y="0" drill="0.92"/>
<hole x="2.2" y="0" drill="0.92"/>
<wire x1="-7.62" y1="-2.75" x2="7.62" y2="-2.75" width="0.127" layer="48" style="shortdash"/>
<pad name="1" x="3.5" y="2.1" drill="0.92"/>
<pad name="2" x="1" y="2.1" drill="0.92"/>
<pad name="3" x="-1" y="2.1" drill="0.92"/>
<pad name="4" x="-3.5" y="2.1" drill="0.92"/>
<wire x1="-6" y1="1" x2="-6" y2="-17.8" width="0.127" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-17.8" width="0.127" layer="21"/>
<wire x1="-6" y1="1" x2="6" y2="1" width="0.127" layer="48"/>
<wire x1="-6" y1="-17.8" x2="6" y2="-17.8" width="0.127" layer="48"/>
</package>
<package name="TSOP-6">
<description>TSOP-6 CAS E318G-02 ISSUE V (ON Semiconductor)</description>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.1016" layer="51"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.1016" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.1016" layer="21"/>
<circle x="-1.71" y="-0.5" radius="0.2136" width="0" layer="51"/>
<smd name="1" x="-0.95" y="-1.175" dx="0.6" dy="1.05" layer="1" roundness="50"/>
<smd name="2" x="0" y="-1.125" dx="0.6" dy="0.95" layer="1" roundness="50"/>
<smd name="3" x="0.95" y="-1.125" dx="0.6" dy="0.95" layer="1" roundness="50"/>
<smd name="4" x="0.95" y="1.125" dx="0.6" dy="0.95" layer="1" roundness="50" rot="R180"/>
<smd name="6" x="-0.95" y="1.125" dx="0.6" dy="0.95" layer="1" roundness="50" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="0.825" x2="-0.7" y2="1.5" layer="51"/>
<rectangle x1="0.7" y1="0.825" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.825" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.825" layer="51"/>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.825" layer="51"/>
<smd name="5" x="0" y="1.125" dx="0.6" dy="0.95" layer="1" roundness="50" rot="R180"/>
<rectangle x1="-0.25" y1="0.825" x2="0.25" y2="1.5" layer="51"/>
</package>
<package name="JACK2.5_SJ-2523">
<smd name="1" x="-4.3" y="-3.2741" dx="2.4" dy="2.94" layer="1" rot="R180"/>
<smd name="3" x="-1.6" y="2.9997" dx="2.4" dy="2.94" layer="1" rot="R180"/>
<smd name="2" x="5.4" y="-3.4239" dx="2.4" dy="2.94" layer="1" rot="R180"/>
<hole x="-2.9997" y="0" drill="1"/>
<hole x="2.9997" y="0" drill="1"/>
<wire x1="-6" y1="-2.5" x2="6" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-6" y1="-2.5" x2="-6" y2="-2" width="0.127" layer="21"/>
<wire x1="-6" y1="-2" x2="-6" y2="2" width="0.127" layer="21"/>
<wire x1="-6" y1="2" x2="-6" y2="2.5" width="0.127" layer="21"/>
<wire x1="-6" y1="2.5" x2="6" y2="2.5" width="0.127" layer="21"/>
<wire x1="6" y1="2.5" x2="6" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-6" y1="2" x2="-7.5" y2="2" width="0.127" layer="21"/>
<wire x1="-7.5" y1="2" x2="-7.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-2" x2="-6" y2="-2" width="0.127" layer="21"/>
<text x="0.6" y="3.1" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.7" y="-4.1" size="1.27" layer="25">&gt;NAME</text>
<circle x="-3" y="0" radius="0.647575" width="0.127" layer="41"/>
<circle x="3" y="0" radius="0.647575" width="0.127" layer="41"/>
<circle x="-3" y="0" radius="0.647575" width="0.127" layer="42"/>
<circle x="3" y="0" radius="0.647575" width="0.127" layer="42"/>
</package>
<package name="LUM_KLB13">
<description>Lumberg KLB 13 2.5mm stereo jack, vertical</description>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<pad name="3" x="0" y="-2.9" drill="2.3"/>
<pad name="2" x="-2.7" y="0" drill="2.2"/>
<pad name="1" x="2.7" y="0" drill="2.2"/>
<text x="-2" y="4.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-3" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="1X3_SMD_90">
<description>From Sullins G_C__S__N-M89 data sheet Rev. D (11/12/2009)</description>
<smd name="P$1" x="-2.54" y="0" dx="1.27" dy="3.18" layer="1"/>
<smd name="P$2" x="0" y="0" dx="1.27" dy="3.18" layer="1"/>
<smd name="P$3" x="2.54" y="0" dx="1.27" dy="3.18" layer="1"/>
<text x="-3.81" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="21">1</text>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-3.3" width="0.64" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="-3.3" width="0.64" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-3.3" width="0.64" layer="21"/>
<wire x1="-3.81" y1="-3.3" x2="3.81" y2="-3.3" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-4.57" x2="-2.54" y2="-4.57" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-4.57" x2="0" y2="-4.57" width="0.127" layer="21"/>
<wire x1="0" y1="-4.57" x2="2.54" y2="-4.57" width="0.127" layer="21"/>
<wire x1="2.54" y1="-4.57" x2="3.81" y2="-4.57" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-4.57" x2="-2.54" y2="-10.41" width="0.64" layer="21"/>
<wire x1="0" y1="-4.57" x2="0" y2="-10.41" width="0.64" layer="21"/>
<wire x1="2.54" y1="-4.57" x2="2.54" y2="-10.41" width="0.64" layer="21"/>
<wire x1="-3.81" y1="-3.3" x2="-3.81" y2="-4.57" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.3" x2="3.81" y2="-4.57" width="0.127" layer="21"/>
</package>
<package name="1X03_SMD">
<smd name="1" x="-2.54" y="2.54" dx="1.37" dy="6.45" layer="1"/>
<smd name="2" x="0" y="-2.54" dx="1.37" dy="6.45" layer="1"/>
<wire x1="-3.81" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="4.065" width="0.5" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-4.065" width="0.5" layer="21"/>
<text x="-5.08" y="-1.27" size="1.27" layer="21">1</text>
<text x="-1.27" y="-2.54" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<wire x1="2.54" y1="0" x2="2.54" y2="4.065" width="0.5" layer="21"/>
<smd name="3" x="2.54" y="2.54" dx="1.37" dy="6.45" layer="1"/>
<wire x1="1.27" y1="1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="USB-A">
<pin name="VBUS" x="-7.62" y="5.08" length="middle" direction="pas"/>
<pin name="D_N" x="-7.62" y="2.54" length="middle"/>
<pin name="D_P" x="-7.62" y="0" length="middle"/>
<pin name="GND" x="-7.62" y="-2.54" length="middle" direction="pas"/>
<pin name="SHIELD" x="-7.62" y="-5.08" length="middle" direction="pas"/>
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-2.54" y="-10.16" size="1.27" layer="95">&gt;NAME</text>
<text x="7.62" y="0" size="1.27" layer="97" rot="R90">USB-A</text>
</symbol>
<symbol name="USB_TVS">
<wire x1="-1.016" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.651" y1="-1.27" x2="-1.016" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.27" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.651" y1="-1.27" x2="-1.651" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.27" x2="-0.381" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.381" y1="1.016" x2="-0.381" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="-1.016" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="10.16" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="1A" x="-10.16" y="-2.54" length="short" direction="pas"/>
<pin name="2A" x="-10.16" y="2.54" length="short" direction="pas"/>
<pin name="2B" x="10.16" y="2.54" length="short" direction="pas" rot="R180"/>
<pin name="1B" x="10.16" y="-2.54" length="short" direction="pas" rot="R180"/>
<pin name="VCC" x="0" y="10.16" length="short" direction="pas" rot="R270"/>
<pin name="GND" x="0" y="-10.16" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="JACK2.5STEREO">
<wire x1="0" y1="2.54" x2="1.524" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.524" y1="1.016" x2="2.032" y2="1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="0.508" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-5.08" x2="4.064" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="4.064" y1="-1.524" x2="4.572" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-2.54" x2="2.54" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="3.048" y2="-1.524" width="0.1524" layer="94"/>
<text x="-2.54" y="4.064" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-8.89" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="5.207" y1="-2.667" x2="6.604" y2="2.667" layer="94"/>
<pin name="RING" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="TIP" x="-5.08" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="MIDDLE" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="PINHD3">
<wire x1="-3.81" y1="-5.08" x2="3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="5.08" x2="-3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<text x="-3.81" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="0" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="0" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB-A" prefix="X">
<description>USB A socket THT Amphenol UE27-AEX4-X0X</description>
<gates>
<gate name="G$1" symbol="USB-A" x="-2.54" y="0"/>
</gates>
<devices>
<device name="-VERT" package="USB_A_VERT">
<connects>
<connect gate="G$1" pin="D_N" pad="2"/>
<connect gate="G$1" pin="D_P" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SHIELD" pad="P$1 P$2"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-HORIZ" package="USB_A_HORIZ">
<connects>
<connect gate="G$1" pin="D_N" pad="2"/>
<connect gate="G$1" pin="D_P" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SHIELD" pad="P$1 P$2"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MALE" package="USB-A-MALE">
<connects>
<connect gate="G$1" pin="D_N" pad="2"/>
<connect gate="G$1" pin="D_P" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SHIELD" pad="S1 S2"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="IP4234CZ6" prefix="U">
<description>NXP IP4234CZ6 - Single USB 2.0 ESD protection to IEC 61000-4-2 level 4</description>
<gates>
<gate name="G$1" symbol="USB_TVS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSOP-6">
<connects>
<connect gate="G$1" pin="1A" pad="1"/>
<connect gate="G$1" pin="1B" pad="3"/>
<connect gate="G$1" pin="2A" pad="6"/>
<connect gate="G$1" pin="2B" pad="4"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCC" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="OC_DIGIKEY" value="568-5869-1-ND" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JACK2.5_SJ-2523" prefix="X">
<description>CUI Inc. SJ-2523-SMT</description>
<gates>
<gate name="G$1" symbol="JACK2.5STEREO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JACK2.5_SJ-2523">
<connects>
<connect gate="G$1" pin="MIDDLE" pad="3"/>
<connect gate="G$1" pin="RING" pad="1"/>
<connect gate="G$1" pin="TIP" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="OC_DIGIKEY" value="CP-2523SJCT-ND" constant="no"/>
</technology>
</technologies>
</device>
<device name="-KLB13" package="LUM_KLB13">
<connects>
<connect gate="G$1" pin="MIDDLE" pad="2"/>
<connect gate="G$1" pin="RING" pad="3"/>
<connect gate="G$1" pin="TIP" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD_1X3" prefix="X">
<gates>
<gate name="G$1" symbol="PINHD3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X3_SMD_90">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
<connect gate="G$1" pin="3" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="1X03_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ice40">
<packages>
<package name="QFN48">
<wire x1="3.13" y1="3.55" x2="3.58" y2="3.55" width="0.127" layer="21"/>
<wire x1="3.58" y1="3.55" x2="3.58" y2="3.15" width="0.127" layer="21"/>
<wire x1="-3.52" y1="-3.15" x2="-3.52" y2="-3.55" width="0.127" layer="21"/>
<wire x1="-3.52" y1="-3.55" x2="-3.17" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.18" y1="-3.55" x2="3.58" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.58" y1="-3.55" x2="3.58" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-3.52" y1="3.1" x2="-3.52" y2="3.25" width="0.127" layer="21"/>
<wire x1="-3.52" y1="3.25" x2="-3.22" y2="3.55" width="0.127" layer="21"/>
<wire x1="-3.22" y1="3.55" x2="-3.12" y2="3.55" width="0.127" layer="21"/>
<smd name="1" x="-3.32" y="2.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="2" x="-3.32" y="2.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="3" x="-3.32" y="1.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="4" x="-3.32" y="1.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="5" x="-3.32" y="0.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="6" x="-3.32" y="0.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="7" x="-3.32" y="-0.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="8" x="-3.32" y="-0.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="9" x="-3.32" y="-1.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="10" x="-3.32" y="-1.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="11" x="-3.32" y="-2.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="12" x="-3.32" y="-2.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R90"/>
<smd name="13" x="-2.72" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="14" x="-2.22" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="15" x="-1.72" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="16" x="-1.22" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="17" x="-0.72" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="18" x="-0.22" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="19" x="0.28" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="20" x="0.78" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="21" x="1.28" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="22" x="1.78" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="23" x="2.28" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="24" x="2.78" y="-3.35" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R180"/>
<smd name="25" x="3.38" y="-2.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="26" x="3.38" y="-2.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="27" x="3.38" y="-1.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="28" x="3.38" y="-1.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="29" x="3.38" y="-0.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="30" x="3.38" y="-0.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="31" x="3.38" y="0.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="32" x="3.38" y="0.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="33" x="3.38" y="1.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="34" x="3.38" y="1.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="35" x="3.38" y="2.25" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="36" x="3.38" y="2.75" dx="0.3" dy="0.6" layer="1" roundness="50" rot="R270"/>
<smd name="37" x="2.78" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="38" x="2.28" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="39" x="1.78" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="40" x="1.28" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="41" x="0.78" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="42" x="0.28" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="43" x="-0.22" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="44" x="-0.72" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="45" x="-1.22" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="46" x="-1.72" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="47" x="-2.22" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<smd name="48" x="-2.72" y="3.35" dx="0.3" dy="0.6" layer="1" roundness="50"/>
<text x="-0.01875" y="4.3" size="1.016" layer="25" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0.04" y="-5.35875" size="1.016" layer="27" ratio="15" align="bottom-center">&gt;VALUE</text>
<polygon width="0.127" layer="29">
<vertex x="2.55" y="1.915"/>
<vertex x="2.55" y="-1.915" curve="-90"/>
<vertex x="1.915" y="-2.55"/>
<vertex x="-1.915" y="-2.55" curve="-90"/>
<vertex x="-2.55" y="-1.915"/>
<vertex x="-2.55" y="1.915" curve="-90"/>
<vertex x="-1.915" y="2.55"/>
<vertex x="1.915" y="2.55" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.2225" y="0.9525"/>
<vertex x="-2.2225" y="1.5875" curve="-90"/>
<vertex x="-1.5875" y="2.2225"/>
<vertex x="-0.9525" y="2.2225" curve="-90"/>
<vertex x="-0.3175" y="1.5875"/>
<vertex x="-0.3175" y="0.9525" curve="-90"/>
<vertex x="-0.9525" y="0.3175"/>
<vertex x="-1.5875" y="0.3175" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.3175" y="0.9525"/>
<vertex x="0.3175" y="1.5875" curve="-90"/>
<vertex x="0.9525" y="2.2225"/>
<vertex x="1.5875" y="2.2225" curve="-90"/>
<vertex x="2.2225" y="1.5875"/>
<vertex x="2.2225" y="0.9525" curve="-90"/>
<vertex x="1.5875" y="0.3175"/>
<vertex x="0.9525" y="0.3175" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.2225" y="-1.5875"/>
<vertex x="-2.2225" y="-0.9525" curve="-90"/>
<vertex x="-1.5875" y="-0.3175"/>
<vertex x="-0.9525" y="-0.3175" curve="-90"/>
<vertex x="-0.3175" y="-0.9525"/>
<vertex x="-0.3175" y="-1.5875" curve="-90"/>
<vertex x="-0.9525" y="-2.2225"/>
<vertex x="-1.5875" y="-2.2225" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.3175" y="-1.5875"/>
<vertex x="0.3175" y="-0.9525" curve="-90"/>
<vertex x="0.9525" y="-0.3175"/>
<vertex x="1.5875" y="-0.3175" curve="-90"/>
<vertex x="2.2225" y="-0.9525"/>
<vertex x="2.2225" y="-1.5875" curve="-90"/>
<vertex x="1.5875" y="-2.2225"/>
<vertex x="0.9525" y="-2.2225" curve="-90"/>
</polygon>
<smd name="EXP" x="0" y="0" dx="5.08" dy="5.08" layer="1" roundness="30" cream="no"/>
<wire x1="-3.52" y1="3.1" x2="-3.52" y2="3.25" width="0.127" layer="51"/>
<wire x1="-3.52" y1="3.25" x2="-3.22" y2="3.55" width="0.127" layer="51"/>
<wire x1="-3.22" y1="3.55" x2="-3.12" y2="3.55" width="0.127" layer="51"/>
<circle x="-3.6725" y="3.66875" radius="0.2" width="0.4064" layer="51"/>
<circle x="-3.6725" y="3.66875" radius="0.2" width="0.4064" layer="21"/>
<circle x="-3.6725" y="3.66875" radius="0.2" width="0.4064" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="ICE40UP5K-SG48I">
<description>ICE40UP5K-SG48I</description>
<wire x1="-20.32" y1="60.96" x2="25.4" y2="60.96" width="0.254" layer="94"/>
<wire x1="25.4" y1="60.96" x2="25.4" y2="-60.96" width="0.254" layer="94"/>
<wire x1="25.4" y1="-60.96" x2="-20.32" y2="-60.96" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-60.96" x2="-20.32" y2="60.96" width="0.254" layer="94"/>
<text x="12.7" y="68.58" size="2.54" layer="95" ratio="10" align="center">&gt;NAME</text>
<pin name="IOB_9B" x="30.48" y="38.1" length="middle" rot="R180"/>
<pin name="VCC_PLL" x="-5.08" y="66.04" length="middle" direction="pwr" rot="R270"/>
<pin name="IOT_51A" x="30.48" y="-45.72" length="middle" rot="R180"/>
<pin name="IOT_39A" x="30.48" y="-20.32" length="middle" rot="R180"/>
<pin name="VCC" x="0" y="66.04" length="middle" direction="pwr" rot="R270"/>
<pin name="IOB_3B_G6" x="30.48" y="50.8" length="middle" rot="R180"/>
<pin name="IOT_41A" x="30.48" y="-22.86" length="middle" rot="R180"/>
<pin name="VPP_2V5" x="-10.16" y="66.04" length="middle" direction="pwr" rot="R270"/>
<pin name="IOB_8A" x="30.48" y="40.64" length="middle" rot="R180"/>
<pin name="IOB_6A" x="30.48" y="43.18" length="middle" rot="R180"/>
<pin name="IOB_5B" x="30.48" y="45.72" length="middle" rot="R180"/>
<pin name="IOB_4A" x="30.48" y="48.26" length="middle" rot="R180"/>
<pin name="IOB_2A" x="30.48" y="53.34" length="middle" rot="R180"/>
<pin name="IOB_0A" x="30.48" y="55.88" length="middle" rot="R180"/>
<pin name="IOB_13B" x="30.48" y="25.4" length="middle" rot="R180"/>
<pin name="CDONE" x="30.48" y="33.02" length="middle" direction="out" rot="R180"/>
<pin name="!CRESET_B" x="30.48" y="30.48" length="middle" rot="R180"/>
<pin name="IOT_50B" x="30.48" y="-43.18" length="middle" rot="R180"/>
<pin name="VCC_IO_1" x="-25.4" y="12.7" length="middle" direction="pwr"/>
<pin name="GND" x="-5.08" y="-66.04" length="middle" rot="R90"/>
<pin name="RGB2" x="30.48" y="-55.88" length="middle" rot="R180"/>
<pin name="RGB1" x="30.48" y="-53.34" length="middle" rot="R180"/>
<pin name="RGB0" x="30.48" y="-50.8" length="middle" rot="R180"/>
<pin name="IOT_49A" x="30.48" y="-40.64" length="middle" rot="R180"/>
<pin name="IOT_48B" x="30.48" y="-38.1" length="middle" rot="R180"/>
<pin name="IOT_46B_G0" x="30.48" y="-35.56" length="middle" rot="R180"/>
<pin name="VCC_IO_0" x="-25.4" y="-33.02" length="middle" direction="pwr"/>
<pin name="IOT_45A_G1" x="30.48" y="-33.02" length="middle" rot="R180"/>
<pin name="IOT_44B" x="30.48" y="-30.48" length="middle" rot="R180"/>
<pin name="IOT_43A" x="30.48" y="-27.94" length="middle" rot="R180"/>
<pin name="IOT_42B" x="30.48" y="-25.4" length="middle" rot="R180"/>
<pin name="IOT_38B" x="30.48" y="-17.78" length="middle" rot="R180"/>
<pin name="IOT_37A" x="30.48" y="-15.24" length="middle" rot="R180"/>
<pin name="IOT_36B" x="30.48" y="-12.7" length="middle" rot="R180"/>
<text x="22.86" y="63.5" size="2.54" layer="97" ratio="10" rot="R180" align="center">ICE40UP5K-SG48I</text>
<text x="0" y="45.72" size="2.54" layer="97" ratio="10" rot="R90" align="center">bank 2</text>
<wire x1="3.302" y1="56.642" x2="2.54" y2="55.88" width="0.254" layer="97"/>
<wire x1="2.54" y1="55.88" x2="2.54" y2="38.1" width="0.254" layer="97"/>
<wire x1="2.54" y1="38.1" x2="3.302" y2="37.338" width="0.254" layer="97"/>
<pin name="IOB_16A" x="30.48" y="22.86" length="middle" rot="R180"/>
<pin name="IOB_18A" x="30.48" y="20.32" length="middle" rot="R180"/>
<pin name="IOB_20A" x="30.48" y="17.78" length="middle" rot="R180"/>
<pin name="IOB_22A" x="30.48" y="15.24" length="middle" rot="R180"/>
<pin name="IOB_23B" x="30.48" y="12.7" length="middle" rot="R180"/>
<pin name="IOB_24A" x="30.48" y="10.16" length="middle" rot="R180"/>
<pin name="IOB_25B_G3" x="30.48" y="7.62" length="middle" rot="R180"/>
<pin name="IOB_29B" x="30.48" y="5.08" length="middle" rot="R180"/>
<pin name="IOB_31B" x="30.48" y="2.54" length="middle" rot="R180"/>
<pin name="IOB_32A_SPI_SO" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="IOB_33B_SPI_SI" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="IOB_34A_SPI_SCK" x="30.48" y="-5.08" length="middle" rot="R180"/>
<pin name="IOB_35B_SPI_SS" x="30.48" y="-7.62" length="middle" rot="R180"/>
<text x="0" y="12.7" size="2.54" layer="97" ratio="10" rot="R90" align="center">bank 1</text>
<text x="0" y="-35.56" size="2.54" layer="97" ratio="10" rot="R90" align="center">bank 0</text>
<wire x1="3.302" y1="33.782" x2="2.54" y2="33.02" width="0.254" layer="97"/>
<wire x1="2.54" y1="33.02" x2="2.54" y2="-7.62" width="0.254" layer="97"/>
<wire x1="2.54" y1="-7.62" x2="3.302" y2="-8.382" width="0.254" layer="97"/>
<wire x1="3.302" y1="-11.938" x2="2.54" y2="-12.7" width="0.254" layer="97"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-55.88" width="0.254" layer="97"/>
<wire x1="2.54" y1="-55.88" x2="3.302" y2="-56.642" width="0.254" layer="97"/>
<pin name="VCC_IO_2" x="-25.4" y="45.72" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ICE40UP5K-SG48I" prefix="U">
<description>Field Programmable Gate Array iCE40-UltraPlus, 5280 LUTs, 1.2V</description>
<gates>
<gate name="G$1" symbol="ICE40UP5K-SG48I" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="QFN48">
<connects>
<connect gate="G$1" pin="!CRESET_B" pad="8"/>
<connect gate="G$1" pin="CDONE" pad="7"/>
<connect gate="G$1" pin="GND" pad="EXP"/>
<connect gate="G$1" pin="IOB_0A" pad="46"/>
<connect gate="G$1" pin="IOB_13B" pad="6"/>
<connect gate="G$1" pin="IOB_16A" pad="9"/>
<connect gate="G$1" pin="IOB_18A" pad="10"/>
<connect gate="G$1" pin="IOB_20A" pad="11"/>
<connect gate="G$1" pin="IOB_22A" pad="12"/>
<connect gate="G$1" pin="IOB_23B" pad="21"/>
<connect gate="G$1" pin="IOB_24A" pad="13"/>
<connect gate="G$1" pin="IOB_25B_G3" pad="20"/>
<connect gate="G$1" pin="IOB_29B" pad="19"/>
<connect gate="G$1" pin="IOB_2A" pad="47"/>
<connect gate="G$1" pin="IOB_31B" pad="18"/>
<connect gate="G$1" pin="IOB_32A_SPI_SO" pad="14"/>
<connect gate="G$1" pin="IOB_33B_SPI_SI" pad="17"/>
<connect gate="G$1" pin="IOB_34A_SPI_SCK" pad="15"/>
<connect gate="G$1" pin="IOB_35B_SPI_SS" pad="16"/>
<connect gate="G$1" pin="IOB_3B_G6" pad="44"/>
<connect gate="G$1" pin="IOB_4A" pad="48"/>
<connect gate="G$1" pin="IOB_5B" pad="45"/>
<connect gate="G$1" pin="IOB_6A" pad="2"/>
<connect gate="G$1" pin="IOB_8A" pad="4"/>
<connect gate="G$1" pin="IOB_9B" pad="3"/>
<connect gate="G$1" pin="IOT_36B" pad="25"/>
<connect gate="G$1" pin="IOT_37A" pad="23"/>
<connect gate="G$1" pin="IOT_38B" pad="27"/>
<connect gate="G$1" pin="IOT_39A" pad="26"/>
<connect gate="G$1" pin="IOT_41A" pad="28"/>
<connect gate="G$1" pin="IOT_42B" pad="31"/>
<connect gate="G$1" pin="IOT_43A" pad="32"/>
<connect gate="G$1" pin="IOT_44B" pad="34"/>
<connect gate="G$1" pin="IOT_45A_G1" pad="37"/>
<connect gate="G$1" pin="IOT_46B_G0" pad="35"/>
<connect gate="G$1" pin="IOT_48B" pad="36"/>
<connect gate="G$1" pin="IOT_49A" pad="43"/>
<connect gate="G$1" pin="IOT_50B" pad="38"/>
<connect gate="G$1" pin="IOT_51A" pad="42"/>
<connect gate="G$1" pin="RGB0" pad="39"/>
<connect gate="G$1" pin="RGB1" pad="40"/>
<connect gate="G$1" pin="RGB2" pad="41"/>
<connect gate="G$1" pin="VCC" pad="5 30"/>
<connect gate="G$1" pin="VCC_IO_0" pad="33"/>
<connect gate="G$1" pin="VCC_IO_1" pad="22"/>
<connect gate="G$1" pin="VCC_IO_2" pad="1"/>
<connect gate="G$1" pin="VCC_PLL" pad="29"/>
<connect gate="G$1" pin="VPP_2V5" pad="24"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rc-sysmocom">
<packages>
<package name="0805">
<description>&lt;b&gt;0805 (2012 Metric)&lt;/b&gt;</description>
<wire x1="-1.873" y1="0.883" x2="1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="1.873" y1="-0.883" x2="-1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.873" y1="-0.883" x2="-1.873" y2="0.883" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.873" y1="0.883" x2="1.873" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="1.8" y1="0.9" x2="1.8" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="-1.8" y2="-0.9" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.9" x2="-1.8" y2="0.9" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.9" x2="1.8" y2="0.9" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1" roundness="30"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1" roundness="30"/>
<text x="-1.778" y="1.143" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.778" y="-1.7145" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="1206">
<description>&lt;b&gt;1206 (3216 Metric)&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-2.4" y1="1" x2="2.4" y2="1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1" x2="-2.4" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-2.4" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="30" cream="no"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1" roundness="30" cream="no"/>
<text x="-2.54" y="1.143" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.5875" y="-1.905" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<polygon width="0.3048" layer="31">
<vertex x="-1.984375" y="0.714375"/>
<vertex x="-1.190625" y="0.714375"/>
<vertex x="-0.714375" y="0"/>
<vertex x="-1.190625" y="-0.714375"/>
<vertex x="-1.984375" y="-0.714375"/>
</polygon>
<polygon width="0.3048" layer="31">
<vertex x="0.714375" y="0"/>
<vertex x="1.190625" y="0.714375"/>
<vertex x="1.984375" y="0.714375"/>
<vertex x="1.984375" y="-0.714375"/>
<vertex x="1.190625" y="-0.714375"/>
</polygon>
</package>
<package name="0603">
<description>&lt;b&gt; 0603 (1608 Metric)&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.729" x2="1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.729" x2="1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.729" x2="-1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.729" x2="-1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.2032" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1" roundness="30"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1" roundness="30"/>
<text x="-1.7145" y="1.143" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.7145" y="-1.7145" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402">
<description>&lt;b&gt;0402&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.2032" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.2032" layer="51"/>
<wire x1="-1.346" y1="0.483" x2="1.346" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.346" y1="0.483" x2="1.346" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.346" y1="-0.483" x2="-1.346" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.346" y1="-0.483" x2="-1.346" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="30"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1" roundness="30"/>
<text x="-1.4605" y="1.0795" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.4605" y="-1.5875" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="_0402">
<description>&lt;b&gt; 0402&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.0573" y1="0.5557" x2="1.0573" y2="0.5557" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="0.5557" x2="1.0573" y2="-0.5556" width="0.2032" layer="21"/>
<wire x1="1.0573" y1="-0.5556" x2="-1.0573" y2="-0.5557" width="0.2032" layer="21"/>
<wire x1="-1.0573" y1="-0.5557" x2="-1.0573" y2="0.5557" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.6" dy="0.6" layer="1" roundness="30" cream="no"/>
<smd name="2" x="0.508" y="0" dx="0.6" dy="0.6" layer="1" roundness="30" cream="no"/>
<text x="-1.27" y="0.7939" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-1.3336" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.0794" y1="-0.2381" x2="0.0794" y2="0.2381" layer="35"/>
<rectangle x1="0.25" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.25" y2="0.25" layer="51"/>
<polygon width="0.127" layer="31">
<vertex x="-0.714375" y="0.238125"/>
<vertex x="-0.396875" y="0.238125"/>
<vertex x="-0.238125" y="0"/>
<vertex x="-0.396875" y="-0.238125"/>
<vertex x="-0.714375" y="-0.238125"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.238125" y="0"/>
<vertex x="0.396875" y="0.238125"/>
<vertex x="0.714375" y="0.238125"/>
<vertex x="0.714375" y="-0.238125"/>
<vertex x="0.396875" y="-0.238125"/>
</polygon>
</package>
<package name="_0402MP">
<description>&lt;b&gt;0402 MicroPitch&lt;p&gt;</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="0" y1="0.127" x2="0" y2="-0.127" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1" roundness="30"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1" roundness="30"/>
<text x="-0.9525" y="0.4763" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-0.9525" y="-1.1113" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="_0603">
<description>&lt;b&gt;0603&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.4605" y1="0.635" x2="1.4605" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="0.635" x2="1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.4605" y1="-0.635" x2="-1.4605" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.4605" y1="-0.635" x2="-1.4605" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.9" dy="0.8" layer="1" roundness="30" cream="no"/>
<smd name="2" x="0.762" y="0" dx="0.9" dy="0.8" layer="1" roundness="30" cream="no"/>
<text x="-1.5875" y="0.9525" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.5875" y="-1.4923" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8382" y2="0.4" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<polygon width="0.127" layer="31">
<vertex x="0.635" y="0.3175"/>
<vertex x="1.11125" y="0.3175"/>
<vertex x="1.11125" y="-0.3175"/>
<vertex x="0.635" y="-0.3175"/>
<vertex x="0.396875" y="0"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.11125" y="0.3175"/>
<vertex x="-0.635" y="0.3175"/>
<vertex x="-0.396875" y="0"/>
<vertex x="-0.635" y="-0.3175"/>
<vertex x="-1.11125" y="-0.3175"/>
</polygon>
</package>
<package name="_0603MP">
<description>&lt;b&gt;0603 MicroPitch&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1" roundness="30"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1" roundness="30"/>
<text x="-1.27" y="0.635" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-1.27" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="_0805">
<description>&lt;b&gt;0805&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.585" x2="0.41" y2="0.585" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.585" x2="0.41" y2="-0.585" width="0.1016" layer="51"/>
<wire x1="-1.905" y1="0.889" x2="1.905" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.889" x2="1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-0.889" x2="-1.905" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-0.889" x2="-1.905" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1" roundness="30" cream="no"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1" roundness="30" cream="no"/>
<text x="-1.905" y="1.27" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.9049" y="-1.651" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1.0564" y2="0.65" layer="51"/>
<rectangle x1="-1.0668" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<polygon width="0.127" layer="31">
<vertex x="-1.508125" y="0.555625"/>
<vertex x="-0.873125" y="0.555625"/>
<vertex x="-0.47625" y="0"/>
<vertex x="-0.873125" y="-0.555625"/>
<vertex x="-1.508125" y="-0.555625"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.47625" y="0"/>
<vertex x="0.873125" y="0.555625"/>
<vertex x="1.508125" y="0.555625"/>
<vertex x="1.508125" y="-0.555625"/>
<vertex x="0.873125" y="-0.555625"/>
</polygon>
</package>
<package name="_0805MP">
<description>&lt;b&gt;0805 MicroPitch&lt;/b&gt;</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1" roundness="30"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1" roundness="30"/>
<text x="-1.905" y="0.9525" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.905" y="-1.5875" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
High volt MLC; no solder stop between for higher isolation&lt;br&gt;
Source: http://www.avx.com/docs/catalogs/aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1" roundness="30" cream="no"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1" roundness="30" cream="no"/>
<text x="-2.233" y="1.827" size="1.016" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.016" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
<wire x1="-2.8575" y1="1.27" x2="2.8575" y2="1.27" width="0.06" layer="39"/>
<wire x1="2.8575" y1="1.27" x2="2.8575" y2="-1.27" width="0.06" layer="39"/>
<wire x1="2.8575" y1="-1.27" x2="-2.8575" y2="-1.27" width="0.06" layer="39"/>
<wire x1="-2.8575" y1="-1.27" x2="-2.8575" y2="1.27" width="0.06" layer="39"/>
<polygon width="0.1" layer="29">
<vertex x="-0.9525" y="1.27"/>
<vertex x="0.9525" y="1.27"/>
<vertex x="0.9525" y="-1.27"/>
<vertex x="-0.9525" y="-1.27"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="-2.619375" y="0.9525"/>
<vertex x="-2.619375" y="-0.9525"/>
<vertex x="-1.74625" y="-0.9525"/>
<vertex x="-1.27" y="0"/>
<vertex x="-1.74625" y="0.9525"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="1.27" y="0"/>
<vertex x="1.74625" y="0.9525"/>
<vertex x="2.54" y="0.9525"/>
<vertex x="2.54" y="-0.9525"/>
<vertex x="1.74625" y="-0.9525"/>
</polygon>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt; chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="30" cream="no"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="30" cream="no"/>
<text x="-1.397" y="1.651" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<polygon width="0.2" layer="31">
<vertex x="-2.06375" y="1.190625"/>
<vertex x="-2.06375" y="-1.190625"/>
<vertex x="-1.11125" y="-1.190625"/>
<vertex x="-0.635" y="0"/>
<vertex x="-1.11125" y="1.190625"/>
</polygon>
<polygon width="0.2" layer="31">
<vertex x="0.635" y="0"/>
<vertex x="1.11125" y="1.190625"/>
<vertex x="2.06375" y="1.190625"/>
<vertex x="2.06375" y="-1.190625"/>
<vertex x="1.11125" y="-1.190625"/>
</polygon>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1" roundness="30" cream="no"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1" roundness="30" cream="no"/>
<text x="-2.25" y="2.10875" size="1.016" layer="25" ratio="17">&gt;NAME</text>
<text x="-2.25" y="-3.13375" size="1.016" layer="27" ratio="17">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
<polygon width="0.4064" layer="31">
<vertex x="-2.69875" y="1.5875"/>
<vertex x="-1.905" y="1.5875"/>
<vertex x="-1.27" y="0"/>
<vertex x="-1.905" y="-1.5875"/>
<vertex x="-2.69875" y="-1.5875"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="1.27" y="0"/>
<vertex x="1.905" y="1.5875"/>
<vertex x="2.69875" y="1.5875"/>
<vertex x="2.69875" y="-1.5875"/>
<vertex x="1.905" y="-1.5875"/>
</polygon>
</package>
<package name="2012">
<description>&lt;b&gt;2012&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.2032" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-3.302" y1="1.524" x2="3.302" y2="1.524" width="0.2032" layer="21"/>
<wire x1="3.302" y1="1.524" x2="3.302" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="3.302" y1="-1.524" x2="-3.302" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-3.302" y1="-1.524" x2="-3.302" y2="1.524" width="0.2032" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1" roundness="30" cream="no"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1" roundness="30" cream="no"/>
<text x="-2.54" y="1.8415" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.397" y="-2.159" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<polygon width="0.3048" layer="31">
<vertex x="-2.936875" y="1.190625"/>
<vertex x="-2.936875" y="-1.190625"/>
<vertex x="-1.905" y="-1.190625"/>
<vertex x="-1.42875" y="0"/>
<vertex x="-1.905" y="1.190625"/>
</polygon>
<polygon width="0.3048" layer="31">
<vertex x="1.42875" y="0"/>
<vertex x="1.905" y="1.190625"/>
<vertex x="2.936875" y="1.190625"/>
<vertex x="2.936875" y="-1.190625"/>
<vertex x="1.905" y="-1.190625"/>
</polygon>
</package>
<package name="2512">
<description>&lt;b&gt;2512 (metric 6432)&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="30" cream="no"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1" roundness="30" cream="no"/>
<text x="-3.683" y="2.2225" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-1.3335" y="-2.6035" size="0.508" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
<polygon width="0.3048" layer="31">
<vertex x="-2.54" y="1.42875"/>
<vertex x="-2.06375" y="0"/>
<vertex x="-2.54" y="-1.42875"/>
<vertex x="-3.4925" y="-1.42875"/>
<vertex x="-3.4925" y="1.42875"/>
</polygon>
<polygon width="0.3048" layer="31">
<vertex x="2.06375" y="0"/>
<vertex x="2.54" y="1.42875"/>
<vertex x="3.4925" y="1.42875"/>
<vertex x="3.4925" y="-1.42875"/>
<vertex x="2.54" y="-1.42875"/>
</polygon>
</package>
<package name="1210">
<description>&lt;b&gt;RESISTOR 1210&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="30" cream="no"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="30" cream="no"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
<polygon width="0.3048" layer="31">
<vertex x="-1.905" y="1.11125"/>
<vertex x="-1.905" y="-1.11125"/>
<vertex x="-1.27" y="-1.11125"/>
<vertex x="-0.79375" y="0"/>
<vertex x="-1.27" y="1.11125"/>
</polygon>
<polygon width="0.3048" layer="31">
<vertex x="1.905" y="1.11125"/>
<vertex x="1.905" y="-1.11125"/>
<vertex x="1.27" y="-1.11125"/>
<vertex x="0.79375" y="0"/>
<vertex x="1.27" y="1.11125"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<wire x1="1.778" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<text x="-1.524" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-1.524" y="-3.556" size="1.27" layer="96">&gt;VALUE</text>
<rectangle x1="0.508" y1="-0.254" x2="3.048" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-0.508" y1="-0.254" x2="2.032" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.016" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="-2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<text x="-2.54" y="2.286" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.429" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP_CERAMIC" prefix="C" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;Ceramic Capacitors&lt;/b&gt;&lt;/p&gt;
&lt;b&gt;0402&lt;/b&gt; - 0402 Surface Mount Capacitors
&lt;ul&gt;
&lt;li&gt;16pF 50V 5% [Digikey: 445-4899-2-ND]&lt;/li&gt;
&lt;li&gt;18pF 50V 5% [Digikey: 490-1281-2-ND]&lt;/li&gt;
&lt;li&gt;22pF 50V 5% [Digikey: 490-1283-2-ND]&lt;/li&gt;
&lt;li&gt;68pF 50V 5% [Digikey: 490-1289-2-ND]&lt;/li&gt;
&lt;li&gt;0.1uF 10V 10% [Digikey: 490-1318-2-ND]&lt;/li&gt;
&lt;li&gt;1.0uF 6.3V 10% [Digikey: 490-1320-2-ND]&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;0603&lt;/b&gt; - 0603 Surface Mount Capacitors
&lt;ul&gt;
&lt;li&gt;16 pF 50V 5% [Digikey: 445-5051-2-ND]&lt;/li&gt;
&lt;li&gt;22 pF 50V [Digikey: PCC220ACVTR-ND]&lt;/li&gt;
&lt;li&gt;33 pF 50V 5% [Digikey: 490-1415-1-ND]&lt;/li&gt;
&lt;li&gt;56pF 50V 5% [Digikey: 490-1421-1-ND]&lt;/li&gt;
&lt;li&gt;220pF 50V 5% [Digikey: 445-1285-1-ND]&lt;/li&gt;
&lt;li&gt;680 pF 50V &lt;/li&gt;
&lt;li&gt;2200 pF 50V 5% C0G [Digikey: 445-1297-1-ND]&lt;/li&gt;
&lt;li&gt;5600 pF 100V 5% X7R [Digikey: 478-3711-1-ND]&lt;/li&gt;
&lt;li&gt;0.1 µF 25V 10% [Digikey: PCC2277TR-ND]&lt;/li&gt;
&lt;li&gt;0.22 µF 16V 10% X7R [Digikey: 445-1318-1-ND]&lt;/li&gt;
&lt;li&gt;1.0 µF 25V 10% [Digikey: 445-5146-2-ND]&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;0603&lt;/b&gt; - RF Specific
&lt;ul&gt;
&lt;li&gt;3pF 250V +/-0.1pF RF [Digikey: 712-1347-1-ND]&lt;/li&gt;
&lt;li&gt;18 pF 250V 5%  [Digikey: 478-3505-1-ND or 712-1322-1-ND]&lt;/li&gt;
&lt;li&gt;56 pF 250V 5% C0G RF [Digikey: 490-4867-1-ND]&lt;/li&gt;
&lt;li&gt;68 pF 250V RF [Digikey: 490-4868-1-ND]&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;0805&lt;/b&gt; - 0805 Surface Mount Capacitors
&lt;ul&gt;
&lt;li&gt;220 pF 250V 2% &lt;strong&gt;RF&lt;/strong&gt; Ceramic Capacitor [Digikey: 712-1398-1-ND]&lt;/li&gt;
&lt;li&gt;1000 pF 50V 2% NP0 Ceramic Capacitor [Digikey: 478-3760-1-ND]&lt;/li&gt;
&lt;li&gt;0.1 µF 25V 10% Ceramic Capacitor [Digikey: PCC1828TR-ND]&lt;/li&gt;
&lt;li&gt;1.0 µF 16V 10% Ceramic Capacitor[Digikey: 490-1691-2-ND]&lt;/li&gt;
&lt;li&gt;10.0 µF 10V 10% Ceramic Capacitor[Digikey: 709-1228-1-ND]&lt;/li&gt;
&lt;li&gt;10.0 uF 16V 10% Ceramic Capacitor [Digikey: 478-5165-2-ND]&lt;/li&gt;
&lt;li&gt;47 uF 6.3V 20% Ceramic Capacitor [Digikey: 587-1779-1-ND or 399-5506-1-ND]&lt;/li&gt;
&lt;/ul&gt;
&lt;/ul&gt;&lt;b&gt;1206&lt;/b&gt; - 1206 Surface Mount Capacitors
&lt;ul&gt;
&lt;li&gt;47uF 10V 20% Ceramic Capacitor [Digikey: 490-5528-1-ND or 399-5508-1-ND or 445-6010-1-ND]&lt;/li&gt;
&lt;li&gt;100uF 6.3V -20%, +80% Y5V Ceramic Capacitor (Digikey: 490-4512-1-ND, Mouser: 81-GRM31CF50J107ZE1L)&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;1810&lt;/b&gt; - 1810 Surface Mount Capacitors&lt;br&gt;
High volt MLC; no solder stop between for higher isolation</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="-2.54"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402" package="_0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402MP" package="_0402MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="_0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603MP" package="_0603MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="_0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805MP" package="_0805MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CAP_HV_1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-C1812" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;Resistors&lt;/b&gt;&lt;/p&gt;
&lt;b&gt;0402&lt;/b&gt; - 0402 Surface Mount Package
&lt;ul&gt;
&lt;li&gt;22 Ohm 1% 1/16W [Digikey: 311-22.0LRTR-ND]&lt;/li&gt;
&lt;li&gt;33 Ohm 5% 1/16W&lt;/li&gt;
&lt;li&gt;1.0K 5% 1/16W&lt;/li&gt;
&lt;li&gt;1.5K 5% 1/16W&lt;/li&gt;
&lt;li&gt;2.0K 1% 1/16W&lt;/li&gt;
&lt;li&gt;10.0K 1% 1/16W [Digikey: 311-10.0KLRTR-ND]&lt;/li&gt;
&lt;li&gt;10.0K 5% 1/16W [Digikey: RMCF0402JT10K0TR-ND]&lt;/li&gt;
&lt;li&gt;12.1K 1% 1/16W [Digikey: 311-22.0LRTR-ND]&lt;/li&gt;
&lt;li&gt;100.0K 5% 1/16W&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;0603&lt;/b&gt; - 0603 Surface Mount Package
&lt;ul&gt;&lt;li&gt;1/10 Watt&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;0805&lt;/b&gt; - 0805 Surface Mount Package
&lt;ul&gt;
&lt;li&gt;1/8 Watt&lt;/li&gt;
&lt;/ul&gt;
&lt;b&gt;1206&lt;/b&gt; - 1206 Surface Mount Package&lt;br/&gt;
&lt;br/&gt;
&lt;b&gt;2012&lt;/b&gt; - 2010 Surface Mount Package
&lt;ul&gt;&lt;li&gt;0.11 Ohm 1/2 Watt 1% Resistor - Digikey: RHM.11UCT-ND&lt;/li&gt;&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402" package="_0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0402MP" package="_0402MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="_0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603MP" package="_0603MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805" package="_0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0805MP" package="_0805MP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML1206">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="SML0603">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="spiflash">
<packages>
<package name="SO08COMBINED">
<wire x1="-2.362" y1="-1.803" x2="2.362" y2="-1.803" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-1.803" x2="2.362" y2="1.803" width="0.1524" layer="21"/>
<wire x1="2.362" y1="1.803" x2="-2.362" y2="1.803" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="1.803" x2="-2.362" y2="-1.803" width="0.1524" layer="21"/>
<smd name="1" x="-1.905" y="-3.3512" dx="0.6" dy="3.7" layer="1" roundness="80"/>
<smd name="8" x="-1.905" y="3.2512" dx="0.6" dy="3.5" layer="1" roundness="80"/>
<smd name="2" x="-0.635" y="-3.2512" dx="0.6" dy="3.5" layer="1" roundness="80"/>
<smd name="3" x="0.635" y="-3.2512" dx="0.6" dy="3.5" layer="1" roundness="80"/>
<smd name="7" x="-0.635" y="3.2512" dx="0.6" dy="3.5" layer="1" roundness="80"/>
<smd name="6" x="0.635" y="3.2512" dx="0.6" dy="3.5" layer="1" roundness="80"/>
<smd name="4" x="1.905" y="-3.2512" dx="0.6" dy="3.5" layer="1" roundness="80"/>
<smd name="5" x="1.905" y="3.2512" dx="0.6" dy="3.5" layer="1" roundness="80"/>
<text x="-4.0005" y="-2.667" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.8702" x2="-1.7272" y2="-1.8542" layer="51"/>
<rectangle x1="-0.8128" y1="-2.8702" x2="-0.4572" y2="-1.8542" layer="51"/>
<rectangle x1="0.4572" y1="-2.8702" x2="0.8128" y2="-1.8542" layer="51"/>
<rectangle x1="1.7272" y1="-2.8702" x2="2.0828" y2="-1.8542" layer="51"/>
<rectangle x1="-2.0828" y1="1.8542" x2="-1.7272" y2="2.8702" layer="51"/>
<rectangle x1="-0.8128" y1="1.8542" x2="-0.4572" y2="2.8702" layer="51"/>
<rectangle x1="0.4572" y1="1.8542" x2="0.8128" y2="2.8702" layer="51"/>
<rectangle x1="1.7272" y1="1.8542" x2="2.0828" y2="2.8702" layer="51"/>
<circle x="-2.7559" y="-1.9431" radius="0.1436" width="0.2032" layer="21"/>
<text x="4.8895" y="-2.667" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SPIFLASH_DUAL/QUAD">
<description>(Q)SPI flash</description>
<pin name="!CS" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="!WP(D2)" x="-12.7" y="0" length="short"/>
<pin name="GND" x="-12.7" y="-2.54" length="short" direction="pwr"/>
<pin name="SO(D1)" x="-12.7" y="2.54" length="short"/>
<pin name="VCC" x="17.78" y="5.08" length="short" direction="pwr" rot="R180"/>
<pin name="!HOLD(D3)" x="17.78" y="2.54" length="short" rot="R180"/>
<pin name="SCK" x="17.78" y="0" length="short" direction="in" rot="R180"/>
<pin name="SI(D0)" x="17.78" y="-2.54" length="short" rot="R180"/>
<wire x1="-10.16" y1="-5.715" x2="-10.16" y2="8.255" width="0.254" layer="94"/>
<wire x1="-10.16" y1="8.255" x2="15.24" y2="8.255" width="0.254" layer="94"/>
<wire x1="15.24" y1="8.255" x2="15.24" y2="-5.715" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.715" x2="-10.16" y2="-5.715" width="0.254" layer="94"/>
<text x="-10.16" y="9.144" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-8.636" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AT25SF" prefix="IC">
<description>
</description>
<gates>
<gate name="G$1" symbol="SPIFLASH_DUAL/QUAD" x="0" y="0"/>
</gates>
<devices>
<device name="-COMB" package="SO08COMBINED">
<connects>
<connect gate="G$1" pin="!CS" pad="1"/>
<connect gate="G$1" pin="!HOLD(D3)" pad="7"/>
<connect gate="G$1" pin="!WP(D2)" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="SCK" pad="6"/>
<connect gate="G$1" pin="SI(D0)" pad="5"/>
<connect gate="G$1" pin="SO(D1)" pad="2"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Wuerth_Elektronik_eiSos_V6_20140807">
<description>&lt;BR&gt;Würth Elektronik -- EMC &amp; Inductive Solutions&lt;br&gt;&lt;Hr&gt;

Version 7.0, March 6-th 2014
&lt;hr&gt;
&lt;BR&gt;&lt;BR&gt; 
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-5000&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com"&gt;http://www.we-online.com&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:pm.hotline@we-online.de"&gt;pm.hotline@we-online.de&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither CadSoft nor WE-eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;&lt;BR&gt;&lt;br&gt;

&lt;HR&gt;
Copyright: Würth Elektronik</description>
<packages>
<package name="WE-CNSW_0805">
<description>SMD Common Mode Noise Suppressor WE-CNSW</description>
<wire x1="0.4" y1="0.85" x2="0.4" y2="-0.85" width="0.07" layer="51"/>
<wire x1="-0.4" y1="0.85" x2="-0.4" y2="-0.85" width="0.07" layer="51"/>
<wire x1="-0.6246" y1="-1.2874" x2="-0.6246" y2="1.2635" width="0.127" layer="51"/>
<wire x1="-0.6246" y1="1.2635" x2="0.6251" y2="1.2635" width="0.127" layer="51"/>
<wire x1="0.6251" y1="1.2635" x2="0.6251" y2="-1.2826" width="0.127" layer="51"/>
<wire x1="0.6203" y1="-1.2874" x2="-0.6246" y2="-1.2874" width="0.127" layer="51"/>
<smd name="3" x="0.4" y="0.85" dx="0.9" dy="0.4" layer="1" rot="R270"/>
<smd name="4" x="0.4" y="-0.85" dx="0.9" dy="0.4" layer="1" rot="R270"/>
<smd name="2" x="-0.4" y="0.85" dx="0.9" dy="0.4" layer="1" rot="R270"/>
<smd name="1" x="-0.4" y="-0.85" dx="0.9" dy="0.4" layer="1" rot="R270"/>
<text x="2.6808" y="-1.5288" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.1712" y="-1.5288" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="0.25" y1="-0.1" x2="0.5501" y2="0.1001" layer="51" rot="R270"/>
<rectangle x1="-0.55" y1="-0.1" x2="-0.2499" y2="0.1001" layer="51" rot="R270"/>
<wire x1="-0.8" y1="1.5" x2="-0.8" y2="-1.5" width="0.127" layer="39"/>
<wire x1="-0.8" y1="-1.5" x2="0.8" y2="-1.5" width="0.127" layer="39"/>
<wire x1="0.8" y1="-1.5" x2="0.8" y2="1.5" width="0.127" layer="39"/>
<wire x1="0.8" y1="1.5" x2="-0.8" y2="1.5" width="0.127" layer="39"/>
</package>
<package name="WE-CNSW_1206">
<description>SMD Common Mode Noise Suppressor WE-CNSW</description>
<wire x1="0.5" y1="1.35" x2="0.5" y2="-1.35" width="0.127" layer="51"/>
<wire x1="-0.5" y1="1.35" x2="-0.5" y2="-1.35" width="0.127" layer="51"/>
<wire x1="0.811" y1="-1.8996" x2="0.811" y2="1.9107" width="0.127" layer="51"/>
<wire x1="0.811" y1="1.9107" x2="-0.811" y2="1.9107" width="0.127" layer="51"/>
<wire x1="-0.811" y1="1.9107" x2="-0.811" y2="-1.8996" width="0.127" layer="51"/>
<wire x1="-0.811" y1="-1.8996" x2="0.811" y2="-1.8996" width="0.127" layer="51"/>
<smd name="3" x="0.5" y="1.35" dx="1.1" dy="0.6" layer="1" rot="R270"/>
<smd name="4" x="0.5" y="-1.35" dx="1.1" dy="0.6" layer="1" rot="R270"/>
<smd name="2" x="-0.5" y="1.35" dx="1.1" dy="0.6" layer="1" rot="R270"/>
<smd name="1" x="-0.5" y="-1.35" dx="1.1" dy="0.6" layer="1" rot="R270"/>
<text x="2.27" y="1.5875" size="1.27" layer="27" rot="R270">&gt;VALUE</text>
<text x="-2.905" y="1.5875" size="1.27" layer="25" rot="R270">&gt;NAME</text>
<rectangle x1="0.2" y1="-0.15" x2="0.8001" y2="0.1501" layer="51" rot="R270"/>
<rectangle x1="-0.8" y1="-0.15" x2="-0.1999" y2="0.1501" layer="51" rot="R270"/>
<wire x1="-1" y1="2.1" x2="-1" y2="-2.1" width="0.127" layer="39"/>
<wire x1="-1" y1="-2.1" x2="1" y2="-2.1" width="0.127" layer="39"/>
<wire x1="1" y1="-2.1" x2="1" y2="2.1" width="0.127" layer="39"/>
<wire x1="1" y1="2.1" x2="-1" y2="2.1" width="0.127" layer="39"/>
</package>
<package name="WE-CNSW_0603">
<description>SMD Common Mode Noise Suppressor WE-CNSW</description>
<wire x1="-0.425" y1="-0.8" x2="0.425" y2="-0.8" width="0.05" layer="51"/>
<wire x1="0.425" y1="-0.8" x2="0.425" y2="0.8" width="0.05" layer="51"/>
<wire x1="0.425" y1="0.8" x2="-0.425" y2="0.8" width="0.05" layer="51"/>
<wire x1="-0.425" y1="0.8" x2="-0.425" y2="-0.8" width="0.05" layer="51"/>
<wire x1="-0.2" y1="0.45" x2="-0.2" y2="-0.45" width="0.03" layer="51"/>
<wire x1="0.2" y1="0.45" x2="0.2" y2="-0.45" width="0.03" layer="51"/>
<smd name="1" x="-0.25" y="-0.725" dx="0.25" dy="0.85" layer="1"/>
<smd name="2" x="-0.25" y="0.725" dx="0.25" dy="0.85" layer="1"/>
<smd name="3" x="0.25" y="0.725" dx="0.25" dy="0.85" layer="1"/>
<smd name="4" x="0.25" y="-0.725" dx="0.25" dy="0.85" layer="1"/>
<text x="2.7208" y="-1.8288" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.1112" y="-1.8288" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<rectangle x1="-0.25" y1="-0.1" x2="-0.15" y2="0.1" layer="51"/>
<rectangle x1="0.15" y1="-0.1" x2="0.25" y2="0.1" layer="51"/>
<wire x1="-0.6" y1="1.3" x2="-0.6" y2="-1.3" width="0.127" layer="39"/>
<wire x1="-0.6" y1="-1.3" x2="0.6" y2="-1.3" width="0.127" layer="39"/>
<wire x1="0.6" y1="-1.3" x2="0.6" y2="1.3" width="0.127" layer="39"/>
<wire x1="0.6" y1="1.3" x2="-0.6" y2="1.3" width="0.127" layer="39"/>
</package>
<package name="WE-CNSW_1812">
<description>SMD Common Mode Noise Suppressor WE-CNSW</description>
<wire x1="1.6" y1="-2.25" x2="1.6" y2="2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="2.25" x2="-1.6" y2="2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="2.25" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.9525" y1="-1.2065" x2="-0.9525" y2="1.2065" width="0.127" layer="51"/>
<wire x1="0.9525" y1="-1.2065" x2="0.9525" y2="1.2065" width="0.127" layer="51"/>
<circle x="-0.316" y="-0.8145" radius="0.127" width="0.127" layer="51"/>
<smd name="4" x="1.125" y="-1.825" dx="1.15" dy="1.55" layer="1" roundness="10" rot="R90"/>
<smd name="1" x="-1.125" y="-1.825" dx="1.15" dy="1.55" layer="1" roundness="10" rot="R90"/>
<smd name="2" x="-1.125" y="1.825" dx="1.15" dy="1.55" layer="1" roundness="10" rot="R90"/>
<smd name="3" x="1.125" y="1.825" dx="1.15" dy="1.55" layer="1" roundness="10" rot="R90"/>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.3175" x2="-0.254" y2="0.3175" layer="51" rot="R90"/>
<rectangle x1="0.254" y1="-0.3175" x2="1.651" y2="0.3175" layer="51" rot="R90"/>
<wire x1="-2.1" y1="2.6" x2="2.1" y2="2.6" width="0.127" layer="39"/>
<wire x1="2.1" y1="2.6" x2="2.1" y2="-2.6" width="0.127" layer="39"/>
<wire x1="2.1" y1="-2.6" x2="-2.1" y2="-2.6" width="0.127" layer="39"/>
<wire x1="-2.1" y1="-2.6" x2="-2.1" y2="2.6" width="0.127" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="WE-CNSW">
<pin name="4" x="-10.16" y="3.81" length="middle"/>
<pin name="3" x="10.16" y="3.81" length="middle" rot="R180"/>
<pin name="1" x="-10.16" y="-3.81" length="middle"/>
<pin name="2" x="10.16" y="-3.81" length="middle" rot="R180"/>
<text x="-8.89" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="-8.89" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<circle x="-6.985" y="1.905" radius="0.2" width="1.27" layer="94"/>
<circle x="-6.985" y="-1.905" radius="0.2" width="1.27" layer="94"/>
<wire x1="-5.08" y1="-3.81" x2="-2.54" y2="-3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="-3.81" x2="0" y2="-3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="-3.81" x2="2.54" y2="-3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="-3.81" x2="5.08" y2="-3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="3.81" x2="-5.08" y2="3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="3.81" x2="-2.54" y2="3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="1" x2="5.08" y2="1" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1" x2="5.08" y2="-1" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WE-CNSW" prefix="L" uservalue="yes">
<description>&lt;b&gt;SMD Common Mode Noise Suppressor WE-CNSW&lt;/b&gt;&lt;p&gt;

- High common mode suppression at high frequency&lt;br&gt;
- Small influence for high speed signals through winding symmetry&lt;br&gt;&lt;br&gt;

-- USB 2.0&lt;br&gt;
-- Firewire / IEEE 1394 &lt;br&gt;
-- Power supply systems&lt;br&gt;
-- High Speed Data Lines&lt;br&gt;
-- Common mode data line filter



&lt;br&gt;&lt;a href="http://katalog.we-online.de/media/images/eisos/IndCNSW_pf2.jpg"&gt;
&lt;img src="http://katalog.we-online.de/media/images/eisos/IndCNSW_pf2.jpg" width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="http://katalog.we-online.de/en/pbs/WE-CNSW"&gt;http://katalog.we-online.de/en/pbs/WE-CNSW&lt;/a&gt;&lt;p&gt;


Updated by  Dan Xu 2014-06-30&lt;br&gt;
2014 (C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="WE-CNSW" x="0" y="0"/>
</gates>
<devices>
<device name="_0805" package="WE-CNSW_0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1206" package="WE-CNSW_1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0603" package="WE-CNSW_0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1812" package="WE-CNSW_1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X08">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_LOCK">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_LOCK_LONGPADS">
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<wire x1="16.764" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="19.05" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.9906" x2="18.7706" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="19.05" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.9906" x2="18.7706" y2="1.27" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0" x2="18.796" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.905" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
<rectangle x1="17.4879" y1="-0.2921" x2="18.0721" y2="0.2921" layer="51"/>
</package>
<package name="1X08_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="SCREWTERMINAL-3.5MM-8">
<wire x1="-2.3" y1="3.4" x2="26.76" y2="3.4" width="0.2032" layer="21"/>
<wire x1="26.76" y1="3.4" x2="26.76" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-2.8" x2="26.76" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-3.6" x2="-2.3" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-3.6" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-2.8" x2="-2.3" y2="3.4" width="0.2032" layer="21"/>
<wire x1="26.76" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.35" x2="-2.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-1.35" x2="-2.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.7" y1="-2.35" x2="-2.3" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="26.76" y1="3.15" x2="27.16" y2="3.15" width="0.2032" layer="51"/>
<wire x1="27.16" y1="3.15" x2="27.16" y2="2.15" width="0.2032" layer="51"/>
<wire x1="27.16" y1="2.15" x2="26.76" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="3" x="7" y="0" drill="1.2" diameter="2.032"/>
<pad name="4" x="10.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="5" x="14" y="0" drill="1.2" diameter="2.032"/>
<pad name="6" x="17.5" y="0" drill="1.2" diameter="2.032"/>
<pad name="7" x="21" y="0" drill="1.2" diameter="2.032"/>
<pad name="8" x="24.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD">
<wire x1="1.37" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="-1.25" x2="-19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="1.25" x2="-15.963" y2="1.25" width="0.127" layer="21"/>
<wire x1="-18.63" y1="-1.25" x2="-19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="-1.817" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.377" y1="1.25" x2="-0.703" y2="1.25" width="0.127" layer="21"/>
<wire x1="-9.457" y1="1.25" x2="-5.783" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.537" y1="1.25" x2="-10.863" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.329" y1="-1.25" x2="-6.831" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-8.409" y1="-1.25" x2="-11.911" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-13.489" y1="-1.25" x2="-16.991" y2="-1.25" width="0.127" layer="21"/>
<smd name="7" x="-15.24" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5" x="-10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="8" x="-17.78" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="-12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="-2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<text x="-19.05" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-19.05" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_ALT">
<wire x1="1.37" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="51"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="51"/>
<wire x1="-19.15" y1="1.25" x2="-19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="-1.25" x2="1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="-18.63" y1="1.25" x2="-19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.37" y1="1.25" x2="-1.817" y2="1.25" width="0.127" layer="21"/>
<wire x1="0.85" y1="-1.25" x2="1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-19.15" y1="-1.25" x2="-15.963" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-13.403" y1="1.25" x2="-17.077" y2="1.25" width="0.127" layer="21"/>
<wire x1="-8.323" y1="1.25" x2="-11.997" y2="1.25" width="0.127" layer="21"/>
<wire x1="-3.243" y1="1.25" x2="-6.917" y2="1.25" width="0.127" layer="21"/>
<wire x1="-14.451" y1="-1.25" x2="-10.949" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-9.371" y1="-1.25" x2="-5.869" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-4.291" y1="-1.25" x2="-0.789" y2="-1.25" width="0.127" layer="21"/>
<smd name="7" x="-15.24" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="5" x="-10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="8" x="-17.78" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6" x="-12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4" x="-7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2" x="-2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="-19.05" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-19.05" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_COMBINED">
<wire x1="-1.37" y1="-1.25" x2="-1.37" y2="1.25" width="0.127" layer="21"/>
<wire x1="19.15" y1="1.25" x2="19.15" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-0.85" y1="-1.25" x2="-1.37" y2="-1.25" width="0.127" layer="21"/>
<wire x1="19.15" y1="-1.25" x2="18.503" y2="-1.25" width="0.127" layer="21"/>
<wire x1="18.63" y1="1.25" x2="19.15" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.37" y1="1.25" x2="-0.723" y2="1.25" width="0.127" layer="21"/>
<wire x1="14.537" y1="-1.25" x2="13.403" y2="-1.25" width="0.127" layer="21"/>
<wire x1="0.789" y1="1.25" x2="1.751" y2="1.25" width="0.127" layer="21"/>
<wire x1="3.329" y1="1.25" x2="4.291" y2="1.25" width="0.127" layer="21"/>
<wire x1="5.869" y1="1.25" x2="6.831" y2="1.25" width="0.127" layer="21"/>
<wire x1="8.409" y1="1.25" x2="9.371" y2="1.25" width="0.127" layer="21"/>
<wire x1="10.949" y1="1.25" x2="11.911" y2="1.25" width="0.127" layer="21"/>
<wire x1="13.489" y1="1.25" x2="14.451" y2="1.25" width="0.127" layer="21"/>
<wire x1="16.029" y1="1.25" x2="16.991" y2="1.25" width="0.127" layer="21"/>
<wire x1="17.077" y1="-1.25" x2="15.943" y2="-1.25" width="0.127" layer="21"/>
<wire x1="11.997" y1="-1.25" x2="10.863" y2="-1.25" width="0.127" layer="21"/>
<wire x1="9.457" y1="-1.25" x2="8.323" y2="-1.25" width="0.127" layer="21"/>
<wire x1="6.917" y1="-1.25" x2="5.783" y2="-1.25" width="0.127" layer="21"/>
<wire x1="4.377" y1="-1.25" x2="3.243" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.837" y1="-1.25" x2="0.703" y2="-1.25" width="0.127" layer="21"/>
<wire x1="17.78" y1="1.27" x2="17.78" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="15.24" y1="1.27" x2="15.24" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="12.7" y1="1.27" x2="12.7" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="10.16" y1="1.27" x2="10.16" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="7.62" y1="1.27" x2="7.62" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.4064" layer="1"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.4064" layer="1"/>
<smd name="7@2" x="15.24" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="5@2" x="10.16" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="3@2" x="5.08" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="8@2" x="17.78" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="6@2" x="12.7" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="4@2" x="7.62" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="2@2" x="2.54" y="1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1" x="0" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="3" x="5.08" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="4" x="7.62" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="5" x="10.16" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="6" x="12.7" y="-1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="7" x="15.24" y="1.65" dx="2" dy="1" layer="1" rot="R90"/>
<smd name="8" x="17.78" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<smd name="1@2" x="0" y="-1.65" dx="2" dy="1" layer="1" rot="R270"/>
<text x="0" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="BM08B-SRSS-TB">
<description>JST crimp connector: 1mm pitch, top entry</description>
<wire x1="-5" y1="3.3" x2="5" y2="3.3" width="0.127" layer="51"/>
<wire x1="-5" y1="0.4" x2="-5" y2="3.3" width="0.127" layer="51"/>
<wire x1="5" y1="0.4" x2="5" y2="3.3" width="0.127" layer="51"/>
<wire x1="-5" y1="0.4" x2="5" y2="0.4" width="0.127" layer="51"/>
<wire x1="-4.1" y1="0.35" x2="-5.05" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-5.05" y1="0.35" x2="-5.05" y2="1.35" width="0.2032" layer="21"/>
<wire x1="5.05" y1="0.35" x2="4.15" y2="0.35" width="0.2032" layer="21"/>
<wire x1="5.05" y1="0.35" x2="5.05" y2="1.35" width="0.2032" layer="21"/>
<wire x1="-3.9" y1="3.4" x2="3.9" y2="3.4" width="0.2032" layer="21"/>
<circle x="-4.4" y="-0.35" radius="0.1118" width="0.4064" layer="21"/>
<smd name="1" x="-3.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="2" x="-2.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="3" x="-1.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="4" x="-0.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="5" x="0.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="6" x="1.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="7" x="2.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="8" x="3.5" y="0" dx="0.6" dy="1.55" layer="1" rot="R180"/>
<smd name="P$9" x="4.8" y="2.525" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<smd name="P$10" x="-4.8" y="2.525" dx="1.8" dy="1.2" layer="1" rot="R90"/>
<text x="-3.8" y="2.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-3.8" y="1.3" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_SMD_MALE">
<text x="-1" y="3.321" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1" y="-4.591" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.25" x2="19.05" y2="-1.25" width="0.127" layer="51"/>
<wire x1="19.05" y1="-1.25" x2="19.05" y2="1.25" width="0.127" layer="51"/>
<wire x1="19.05" y1="1.25" x2="-1.27" y2="1.25" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="-0.32" y1="0" x2="0.32" y2="2.75" layer="51"/>
<rectangle x1="4.76" y1="0" x2="5.4" y2="2.75" layer="51"/>
<rectangle x1="9.84" y1="0" x2="10.48" y2="2.75" layer="51"/>
<rectangle x1="2.22" y1="-2.75" x2="2.86" y2="0" layer="51" rot="R180"/>
<rectangle x1="7.3" y1="-2.75" x2="7.94" y2="0" layer="51" rot="R180"/>
<rectangle x1="12.38" y1="-2.75" x2="13.02" y2="0" layer="51" rot="R180"/>
<smd name="1" x="0" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="2" x="2.54" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="3" x="5.08" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="4" x="7.62" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="5" x="10.16" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="6" x="12.7" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="-1.27" y1="1.25" x2="-1.27" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="-1.25" x2="-0.635" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="-1.27" y1="1.25" x2="-0.635" y2="1.25" width="0.1778" layer="21"/>
<wire x1="0.762" y1="1.25" x2="1.778" y2="1.25" width="0.1778" layer="21"/>
<wire x1="3.302" y1="1.25" x2="4.318" y2="1.25" width="0.1778" layer="21"/>
<wire x1="5.842" y1="1.25" x2="6.858" y2="1.25" width="0.1778" layer="21"/>
<wire x1="8.382" y1="1.25" x2="9.398" y2="1.25" width="0.1778" layer="21"/>
<wire x1="10.922" y1="1.25" x2="11.938" y2="1.25" width="0.1778" layer="21"/>
<wire x1="1.778" y1="-1.25" x2="0.762" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="4.318" y1="-1.25" x2="3.302" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="6.858" y1="-1.25" x2="5.842" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="9.398" y1="-1.25" x2="8.382" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="11.938" y1="-1.25" x2="10.922" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="-1.25" x2="19.05" y2="1.25" width="0.1778" layer="21"/>
<circle x="15.24" y="0" radius="0.64" width="0.127" layer="51"/>
<circle x="17.78" y="0" radius="0.64" width="0.127" layer="51"/>
<rectangle x1="14.92" y1="0" x2="15.56" y2="2.75" layer="51"/>
<rectangle x1="17.46" y1="-2.75" x2="18.1" y2="0" layer="51" rot="R180"/>
<smd name="7" x="15.24" y="0" dx="1.02" dy="6" layer="1"/>
<smd name="8" x="17.78" y="0" dx="1.02" dy="6" layer="1"/>
<wire x1="16.002" y1="1.25" x2="17.018" y2="1.25" width="0.1778" layer="21"/>
<wire x1="17.018" y1="-1.25" x2="16.002" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="-1.25" x2="18.415" y2="-1.25" width="0.1778" layer="21"/>
<wire x1="19.05" y1="1.25" x2="18.415" y2="1.25" width="0.1778" layer="21"/>
<wire x1="13.462" y1="1.25" x2="14.478" y2="1.25" width="0.1778" layer="21"/>
<wire x1="14.478" y1="-1.25" x2="13.462" y2="-1.25" width="0.1778" layer="21"/>
</package>
<package name="1X08_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_PIN1_NO_SILK">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
<package name="1X08_ROUND">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="18.415" y2="1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="1.27" x2="19.05" y2="0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="0.635" x2="19.05" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="19.05" y1="-0.635" x2="18.415" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="17.145" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="18.415" y1="-1.27" x2="17.145" y2="-1.27" width="0.2032" layer="21"/>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X08_LOCK_NO_SILK">
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="8" x="17.78" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="17.526" y1="-0.254" x2="18.034" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="M08">
<wire x1="1.27" y1="-10.16" x2="-5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="0" y2="10.16" width="0.6096" layer="94"/>
<text x="-5.08" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="5.08" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M08" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 8&lt;/b&gt;
Standard 8-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).</description>
<gates>
<gate name="G$1" symbol="M08" x="-2.54" y="0"/>
</gates>
<devices>
<device name="SILK_FEMALE_PTH" package="1X08">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08438"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X08_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X08_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X08_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-8" package="SCREWTERMINAL-3.5MM-8">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-STRAIGHT-FEMALE" package="1X08_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10204"/>
</technology>
</technologies>
</device>
<device name="SMD-STRAIGHT-ALT-FEMALE" package="1X08_SMD_ALT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10204"/>
</technology>
</technologies>
</device>
<device name="SMD-COMBO-FEMALE" package="1X08_SMD_COMBINED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10204"/>
</technology>
</technologies>
</device>
<device name="BM08B-SRSS-TB" package="BM08B-SRSS-TB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-MALE" package="1X08_SMD_MALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11292"/>
</technology>
</technologies>
</device>
<device name="NO_SILK_FEMALE_PTH" package="1X08_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08438"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="1X08_PIN1_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="1X08_ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_NO_SILK" package="1X08_LOCK_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="tlv700xx">
<packages>
<package name="SOT23-DBV">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt; DBV (R-PDSO-G5)&lt;p&gt;
Source: http://focus.ti.com/lit/ds/symlink/tps77001.pdf</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="1.328" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="-1.328" y2="0.81" width="0.1524" layer="21"/>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<text x="-1.905" y="2.54" size="1.1" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.1" layer="27" font="vector" ratio="16">&gt;VALUE</text>
<circle x="-1.5875" y="-0.9525" radius="0.1" width="0.127" layer="21"/>
<circle x="-1.5875" y="-0.9525" radius="0.1" width="0.127" layer="21"/>
</package>
<package name="SC70">
<wire x1="1" y1="0.7" x2="1" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="-1" y1="-0.7" x2="-1" y2="0.7" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.7" x2="-1" y2="-0.7" width="0.2032" layer="51"/>
<circle x="-0.65" y="-0.35" radius="0.1414" width="0" layer="51"/>
<smd name="2" x="0" y="-1.2" dx="0.3" dy="0.8" layer="1"/>
<smd name="3" x="0.65" y="-1.2" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="1" x="-0.65" y="-1.2" dx="0.3" dy="0.8" layer="1"/>
<smd name="4" x="0.65" y="1.2" dx="0.3" dy="0.8" layer="1"/>
<smd name="5" x="-0.65" y="1.2" dx="0.3" dy="0.8" layer="1"/>
<text x="-1.675" y="-3.005" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.895" y="-3.005" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.75" y1="0.8" x2="-0.55" y2="1.45" layer="51"/>
<rectangle x1="0.55" y1="0.8" x2="0.75" y2="1.45" layer="51"/>
<rectangle x1="-0.75" y1="-1.45" x2="-0.55" y2="-0.8" layer="51"/>
<rectangle x1="-0.1" y1="-1.45" x2="0.1" y2="-0.8" layer="51"/>
<rectangle x1="0.55" y1="-1.45" x2="0.75" y2="-0.8" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="TLV700XX">
<wire x1="-10.16" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="EN" x="-12.7" y="0" length="short" direction="in"/>
<pin name="GND" x="-2.54" y="-7.62" length="short" direction="sup" rot="R90"/>
<pin name="IN" x="-12.7" y="5.08" length="short" direction="pwr"/>
<pin name="OUT" x="7.62" y="5.08" length="short" direction="pwr" rot="R180"/>
<text x="-10.16" y="8.89" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-0.508" y="8.89" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TLV700*" prefix="IC">
<description>TLV700xx also works for SOT variants of 
TLV702xx and TLV733xx</description>
<gates>
<gate name="G$1" symbol="TLV700XX" x="7.62" y="5.08"/>
</gates>
<devices>
<device name="-SOT" package="SOT23-DBV">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2 4"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name="12">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="13">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="15">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="18">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="19">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="22">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="25">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="28">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="29">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="30">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="33">
<attribute name="OC_DIGIKEY" value="296-27780-1-ND" constant="no"/>
</technology>
<technology name="36">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SC70" package="SC70">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2 4"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name="12">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="13">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="15">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="18">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="19">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="22">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="25">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="28">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="29">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="30">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
<technology name="33">
<attribute name="OC_DIGIKEY" value="296-32413-1-ND" constant="no"/>
</technology>
<technology name="36">
<attribute name="OC_DIGIKEY" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CREE_CLX6F-FKC">
<packages>
<package name="CREE-CLX6F-FKC_PLCC6">
<description>Cree PLCC6 SMD LED; 1mm pitch</description>
<wire x1="-1.75" y1="1.7" x2="1.75" y2="1.7" width="0.127" layer="51"/>
<wire x1="1.75" y1="1.7" x2="1.75" y2="-1.7" width="0.127" layer="51"/>
<wire x1="1.75" y1="-1.7" x2="-1.75" y2="-1.7" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.7" x2="-1.75" y2="1.7" width="0.127" layer="51"/>
<circle x="-1.905" y="1.905" radius="0.15" width="0.127" layer="51"/>
<smd name="2" x="-1.5875" y="0" dx="1.27" dy="0.7" layer="1" roundness="80"/>
<smd name="5" x="1.5875" y="0" dx="1.27" dy="0.7" layer="1" roundness="80"/>
<smd name="1" x="-1.5875" y="1" dx="1.27" dy="0.7" layer="1" roundness="80"/>
<smd name="3" x="-1.5875" y="-1" dx="1.27" dy="0.7" layer="1" roundness="80"/>
<smd name="4" x="1.5875" y="-1" dx="1.27" dy="0.7" layer="1" roundness="80"/>
<smd name="6" x="1.5875" y="1" dx="1.27" dy="0.7" layer="1" roundness="80"/>
<text x="-2.2225" y="2.54" size="1.016" layer="25" font="vector" ratio="14">&gt;NAME</text>
<text x="-2.2225" y="-3.4925" size="1.016" layer="27" font="vector" ratio="14">&gt;VALUE</text>
<wire x1="-1.75" y1="1.7" x2="1.75" y2="1.7" width="0.127" layer="21"/>
<wire x1="1.75" y1="-1.7" x2="-1.75" y2="-1.7" width="0.127" layer="21"/>
<circle x="-1.905" y="1.905" radius="0.15" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.127" layer="51" curve="-90"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.127" layer="51" curve="-90"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.127" layer="51" curve="-90"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.127" layer="51" curve="-90"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.127" layer="51"/>
<rectangle x1="-0.635" y1="0.135" x2="-0.135" y2="0.635" layer="51"/>
<rectangle x1="-0.3175" y1="-0.5" x2="0.1825" y2="0" layer="51"/>
<rectangle x1="0" y1="0.135" x2="0.5" y2="0.635" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LEDRGB_PLCC6">
<description>RBG LED, 6 pads</description>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-10.16" y="6.096" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="RED" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="GRN" x="12.7" y="0" length="short" rot="R180"/>
<pin name="BLUE" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="RGND" x="-12.7" y="-2.54" length="short" direction="pwr"/>
<pin name="GGND" x="-12.7" y="0" length="short" direction="pwr"/>
<pin name="BGND" x="-12.7" y="2.54" length="short" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CREE-CLX6F-FKC" prefix="LED">
<description>&lt;b&gt;Cree CLX6F-FKC&lt;/b&gt; &lt;p&gt;
PLCC6 3 in 1 SMD LED&lt;p&gt;
1mm pitch</description>
<gates>
<gate name="G$1" symbol="LEDRGB_PLCC6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CREE-CLX6F-FKC_PLCC6">
<connects>
<connect gate="G$1" pin="BGND" pad="1"/>
<connect gate="G$1" pin="BLUE" pad="6"/>
<connect gate="G$1" pin="GGND" pad="2"/>
<connect gate="G$1" pin="GRN" pad="5"/>
<connect gate="G$1" pin="RED" pad="4"/>
<connect gate="G$1" pin="RGND" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="74LVC2G34">
<packages>
<package name="DBV_R-PDSO-G6">
<description>DBV_R-PDSO-G6 aka SOT23-6</description>
<wire x1="0" y1="-1.29" x2="0" y2="-1.3" width="0.01" layer="21"/>
<wire x1="1.42" y1="0.8" x2="1.42" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.42" y1="-0.8" x2="-1.42" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.42" y1="-0.8" x2="-1.42" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.42" y1="0.8" x2="1.42" y2="0.8" width="0.127" layer="51"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="6" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<smd name="5" x="0" y="1.3" dx="0.55" dy="1.2" layer="1" roundness="50"/>
<text x="-1.905" y="2.2225" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-1.905" y="-3.4925" size="1.016" layer="27" font="vector" ratio="15">&gt;VALUE</text>
<rectangle x1="-1.11" y1="0.8" x2="-0.78" y2="1.43" layer="51"/>
<rectangle x1="0.79" y1="0.8" x2="1.12" y2="1.42" layer="51"/>
<rectangle x1="-1.11" y1="-1.42" x2="-0.78" y2="-0.8" layer="51"/>
<rectangle x1="-0.16" y1="-1.42" x2="0.17" y2="-0.8" layer="51"/>
<rectangle x1="0.79" y1="-1.42" x2="1.12" y2="-0.8" layer="51"/>
<rectangle x1="-0.16" y1="0.8" x2="0.17" y2="1.42" layer="51"/>
<circle x="-0.9525" y="-0.2675" radius="0.2245" width="0" layer="21"/>
<circle x="-1.905" y="-0.9025" radius="0.2245" width="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="BUFFER">
<wire x1="-2.54" y1="5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.4064" layer="94"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="pad" length="short" direction="in"/>
<pin name="Y" x="10.16" y="0" visible="pad" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="PWRN">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-6.35" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="74LVC2G34" prefix="IC">
<description>Dual Buffer/Driver with open drain output</description>
<gates>
<gate name="-G1" symbol="BUFFER" x="-2.54" y="12.7" swaplevel="1"/>
<gate name="-G2" symbol="BUFFER" x="-2.54" y="0" swaplevel="1"/>
<gate name="-P" symbol="PWRN" x="-20.32" y="0"/>
</gates>
<devices>
<device name="DBV" package="DBV_R-PDSO-G6">
<connects>
<connect gate="-G1" pin="A" pad="1"/>
<connect gate="-G1" pin="Y" pad="6"/>
<connect gate="-G2" pin="A" pad="3"/>
<connect gate="-G2" pin="Y" pad="4"/>
<connect gate="-P" pin="GND" pad="2"/>
<connect gate="-P" pin="VCC" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="TI" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microbuilder">
<description>&lt;h2&gt;&lt;b&gt;microBuilder.eu&lt;/b&gt; Eagle Footprint Library&lt;/h2&gt;

&lt;p&gt;Footprints for common components used in our projects and products.  This is the same library that we use internally, and it is regularly updated.  The newest version can always be found at &lt;b&gt;www.microBuilder.eu&lt;/b&gt;.  If you find this library useful, please feel free to purchase something from our online store. Please also note that all holes are optimised for metric drill bits!&lt;/p&gt;

&lt;h3&gt;Obligatory Warning&lt;/h3&gt;
&lt;p&gt;While it probably goes without saying, there are no guarantees that the footprints or schematic symbols in this library are flawless, and we make no promises of fitness for production, prototyping or any other purpose. These libraries are provided for information puposes only, and are used at your own discretion.  While we make every effort to produce accurate footprints, and many of the items found in this library have be proven in production, we can't make any promises of suitability for a specific purpose. If you do find any errors, though, please feel free to contact us at www.microbuilder.eu to let us know about it so that we can update the library accordingly!&lt;/p&gt;

&lt;h3&gt;License&lt;/h3&gt;
&lt;p&gt;This work is placed in the public domain, and may be freely used for commercial and non-commercial work with the following conditions:&lt;/p&gt;
&lt;p&gt;THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
&lt;/p&gt;</description>
<packages>
<package name="FIDUCIAL-1X2.5">
<circle x="0" y="0" radius="1.1" width="1.2" layer="29"/>
<circle x="0" y="0" radius="1.1" width="1.2" layer="39"/>
<circle x="0" y="0" radius="1.1" width="1.2" layer="41"/>
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
<text x="-0.508" y="-1.016" size="0.4064" layer="25">1mm</text>
</package>
<package name="PASS-ROUND">
<description>&lt;b&gt;FIDUCIAL MARKER&lt;/b&gt;&lt;p&gt;
round, layers 1 + 16 + 21 + 39 + 49</description>
<wire x1="0" y1="0.508" x2="0.508" y2="0" width="1.4224" layer="49" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="-0.508" width="1.4224" layer="49" curve="90" cap="flat"/>
<wire x1="0" y1="0.508" x2="0.508" y2="0" width="1.4224" layer="49" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="-0.508" width="1.4224" layer="49" curve="90" cap="flat"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="49"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="49"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.1524" layer="49"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="49"/>
<wire x1="-2.54" y1="0" x2="-1.524" y2="0" width="0.1524" layer="1"/>
<wire x1="0" y1="2.54" x2="0" y2="1.524" width="0.1524" layer="1"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.524" width="0.1524" layer="1"/>
<wire x1="2.54" y1="0" x2="1.524" y2="0" width="0.1524" layer="1"/>
<wire x1="-2.54" y1="0" x2="-1.524" y2="0" width="0.1524" layer="16"/>
<wire x1="0" y1="2.54" x2="0" y2="1.524" width="0.1524" layer="16"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.524" width="0.1524" layer="16"/>
<wire x1="2.54" y1="0" x2="1.524" y2="0" width="0.1524" layer="16"/>
<wire x1="0.0254" y1="0.5842" x2="0.5842" y2="0.0254" width="1.1176" layer="16" curve="-90" cap="flat"/>
<wire x1="0.0254" y1="0.5842" x2="0.5842" y2="0.0254" width="1.1176" layer="1" curve="-90" cap="flat"/>
<wire x1="-0.5842" y1="-0.0254" x2="-0.0254" y2="-0.5842" width="1.1176" layer="16" curve="90" cap="flat"/>
<wire x1="-0.5842" y1="-0.0254" x2="-0.0254" y2="-0.5842" width="1.1176" layer="1" curve="90" cap="flat"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="49"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="49"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="1"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="16"/>
<circle x="0" y="0" radius="2.54" width="0" layer="29"/>
<circle x="0" y="0" radius="2.54" width="0" layer="30"/>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<wire x1="-0.75" y1="0" x2="0" y2="0.75" width="0.5" layer="29" curve="-90"/>
<wire x1="0" y1="0.75" x2="0.75" y2="0" width="0.5" layer="29" curve="-90"/>
<wire x1="0.75" y1="0" x2="0" y2="-0.75" width="0.5" layer="29" curve="-90"/>
<wire x1="0" y1="-0.75" x2="-0.75" y2="0" width="0.5" layer="29" curve="-90"/>
<wire x1="-0.75" y1="0" x2="0" y2="0.75" width="0.5" layer="41" curve="-90"/>
<wire x1="0" y1="0.75" x2="0.75" y2="0" width="0.5" layer="41" curve="-90"/>
<wire x1="0.75" y1="0" x2="0" y2="-0.75" width="0.5" layer="41" curve="-90"/>
<wire x1="0" y1="-0.75" x2="-0.75" y2="0" width="0.5" layer="41" curve="-90"/>
<wire x1="-0.75" y1="0" x2="0" y2="0.75" width="0.5" layer="39" curve="-90"/>
<wire x1="0" y1="0.75" x2="0.75" y2="0" width="0.5" layer="39" curve="-90"/>
<wire x1="0.75" y1="0" x2="0" y2="-0.75" width="0.5" layer="39" curve="-90"/>
<wire x1="0" y1="-0.75" x2="-0.75" y2="0" width="0.5" layer="39" curve="-90"/>
</package>
</packages>
<symbols>
<symbol name="FIDUCIAL">
<wire x1="-0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL" prefix="FID">
<description>&lt;b&gt;Fiducial Alignment Points&lt;/b&gt;
&lt;p&gt;Various fiducial points for machine vision alignment.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="1X2.5" package="FIDUCIAL-1X2.5">
<technologies>
<technology name="">
<attribute name="BOM" value="EXCLUDE" constant="no"/>
</technology>
</technologies>
</device>
<device name="ROUND" package="PASS-ROUND">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="&quot;&quot;" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="EVQ-Q2">
<wire x1="-3.3" y1="3" x2="3.3" y2="3" width="0.127" layer="21"/>
<wire x1="3.3" y1="3" x2="3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="3.3" y1="-3" x2="-3.3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-3" x2="-3.3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.5033" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1" width="0.127" layer="21"/>
<smd name="B" x="-3.4" y="2" dx="3.2" dy="1.2" layer="1" roundness="80"/>
<smd name="B'" x="3.4" y="2" dx="3.2" dy="1.2" layer="1" roundness="80"/>
<smd name="A'" x="3.4" y="-2" dx="3.2" dy="1.2" layer="1" roundness="80"/>
<smd name="A" x="-3.4" y="-2" dx="3.2" dy="1.2" layer="1" roundness="80"/>
<text x="-3" y="3.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-4.8" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KMR2">
<wire x1="-2.1" y1="1.4" x2="2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.4" x2="-2.1" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="1.4" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="-1.1" y2="-0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-1.1" y1="-0.2" x2="-1.1" y2="0.2" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0.2" x2="-0.5" y2="0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.5" y1="0.8" x2="0.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="0.5" y1="0.8" x2="1.1" y2="0.2" width="0.127" layer="21" curve="-90"/>
<wire x1="1.1" y1="0.2" x2="1.1" y2="-0.2" width="0.127" layer="21"/>
<wire x1="1.1" y1="-0.2" x2="0.5" y2="-0.8" width="0.127" layer="21" curve="-90"/>
<wire x1="0.5" y1="-0.8" x2="-0.5" y2="-0.8" width="0.127" layer="21"/>
<smd name="1" x="2" y="0.8" dx="1" dy="1" layer="1"/>
<smd name="2" x="2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="4" x="-2" y="-0.8" dx="1" dy="1" layer="1"/>
<smd name="3" x="-2" y="0.8" dx="1" dy="1" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPST_TACT" prefix="SW">
<description>SMT 6mm switch, EVQQ2 series
&lt;p&gt;http://www.ladyada.net/library/eagle&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="-EVQQ2" package="EVQ-Q2">
<connects>
<connect gate="G$1" pin="P" pad="A"/>
<connect gate="G$1" pin="P1" pad="A'"/>
<connect gate="G$1" pin="S" pad="B"/>
<connect gate="G$1" pin="S1" pad="B'"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-KMR2" package="KMR2">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
<connect gate="G$1" pin="P1" pad="3"/>
<connect gate="G$1" pin="S" pad="2"/>
<connect gate="G$1" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="holes">
<description>&lt;b&gt;Mounting Holes and Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2,8-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 2.8 mm, round</description>
<wire x1="0" y1="2.921" x2="0" y2="2.667" width="0.0508" layer="21"/>
<wire x1="0" y1="-2.667" x2="0" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="0" x2="0" y2="-1.778" width="2.286" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="1.778" x2="1.778" y2="0" width="2.286" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.635" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="2.921" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="39"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="40"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="43"/>
<circle x="0" y="0" radius="1.5" width="0.2032" layer="21"/>
<pad name="B2,8" x="0" y="0" drill="2.8" diameter="5.334"/>
</package>
<package name="3,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.0 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="39"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.6" width="0.2032" layer="21"/>
<pad name="B3,0" x="0" y="0" drill="3" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,0</text>
</package>
<package name="3,2-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.2 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.1524" layer="21"/>
<pad name="B3,2" x="0" y="0" drill="3.2" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,2</text>
</package>
<package name="3,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.3 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="B3,3" x="0" y="0" drill="3.3" diameter="5.842"/>
</package>
<package name="3,6-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.6 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.9" width="0.2032" layer="21"/>
<pad name="B3,6" x="0" y="0" drill="3.6" diameter="5.842"/>
</package>
<package name="4,1-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.1 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="5.08" width="2" layer="43"/>
<circle x="0" y="0" radius="2.15" width="0.2032" layer="21"/>
<pad name="B4,1" x="0" y="0" drill="4.1" diameter="8"/>
</package>
<package name="4,3-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.3 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.25" width="0.1524" layer="21"/>
<pad name="B4,3" x="0" y="0" drill="4.3" diameter="9"/>
</package>
<package name="4,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.35" width="0.1524" layer="21"/>
<pad name="B4,5" x="0" y="0" drill="4.5" diameter="9"/>
</package>
<package name="5,0-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.0 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.6" width="0.1524" layer="21"/>
<pad name="B5" x="0" y="0" drill="5" diameter="9"/>
</package>
<package name="5,5-PAD">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.85" width="0.1524" layer="21"/>
<pad name="B5,5" x="0" y="0" drill="5.5" diameter="9"/>
</package>
<package name="3,3-PAD-7">
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="4.2" width="1.27" layer="39"/>
<circle x="0" y="0" radius="4.2" width="1.27" layer="40"/>
<circle x="0" y="0" radius="4" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="B3,3-7" x="0" y="0" drill="3.3" diameter="7"/>
</package>
</packages>
<symbols>
<symbol name="MOUNT-PAD">
<wire x1="0.254" y1="2.032" x2="2.032" y2="0.254" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="0.254" x2="-0.254" y2="2.032" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="-0.254" x2="-0.254" y2="-2.032" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<wire x1="0.254" y1="-2.032" x2="2.032" y2="-0.254" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<circle x="0" y="0" radius="1.524" width="0.0508" layer="94"/>
<text x="2.794" y="0.5842" size="1.778" layer="95">&gt;NAME</text>
<text x="2.794" y="-2.4638" size="1.778" layer="96">&gt;VALUE</text>
<pin name="MOUNT" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOUNT-PAD-ROUND" prefix="H">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt;, round</description>
<gates>
<gate name="G$1" symbol="MOUNT-PAD" x="0" y="0"/>
</gates>
<devices>
<device name="2.8" package="2,8-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B2,8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.0" package="3,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.2" package="3,2-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.3" package="3,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.6" package="3,6-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.1" package="4,1-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.3" package="4,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4.5" package="4,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.0" package="5,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.5" package="5,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5,5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.3-7" package="3,3-PAD-7">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,3-7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="XO-2x2.5mm">
<packages>
<package name="CRYSTAL-SMD-2X2.5MM">
<description>&lt;h3&gt;2mm X 2.5mm XO&lt;/h3&gt;</description>
<text x="0.008" y="1.776" size="1.016" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0.008" y="-1.672" size="1.016" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<smd name="1" x="-0.854" y="-0.71" dx="1.1" dy="1" layer="1" roundness="50"/>
<smd name="2" x="0.846" y="-0.71" dx="1.1" dy="1" layer="1" roundness="50"/>
<smd name="3" x="0.846" y="0.69" dx="1.1" dy="1" layer="1" roundness="50"/>
<smd name="4" x="-0.854" y="0.69" dx="1.1" dy="1" layer="1" roundness="50"/>
<circle x="-1.71" y="-1.2225" radius="0.161553125" width="0" layer="21"/>
<wire x1="-1.25" y1="1" x2="1.25" y2="1" width="0.127" layer="21"/>
<wire x1="1.25" y1="1" x2="1.25" y2="-1" width="0.127" layer="21"/>
<wire x1="1.25" y1="-1" x2="-1.25" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.25" y1="-1" x2="-1.25" y2="1" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1" x2="1.25" y2="1" width="0.127" layer="51"/>
<wire x1="1.25" y1="-1" x2="-1.25" y2="-1" width="0.127" layer="51"/>
<wire x1="1.25" y1="1" x2="1.25" y2="-1" width="0.127" layer="51"/>
<wire x1="-1.25" y1="-1" x2="-1.25" y2="1" width="0.127" layer="51"/>
<circle x="-1.71" y="-1.2225" radius="0.161553125" width="0" layer="51"/>
<circle x="-1.71" y="-1.2225" radius="0.161553125" width="0" layer="25"/>
</package>
</packages>
<symbols>
<symbol name="OSCILLATOR-4POL">
<wire x1="-1.651" y1="1.524" x2="-1.651" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.651" y1="-1.524" x2="-0.889" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-1.524" x2="-0.889" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.889" y1="1.524" x2="-1.651" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.254" y1="1.778" x2="-0.254" y2="0" width="0.254" layer="94"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-2.286" y1="1.778" x2="-2.286" y2="0" width="0.254" layer="94"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.778" width="0.254" layer="94"/>
<text x="5.08" y="6.096" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VCC" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="out" rot="R180"/>
<pin name="GND" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="-3.81" y1="0" x2="-2.286" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-0.254" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<pin name="EN" x="7.62" y="2.54" visible="off" length="point" direction="in" rot="R270"/>
<wire x1="2.54" y1="1.27" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="5.08" y2="2.54" width="0.19811875" layer="94"/>
<wire x1="5.08" y1="2.54" x2="7.62" y2="2.54" width="0.19811875" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="OSCILLATOR-4" prefix="IC" uservalue="yes">
<description>&lt;h3&gt;4pin-oscillator, 2.5 x 2mm&lt;/h3&gt;</description>
<gates>
<gate name="G$1" symbol="OSCILLATOR-4POL" x="0" y="0"/>
</gates>
<devices>
<device name="-SMD-2X2.5MM" package="CRYSTAL-SMD-2X2.5MM">
<connects>
<connect gate="G$1" pin="EN" pad="1"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUT" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.3" drill="0">
</class>
<class number="1" name="usb" width="0.7" drill="0">
<clearance class="1" value="0.15"/>
</class>
<class number="2" name="fgt" width="0.6" drill="0.2">
<clearance class="0" value="0.2"/>
<clearance class="1" value="0.2"/>
<clearance class="2" value="0.2"/>
</class>
<class number="3" name="supply" width="0.6" drill="0">
<clearance class="0" value="0.3"/>
<clearance class="1" value="0.3"/>
<clearance class="2" value="0.3"/>
<clearance class="3" value="0.3"/>
</class>
</classes>
<parts>
<part name="J2" library="Wuerth_Elektronik_eiCan_Communication_Connectors_v6_20150113" deviceset="61400416121" device="">
<attribute name="CLASS" value="CONNECTOR"/>
<attribute name="DESCRIPTION" value="CONN RCPT USB2.0 TYPEB 4POS R/A"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/wurth-electronics-inc/61400416121/732-2108-ND"/>
<attribute name="MANUFACTURER" value="Würth Elektronik"/>
<attribute name="MANUFACTURERPARTNUMBER" value="61400416121"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="732-2108-ND"/>
</part>
<part name="J1" library="Wuerth_Elektronik_eiCan_Communication_Connectors_v6_20150113" deviceset="61400416121" device="">
<attribute name="CLASS" value="CONNECTOR"/>
<attribute name="DESCRIPTION" value="CONN RCPT USB2.0 TYPEB 4POS R/A"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/wurth-electronics-inc/61400416121/732-2108-ND"/>
<attribute name="MANUFACTURER" value="Würth Elektronik"/>
<attribute name="MANUFACTURERPARTNUMBER" value="61400416121"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="732-2108-ND"/>
</part>
<part name="J3" library="laforge" deviceset="USB-A" device="-HORIZ">
<attribute name="CLASS" value="CONNECTOR"/>
<attribute name="DESCRIPTION" value="CONN RCPT USB2.0 TYPEA 4POS R/A"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/wurth-electronics-inc/61400416021/732-2106-ND"/>
<attribute name="MANUFACTURER" value="Würth Elektronik"/>
<attribute name="MANUFACTURERPARTNUMBER" value="61400416021"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="732-2106-ND"/>
</part>
<part name="FRAME1" library="frames" deviceset="A3L-LOC" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="U1" library="ice40" deviceset="ICE40UP5K-SG48I" device="">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="IC FPGA 39 I/O 48QFN"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/lattice-semiconductor-corporation/ICE40UP5K-SG48I/220-2212-1-ND/"/>
<attribute name="MANUFACTURER" value="Lattice Semiconductor Corporation"/>
<attribute name="MANUFACTURERPARTNUMBER" value="ICE40UP5K-SG48I"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="220-2212-1-ND"/>
</part>
<part name="C3" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="C2" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="C1" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="IC1" library="spiflash" deviceset="AT25SF" device="-COMB" value="W25Q80DVSNIG">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="IC FLASH 8M SPI 104MHZ 8SOIC"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/winbond-electronics/W25Q80DVSNIG/4878496"/>
<attribute name="MANUFACTURER" value="Winbond Electronics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="W25Q80DVSNIG"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="W25Q80DVSNIG-ND"/>
</part>
<part name="C12" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="R12" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="10k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 10K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT10K0/RMCF0603JT10K0CT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT10K0"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT10K0CT-ND"/>
</part>
<part name="R5" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="10k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 10K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT10K0/RMCF0603JT10K0CT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT10K0"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT10K0CT-ND"/>
</part>
<part name="U2" library="laforge" deviceset="IP4234CZ6" device="">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="TVS DIODE 5.5V 6TSOP"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/nexperia-usa-inc/IP4234CZ6125/1727-4717-1-ND/2531156"/>
<attribute name="MANUFACTURER" value="Nexperia USA Inc."/>
<attribute name="MANUFACTURERPARTNUMBER" value="IP4234CZ6,125"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1727-4717-1-ND"/>
</part>
<part name="R17" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 100K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-07100KL/311-100KGRCT-ND/729645"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-07100KL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-100KGRCT-ND"/>
</part>
<part name="R16" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 100K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-07100KL/311-100KGRCT-ND/729645"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-07100KL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-100KGRCT-ND"/>
</part>
<part name="C19" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0402" value="10n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 10000PF 25V X7R 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL05B103KA5NNNC/1276-1057-1-ND/3889143"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL05B103KA5NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1057-1-ND"/>
</part>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="L1" library="Wuerth_Elektronik_eiSos_V6_20140807" deviceset="WE-CNSW" device="_0805" value="744231091">
<attribute name="CLASS" value="INDUCTOR"/>
<attribute name="DESCRIPTION" value="CMC 370MA 2LN 90 OHM SMD"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/wurth-electronics-inc/744231091/732-3198-1-ND"/>
<attribute name="MANUFACTURER" value="Wurth Electronics Inc."/>
<attribute name="MANUFACTURERPARTNUMBER" value="744231091"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="732-3198-1-ND"/>
</part>
<part name="R15" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="1k5">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 1.5K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT1K50/RMCF0603JT1K50CT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT1K50"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT1K50CT-ND"/>
</part>
<part name="C8" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="1u">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 1UF 10V X5R 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A105KP8NNNC/1276-1182-1-ND/3889268"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10A105KP8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1182-1-ND"/>
</part>
<part name="C7" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="C6" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="C5" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="R1" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100R">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 100 OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT100R/RMCF0603JT100RCT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT100R"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT100RCT-ND"/>
</part>
<part name="C4" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="C16" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="1u">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 1UF 10V X5R 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A105KP8NNNC/1276-1182-1-ND/3889268"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10A105KP8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1182-1-ND"/>
</part>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="R21" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="10k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 10K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT10K0/RMCF0603JT10K0CT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT10K0"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT10K0CT-ND"/>
</part>
<part name="C20" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="C18" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="1206" value="22u">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 22UF 10V 10% X5R 1206"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1287-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL31A226KPHNNNE"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1287-1-ND"/>
</part>
<part name="C15" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="1206" value="22u">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 22UF 10V 10% X5R 1206"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1287-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL31A226KPHNNNE"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1287-1-ND"/>
</part>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="C11" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="1u">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 1UF 10V X5R 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A105KP8NNNC/1276-1182-1-ND/3889268"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10A105KP8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1182-1-ND"/>
</part>
<part name="C14" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="1u">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 1UF 10V X5R 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL10A105KP8NNNC/1276-1182-1-ND/3889268"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10A105KP8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1182-1-ND"/>
</part>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="R18" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="2.2k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 2.2K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT2K20/RMCF0603JT2K20CT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT2K20"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT2K20CT-ND"/>
</part>
<part name="CDONE" library="led" deviceset="LED" device="CHIPLED_0603" value="BLUE">
<attribute name="CLASS" value="LED"/>
<attribute name="DESCRIPTION" value="LED BLUE CLEAR CHIP SMD"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/liteon/LTST-C193TBKT-5A/2053656"/>
<attribute name="MANUFACTURER" value="Lite-On Inc."/>
<attribute name="MANUFACTURERPARTNUMBER" value="LTST-C193TBKT-5A"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="160-1827-1-ND"/>
</part>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="JP2" library="SparkFun-Connectors" deviceset="M08" device="LOCK">
<attribute name="CLASS" value="CONNECTOR\JUMPER"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="FALSE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="X2" library="laforge" deviceset="JACK2.5_SJ-2523" device="">
<attribute name="CLASS" value="CONNECTOR"/>
<attribute name="DESCRIPTION" value="CONN JACK STEREO 2.5MM SMD R/A"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/cui-inc/SJ-2523-SMT-TR/CP-2523SJCT-ND/669702"/>
<attribute name="MANUFACTURER" value="CUI Inc."/>
<attribute name="MANUFACTURERPARTNUMBER" value="SJ-2523-SMT-TR"/>
<attribute name="POPULATED" value="FALSE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="CP-2523SJCT-ND"/>
</part>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="U3" library="laforge" deviceset="IP4234CZ6" device="">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="TVS DIODE 5.5V 6TSOP"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/nexperia-usa-inc/IP4234CZ6125/1727-4717-1-ND/2531156"/>
<attribute name="MANUFACTURER" value="Nexperia USA Inc."/>
<attribute name="MANUFACTURERPARTNUMBER" value="IP4234CZ6,125"/>
<attribute name="POPULATED" value="FALSE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1727-4717-1-ND"/>
</part>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="IC5" library="tlv700xx" deviceset="TLV700*" device="-SOT" technology="33" value="NCP114BSN330T1G">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="IC REG LINEAR 3.3V 300MA 5TSOP"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/onsemi/NCP114BSN330T1G/6560606"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="NCP114BSN330T1G"/>
<attribute name="OC_DIGIKEY" value=""/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="NCP114BSN330T1GOSCT-ND"/>
</part>
<part name="R13" library="rc-sysmocom" deviceset="RESISTOR" device="_0402" value="33">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 33 OHM 1%% 1/16W 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0402FR-0733RL/311-33-0LRCT-ND/729540"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0402FR-0733RL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-33.0LRCT-ND"/>
</part>
<part name="R14" library="rc-sysmocom" deviceset="RESISTOR" device="_0402" value="33">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 33 OHM 1%% 1/16W 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0402FR-0733RL/311-33-0LRCT-ND/729540"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0402FR-0733RL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-33.0LRCT-ND"/>
</part>
<part name="LED1" library="CREE_CLX6F-FKC" deviceset="CREE-CLX6F-FKC" device="">
<attribute name="CLASS" value="LED"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/creeled-inc/CLX6F-FKC-CKNNQDGBB7A363/6355679"/>
<attribute name="MANUFACTURER" value="CreeLED, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CLX6F-FKC-CKNNQDGBB7A363"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="CLX6F-FKC-CKNNQDGBB7A363DKR-ND"/>
</part>
<part name="IC3" library="74LVC2G34" deviceset="74LVC2G34" device="DBV">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="IC BUF NON-INVERT 5.5V SOT23-6"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/texas-instruments/SN74LVC2G34DBVT/2261911"/>
<attribute name="MANUFACTURERPARTNUMBER" value="SN74LVC2G34DBVT"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="296-32325-6-ND"/>
</part>
<part name="C17" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="R2" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 100K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-07100KL/311-100KGRCT-ND/729645"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-07100KL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-100KGRCT-ND"/>
</part>
<part name="R3" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 100K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-07100KL/311-100KGRCT-ND/729645"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-07100KL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-100KGRCT-ND"/>
</part>
<part name="C9" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0402" value="10n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 10000PF 25V X7R 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL05B103KA5NNNC/1276-1057-1-ND/3889143"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL05B103KA5NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1057-1-ND"/>
</part>
<part name="R8" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 100K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-07100KL/311-100KGRCT-ND/729645"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-07100KL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-100KGRCT-ND"/>
</part>
<part name="R9" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 100K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-07100KL/311-100KGRCT-ND/729645"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-07100KL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-100KGRCT-ND"/>
</part>
<part name="C10" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0402" value="10n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 10000PF 25V X7R 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL05B103KA5NNNC/1276-1057-1-ND/3889143"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL05B103KA5NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1057-1-ND"/>
</part>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="470R">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 470 OHM 1% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/stackpole-electronics-inc/RMCF0603FT470R/1761144"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603FT470R"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603FT470RCT-ND"/>
</part>
<part name="R7" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="470R">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 470 OHM 1% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/stackpole-electronics-inc/RMCF0603FT470R/1761144"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603FT470R"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603FT470RCT-ND"/>
</part>
<part name="R10" library="rc-sysmocom" deviceset="RESISTOR" device="_0402" value="33">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 33 OHM 1%% 1/16W 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0402FR-0733RL/311-33-0LRCT-ND/729540"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0402FR-0733RL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-33.0LRCT-ND"/>
</part>
<part name="R11" library="rc-sysmocom" deviceset="RESISTOR" device="_0402" value="33">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 33 OHM 1%% 1/16W 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0402FR-0733RL/311-33-0LRCT-ND/729540"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0402FR-0733RL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-33.0LRCT-ND"/>
</part>
<part name="IC2" library="spiflash" deviceset="AT25SF" device="-COMB" value="APS6404L-3SQR-SN">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="PSRAM 64Mb QSPI (x1,x4) 133/84MHz SOIC8"/>
<attribute name="LINK" value="https://www.mouser.de/ProductDetail/AP-Memory/APS6404L-3SQR-SN?qs=IS%252B4QmGtzzqsn3S5xo%2FEEg%3D%3D"/>
<attribute name="MANUFACTURER" value="AP Memory"/>
<attribute name="MANUFACTURERPARTNUMBER" value="APS6404L-3SQR-SN"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="MOUSER"/>
<attribute name="SOURCEPARTNUMBER" value="878-APS6404L-3SQR-SN"/>
</part>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="R6" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="10k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 10K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT10K0/RMCF0603JT10K0CT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT10K0"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT10K0CT-ND"/>
</part>
<part name="C13" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0603" value="100n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 0.1UF 16V 10% X7R 0603"/>
<attribute name="LINK" value="http://www.digikey.de/product-detail/de/foo/1276-1005-1-ND"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics America, Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL10B104KO8NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1005-1-ND"/>
</part>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="FID1" library="microbuilder" deviceset="FIDUCIAL" device="&quot;&quot;">
<attribute name="CLASS" value="UNKNOWN CLASS"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="FID3" library="microbuilder" deviceset="FIDUCIAL" device="&quot;&quot;">
<attribute name="CLASS" value="UNKNOWN CLASS"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="FID2" library="microbuilder" deviceset="FIDUCIAL" device="&quot;&quot;">
<attribute name="CLASS" value="UNKNOWN CLASS"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="SW1" library="adafruit" deviceset="SPST_TACT" device="-EVQQ2">
<attribute name="CLASS" value="SWITCH"/>
<attribute name="DESCRIPTION" value="SWITCH TACTILE SPST-NO 0.02A 15V"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/panasonic-electronic-components/EVQ-Q2B03W/762882"/>
<attribute name="MANUFACTURER" value="Panasonic Electronic Components"/>
<attribute name="MANUFACTURERPARTNUMBER" value="EVQ-Q2B03W"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="P12932SCT-ND"/>
</part>
<part name="LED3" library="led" deviceset="LED" device="CHIP-LED0603" value="GREEN">
<attribute name="CLASS" value="LED"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="LED4" library="led" deviceset="LED" device="CHIP-LED0603" value="GREEN">
<attribute name="CLASS" value="LED"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="H301" library="holes" deviceset="MOUNT-PAD-ROUND" device="3.2">
<attribute name="CLASS" value="HOLE"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="FALSE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="R24" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="100k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 100K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-07100KL/311-100KGRCT-ND/729645"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-07100KL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-100KGRCT-ND"/>
</part>
<part name="C21" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0402" value="10n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 10000PF 25V X7R 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL05B103KA5NNNC/1276-1057-1-ND/3889143"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL05B103KA5NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1057-1-ND"/>
</part>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="R25" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="10k">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 10K OHM 5%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603JT10K0/RMCF0603JT10K0CT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603JT10K0"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603JT10K0CT-ND"/>
</part>
<part name="U4" library="laforge" deviceset="IP4234CZ6" device="">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="TVS DIODE 5.5V 6TSOP"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/nexperia-usa-inc/IP4234CZ6125/1727-4717-1-ND/2531156"/>
<attribute name="MANUFACTURER" value="Nexperia USA Inc."/>
<attribute name="MANUFACTURERPARTNUMBER" value="IP4234CZ6,125"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1727-4717-1-ND"/>
</part>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="C22" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0402" value="10n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 10000PF 25V X7R 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL05B103KA5NNNC/1276-1057-1-ND/3889143"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL05B103KA5NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1057-1-ND"/>
</part>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="C23" library="rc-sysmocom" deviceset="CAP_CERAMIC" device="_0402" value="10n">
<attribute name="CLASS" value="CAPACITOR"/>
<attribute name="DESCRIPTION" value="CAP CER 10000PF 25V X7R 0402"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/samsung-electro-mechanics/CL05B103KA5NNNC/1276-1057-1-ND/3889143"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics"/>
<attribute name="MANUFACTURERPARTNUMBER" value="CL05B103KA5NNNC"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1276-1057-1-ND"/>
</part>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="IC4" library="tlv700xx" deviceset="TLV700*" device="-SOT" technology="33" value="TLV75512PDBVR">
<attribute name="CLASS" value="IC"/>
<attribute name="DESCRIPTION" value="IC REG LINEAR 1.2V 500MA SOT23-5"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/texas-instruments/TLV75512PDBVR/9356525"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="TLV75512PDBVR"/>
<attribute name="OC_DIGIKEY" value=""/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="296-50409-1-ND"/>
</part>
<part name="IC6" library="XO-2x2.5mm" deviceset="OSCILLATOR-4" device="-SMD-2X2.5MM">
<attribute name="CLASS" value="XTAL / OSCILLATOR"/>
<attribute name="DESCRIPTION" value="XTAL OSC XO 12MHZ HCMOS SMD"/>
<attribute name="LINK" value="https://www.digikey.de/de/products/detail/jauch-quartz/O-12-0-JO22-B-1V3-1-T1-LF/10416129"/>
<attribute name="MANUFACTURER" value="Jauch Quartz"/>
<attribute name="MANUFACTURERPARTNUMBER" value="O 12,0-JO22-B-1V3-1-T1-LF"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="1908-O120-JO22-B-1V3-1-T1-LFCT-ND"/>
</part>
<part name="JP1" library="laforge" deviceset="PINHD_1X3" device="SMD">
<attribute name="CLASS" value="CONNECTOR\JUMPER"/>
<attribute name="DESCRIPTION" value="NO DESCRIPTION"/>
<attribute name="LINK" value="UNKNOWN LINK"/>
<attribute name="MANUFACTURER" value="UNKNOWN MANUFACTURER"/>
<attribute name="MANUFACTURERPARTNUMBER" value="UNKNOWN MANUFACTURER PART NUMBER"/>
<attribute name="POPULATED" value="FALSE"/>
<attribute name="ROHSCERTIFICATE" value="UNKNOWN ROHS CERTIFICATE"/>
<attribute name="SOURCE" value="UNKNOWN SOURCE"/>
<attribute name="SOURCEPARTNUMBER" value="UNKNOWN SOURCE PART NUMBER"/>
</part>
<part name="R22" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="510">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 510 OHM 1%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603FT510R/RMCF0603FT510RCT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603FT510R"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603FT510RCT-ND"/>
</part>
<part name="R23" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="510">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES 510 OHM 1%% 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/stackpole-electronics-inc/RMCF0603FT510R/RMCF0603FT510RCT-ND"/>
<attribute name="MANUFACTURER" value="Stackpole Electronics Inc"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RMCF0603FT510R"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="RoHS3-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="RMCF0603FT510RCT-ND"/>
</part>
<part name="R26" library="rc-sysmocom" deviceset="RESISTOR" device="_0603" value="0">
<attribute name="CLASS" value="RESISTOR"/>
<attribute name="DESCRIPTION" value="RES SMD 0 OHM JUMPER 1/10W 0603"/>
<attribute name="LINK" value="https://www.digikey.de/product-detail/de/yageo/RC0603JR-070RL/311-0.0GRCT-ND"/>
<attribute name="MANUFACTURER" value="Yageo"/>
<attribute name="MANUFACTURERPARTNUMBER" value="RC0603JR-070RL"/>
<attribute name="POPULATED" value="TRUE"/>
<attribute name="ROHSCERTIFICATE" value="Bleifrei / RoHS-konform"/>
<attribute name="SOURCE" value="DIGIKEY"/>
<attribute name="SOURCEPARTNUMBER" value="311-0.0GRCT-ND"/>
</part>
</parts>
<sheets>
<sheet>
<plain>
<text x="256.54" y="187.96" size="5.08" layer="97" font="vector">USB</text>
<text x="48.26" y="236.22" size="5.08" layer="97" font="vector">FPGA</text>
<text x="172.72" y="96.52" size="5.08" layer="97" font="vector">Flash + PSRAM</text>
<text x="254" y="241.3" size="5.08" layer="97" font="vector">3V3 LDO</text>
<text x="187.96" y="241.3" size="5.08" layer="97" font="vector">1V2 LDO</text>
<text x="340.36" y="177.8" size="5.08" layer="97" font="vector">12 MHz XO</text>
<text x="347.98" y="226.06" size="1.27" layer="97">Osmocom-style
3.3V UART on
2.5mm jack</text>
<text x="304.8" y="246.38" size="5.08" layer="97" font="vector">serial debug out</text>
<text x="314.96" y="96.52" size="5.08" layer="97" font="vector">misc</text>
<wire x1="124.46" y1="246.38" x2="124.46" y2="10.16" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="200.66" x2="363.22" y2="200.66" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="104.14" x2="378.46" y2="104.14" width="0.1524" layer="97" style="longdash"/>
<wire x1="332.74" y1="195.58" x2="332.74" y2="109.22" width="0.1524" layer="97" style="longdash"/>
<wire x1="261.62" y1="99.06" x2="261.62" y2="10.16" width="0.1524" layer="97" style="longdash"/>
</plain>
<instances>
<instance part="J2" gate="G$1" x="147.32" y="154.94" smashed="yes" rot="MR0">
<attribute name="DESCRIPTION" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="147.32" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="139.7" y="168.91" size="1.778" layer="95"/>
<attribute name="VALUE" x="139.7" y="165.1" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="322.58" y="144.78" smashed="yes">
<attribute name="DESCRIPTION" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCE" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ROHSCERTIFICATE" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATED" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="CLASS" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="LINK" x="322.58" y="144.78" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="NAME" x="317.5" y="156.21" size="1.778" layer="95"/>
<attribute name="VALUE" x="312.42" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="J3" gate="G$1" x="182.88" y="152.4" smashed="yes">
<attribute name="LINK" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="DESCRIPTION" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="182.88" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="195.58" y="154.94" size="1.778" layer="95"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="GND5" gate="1" x="53.34" y="50.8" smashed="yes">
<attribute name="VALUE" x="50.8" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="U1" gate="G$1" x="58.42" y="121.92" smashed="yes">
<attribute name="DESCRIPTION" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="58.42" y="121.92" size="1.27" layer="96" display="off"/>
<attribute name="NAME" x="91.44" y="190.5" size="2.54" layer="95" ratio="10" align="center"/>
</instance>
<instance part="C3" gate="G$1" x="27.94" y="81.28" smashed="yes" rot="R90">
<attribute name="SOURCE" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="DESCRIPTION" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="27.94" y="81.28" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="26.924" y="76.962" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.924" y="83.566" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="27.94" y="76.2" smashed="yes">
<attribute name="VALUE" x="25.4" y="73.66" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="G$1" x="27.94" y="127" smashed="yes" rot="R90">
<attribute name="SOURCE" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="DESCRIPTION" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="27.94" y="127" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="26.416" y="124.714" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.67" y="130.048" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND2" gate="1" x="27.94" y="121.92" smashed="yes">
<attribute name="VALUE" x="25.4" y="119.38" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="27.94" y="160.02" smashed="yes" rot="R90">
<attribute name="SOURCE" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="DESCRIPTION" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="27.94" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="26.924" y="155.702" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="26.924" y="162.306" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="27.94" y="152.4" smashed="yes">
<attribute name="VALUE" x="25.4" y="149.86" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="G$1" x="187.96" y="66.04" smashed="yes">
<attribute name="DESCRIPTION" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="SOURCE" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="ROHSCERTIFICATE" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="POPULATED" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="CLASS" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="LINK" x="187.96" y="66.04" size="1.27" layer="97" rot="MR0" display="off"/>
<attribute name="NAME" x="180.34" y="75.184" size="1.778" layer="95"/>
<attribute name="VALUE" x="180.34" y="57.404" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="210.82" y="71.12" smashed="yes">
<attribute name="SOURCE" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="LINK" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DESCRIPTION" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATED" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="CLASS" x="210.82" y="71.12" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="NAME" x="212.344" y="75.438" size="1.27" layer="95"/>
<attribute name="VALUE" x="212.09" y="73.152" size="1.27" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="172.72" y="53.34" smashed="yes">
<attribute name="VALUE" x="170.18" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="223.52" y="71.12" smashed="yes" rot="R90">
<attribute name="VALUE" x="226.06" y="68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R12" gate="G$1" x="231.14" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="234.442" y="73.66" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="234.569" y="77.724" size="1.27" layer="96" rot="R90"/>
<attribute name="DESCRIPTION" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCE" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="231.14" y="76.2" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R5" gate="G$1" x="167.64" y="76.2" smashed="yes" rot="R90">
<attribute name="DESCRIPTION" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="167.64" y="76.2" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="165.354" y="73.66" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="171.069" y="73.66" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="U2" gate="G$1" x="281.94" y="144.78" smashed="yes">
<attribute name="DESCRIPTION" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="281.94" y="144.78" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="279.4" y="154.94" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="R17" gate="G$1" x="304.8" y="121.92" smashed="yes" rot="MR180">
<attribute name="SOURCE" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="LINK" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DESCRIPTION" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ROHSCERTIFICATE" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATED" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="CLASS" x="304.8" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="NAME" x="307.34" y="124.206" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="307.34" y="118.491" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="R16" gate="G$1" x="294.64" y="121.92" smashed="yes" rot="MR180">
<attribute name="SOURCE" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="LINK" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DESCRIPTION" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ROHSCERTIFICATE" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATED" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="CLASS" x="294.64" y="121.92" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="NAME" x="297.18" y="124.206" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="297.18" y="118.491" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C19" gate="G$1" x="302.26" y="116.332" smashed="yes" rot="R180">
<attribute name="NAME" x="307.34" y="114.046" size="1.27" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="298.704" y="113.792" size="1.27" layer="96" font="vector" rot="R180"/>
<attribute name="DESCRIPTION" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCE" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ROHSCERTIFICATE" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATED" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="CLASS" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="LINK" x="302.26" y="116.332" size="1.778" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="GND21" gate="1" x="281.94" y="111.76" smashed="yes" rot="MR0">
<attribute name="VALUE" x="284.48" y="109.22" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="L1" gate="G$1" x="254" y="146.05" smashed="yes" rot="MR0">
<attribute name="NAME" x="259.588" y="152.654" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="252.222" y="152.654" size="1.778" layer="96" rot="MR0"/>
<attribute name="DESCRIPTION" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCE" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ROHSCERTIFICATE" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATED" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="CLASS" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="LINK" x="254" y="146.05" size="1.778" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="R15" gate="G$1" x="256.54" y="134.62" smashed="yes" rot="MR180">
<attribute name="NAME" x="259.08" y="136.906" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="259.08" y="131.191" size="1.27" layer="96" rot="MR0"/>
<attribute name="DESCRIPTION" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="256.54" y="134.62" size="1.27" layer="96" display="off"/>
</instance>
<instance part="C8" gate="G$1" x="68.58" y="195.58" smashed="yes" rot="R180">
<attribute name="DESCRIPTION" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURER" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCE" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ROHSCERTIFICATE" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="POPULATED" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="CLASS" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="LINK" x="68.58" y="195.58" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="NAME" x="70.866" y="193.802" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="65.532" y="193.548" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="C7" gate="G$1" x="68.58" y="200.66" smashed="yes" rot="R180">
<attribute name="SOURCE" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DESCRIPTION" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="68.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="71.628" y="199.39" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="66.294" y="199.136" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="C6" gate="G$1" x="68.58" y="208.28" smashed="yes" rot="R180">
<attribute name="SOURCE" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DESCRIPTION" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="68.58" y="208.28" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="71.628" y="207.01" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="66.294" y="206.756" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="GND6" gate="1" x="76.2" y="195.58" smashed="yes" rot="R90">
<attribute name="VALUE" x="78.74" y="193.04" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="68.58" y="213.36" smashed="yes" rot="R180">
<attribute name="SOURCE" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DESCRIPTION" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="68.58" y="213.36" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="71.628" y="212.09" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="66.294" y="211.836" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="R1" gate="G$1" x="53.34" y="208.28" smashed="yes" rot="MR90">
<attribute name="NAME" x="51.054" y="210.82" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="56.769" y="210.82" size="1.27" layer="96" rot="MR270"/>
<attribute name="DESCRIPTION" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="53.34" y="208.28" size="1.27" layer="96" display="off"/>
</instance>
<instance part="C4" gate="G$1" x="38.1" y="195.58" smashed="yes" rot="R90">
<attribute name="SOURCE" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="DESCRIPTION" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="38.1" y="195.58" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="36.83" y="192.532" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="36.576" y="197.866" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND4" gate="1" x="38.1" y="190.5" smashed="yes">
<attribute name="VALUE" x="35.56" y="187.96" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="322.58" y="43.18" smashed="yes">
<attribute name="VALUE" x="320.04" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="279.4" y="157.48" smashed="yes" rot="R180">
<attribute name="DESCRIPTION" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURER" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCE" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ROHSCERTIFICATE" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="POPULATED" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="CLASS" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="LINK" x="279.4" y="157.48" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="NAME" x="276.606" y="160.782" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="279.146" y="160.02" size="1.27" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="271.78" y="157.48" smashed="yes" rot="MR90">
<attribute name="VALUE" x="269.24" y="154.94" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="R21" gate="G$1" x="358.14" y="147.32" smashed="yes" rot="MR180">
<attribute name="DESCRIPTION" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="MANUFACTURER" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="SOURCE" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="POPULATED" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="CLASS" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="LINK" x="358.14" y="147.32" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="NAME" x="358.394" y="149.352" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="358.902" y="144.399" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="C20" gate="G$1" x="360.68" y="152.4" smashed="yes" rot="MR0">
<attribute name="SOURCE" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="LINK" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="DESCRIPTION" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURER" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="POPULATED" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="CLASS" x="360.68" y="152.4" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="NAME" x="359.156" y="156.718" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="359.41" y="154.432" size="1.27" layer="96" rot="MR0"/>
</instance>
<instance part="GND29" gate="1" x="365.76" y="124.46" smashed="yes" rot="MR0">
<attribute name="VALUE" x="368.3" y="121.92" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND28" gate="1" x="350.52" y="152.4" smashed="yes" rot="MR90">
<attribute name="VALUE" x="347.98" y="149.86" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GND18" gate="1" x="269.24" y="205.74" smashed="yes" rot="MR0">
<attribute name="VALUE" x="271.78" y="203.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C18" gate="G$1" x="284.48" y="213.36" smashed="yes" rot="R270">
<attribute name="SOURCE" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="LINK" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DESCRIPTION" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATED" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="CLASS" x="284.48" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="NAME" x="282.575" y="208.788" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="282.321" y="214.376" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="256.54" y="213.36" smashed="yes" rot="R270">
<attribute name="SOURCE" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="LINK" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DESCRIPTION" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATED" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="CLASS" x="256.54" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="NAME" x="254.635" y="208.788" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="254.889" y="215.138" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND17" gate="1" x="256.54" y="205.74" smashed="yes" rot="MR0">
<attribute name="VALUE" x="259.08" y="203.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND22" gate="1" x="284.48" y="205.74" smashed="yes" rot="MR0">
<attribute name="VALUE" x="287.02" y="203.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND13" gate="1" x="203.2" y="205.74" smashed="yes" rot="MR0">
<attribute name="VALUE" x="205.74" y="203.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C11" gate="G$1" x="187.96" y="213.36" smashed="yes" rot="R270">
<attribute name="DESCRIPTION" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCE" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATED" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="CLASS" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="LINK" x="187.96" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="NAME" x="184.658" y="210.566" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="185.42" y="213.106" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C14" gate="G$1" x="218.44" y="213.36" smashed="yes" rot="R270">
<attribute name="DESCRIPTION" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCE" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATED" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="CLASS" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="LINK" x="218.44" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="NAME" x="215.138" y="210.566" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="215.9" y="213.106" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND12" gate="1" x="187.96" y="205.74" smashed="yes" rot="MR0">
<attribute name="VALUE" x="190.5" y="203.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND14" gate="1" x="218.44" y="205.74" smashed="yes" rot="MR0">
<attribute name="VALUE" x="220.98" y="203.2" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R18" gate="G$1" x="304.8" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="302.006" y="68.58" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="306.451" y="68.58" size="1.27" layer="96" rot="R270"/>
<attribute name="DESCRIPTION" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="SOURCE" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="304.8" y="66.04" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="CDONE" gate="G$1" x="304.8" y="53.34">
<attribute name="DESCRIPTION" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="MANUFACTURER" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="SOURCE" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="ROHSCERTIFICATE" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="POPULATED" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="CLASS" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
<attribute name="LINK" x="304.8" y="53.34" size="1.27" layer="97" display="off"/>
</instance>
<instance part="GND23" gate="1" x="304.8" y="43.18" smashed="yes">
<attribute name="VALUE" x="302.26" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="276.86" y="58.42" smashed="yes" rot="R90">
<attribute name="VALUE" x="289.56" y="53.34" size="1.778" layer="96" rot="R90"/>
<attribute name="NAME" x="276.86" y="51.308" size="1.778" layer="95" rot="R180"/>
<attribute name="DESCRIPTION" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="276.86" y="58.42" size="1.778" layer="96" display="off"/>
</instance>
<instance part="X2" gate="G$1" x="353.06" y="218.44" smashed="yes">
<attribute name="DESCRIPTION" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="353.06" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="363.22" y="222.504" size="1.778" layer="95"/>
<attribute name="VALUE" x="350.52" y="209.55" size="1.778" layer="96"/>
</instance>
<instance part="GND27" gate="1" x="345.44" y="205.74"/>
<instance part="U3" gate="G$1" x="330.2" y="218.44" rot="MR0">
<attribute name="DESCRIPTION" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="330.2" y="218.44" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND25" gate="1" x="330.2" y="205.74"/>
<instance part="IC5" gate="G$1" x="271.78" y="220.98">
<attribute name="SOURCEPARTNUMBER" x="271.78" y="220.98" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DESCRIPTION" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="271.78" y="220.98" size="1.27" layer="96" display="off"/>
</instance>
<instance part="R13" gate="G$1" x="236.22" y="149.86" smashed="yes" rot="MR180">
<attribute name="NAME" x="234.95" y="151.638" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="239.776" y="151.765" size="1.27" layer="96" rot="MR0"/>
<attribute name="DESCRIPTION" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="236.22" y="149.86" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R14" gate="G$1" x="236.22" y="142.24" smashed="yes" rot="MR180">
<attribute name="NAME" x="235.458" y="144.272" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="239.776" y="144.145" size="1.27" layer="96" rot="MR0"/>
<attribute name="DESCRIPTION" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="236.22" y="142.24" size="1.778" layer="96" display="off"/>
</instance>
<instance part="LED1" gate="G$1" x="83.82" y="40.64" rot="MR0">
<attribute name="DESCRIPTION" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCE" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="83.82" y="40.64" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="IC3" gate="-G2" x="208.28" y="172.72" smashed="yes">
<attribute name="DESCRIPTION" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCE" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="208.28" y="172.72" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="NAME" x="199.39" y="164.719" size="1.778" layer="95"/>
<attribute name="VALUE" x="211.582" y="166.116" size="1.778" layer="96"/>
</instance>
<instance part="IC3" gate="-G1" x="215.9" y="180.34" smashed="yes">
<attribute name="NAME" x="200.66" y="183.515" size="1.778" layer="95"/>
<attribute name="VALUE" x="217.678" y="184.404" size="1.778" layer="96"/>
</instance>
<instance part="IC3" gate="-P" x="269.24" y="25.4"/>
<instance part="C17" gate="G$1" x="279.4" y="25.4" smashed="yes" rot="R90">
<attribute name="SOURCE" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="DESCRIPTION" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="279.4" y="25.4" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="277.622" y="26.924" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="282.448" y="26.67" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="274.32" y="15.24" smashed="yes">
<attribute name="VALUE" x="271.78" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="167.64" y="147.32" smashed="yes" rot="MR0">
<attribute name="VALUE" x="170.18" y="144.78" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R2" gate="G$1" x="149.86" y="132.08" smashed="yes" rot="MR90">
<attribute name="SOURCE" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="LINK" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="DESCRIPTION" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURER" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="POPULATED" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="CLASS" x="149.86" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="NAME" x="147.574" y="134.62" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="153.289" y="134.62" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R3" gate="G$1" x="149.86" y="121.92" smashed="yes" rot="MR90">
<attribute name="SOURCE" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="LINK" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="DESCRIPTION" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURER" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="POPULATED" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="CLASS" x="149.86" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="NAME" x="147.574" y="124.46" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="153.289" y="124.46" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="C9" gate="G$1" x="154.94" y="129.54" smashed="yes" rot="R270">
<attribute name="NAME" x="157.226" y="134.62" size="1.27" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="157.48" y="125.984" size="1.27" layer="96" font="vector" rot="R270"/>
<attribute name="DESCRIPTION" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURER" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCE" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="POPULATED" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="CLASS" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="LINK" x="154.94" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
</instance>
<instance part="R8" gate="G$1" x="175.26" y="132.08" smashed="yes" rot="MR90">
<attribute name="SOURCE" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="LINK" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="DESCRIPTION" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURER" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="POPULATED" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="CLASS" x="175.26" y="132.08" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="NAME" x="172.974" y="134.62" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="178.689" y="134.62" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="R9" gate="G$1" x="175.26" y="121.92" smashed="yes" rot="MR90">
<attribute name="SOURCE" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="LINK" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="DESCRIPTION" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURER" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="POPULATED" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="CLASS" x="175.26" y="121.92" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="NAME" x="172.974" y="124.46" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="178.689" y="124.46" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="C10" gate="G$1" x="180.34" y="129.54" smashed="yes" rot="R270">
<attribute name="NAME" x="182.626" y="134.62" size="1.27" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="182.88" y="125.984" size="1.27" layer="96" font="vector" rot="R270"/>
<attribute name="DESCRIPTION" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURER" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCE" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="POPULATED" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="CLASS" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
<attribute name="LINK" x="180.34" y="129.54" size="1.778" layer="96" rot="MR270" display="off"/>
</instance>
<instance part="GND7" gate="1" x="152.4" y="114.3" smashed="yes" rot="MR0">
<attribute name="VALUE" x="154.94" y="111.76" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND11" gate="1" x="177.8" y="114.3" smashed="yes" rot="MR0">
<attribute name="VALUE" x="180.34" y="111.76" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R4" gate="G$1" x="165.1" y="167.64" smashed="yes" rot="MR90">
<attribute name="NAME" x="163.576" y="163.576" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="163.703" y="175.006" size="1.27" layer="96" rot="MR270"/>
<attribute name="DESCRIPTION" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCE" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="165.1" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R7" gate="G$1" x="170.18" y="167.64" smashed="yes" rot="MR90">
<attribute name="NAME" x="173.99" y="164.846" size="1.27" layer="95" rot="MR270"/>
<attribute name="VALUE" x="171.577" y="168.91" size="1.27" layer="96" rot="MR90"/>
<attribute name="DESCRIPTION" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCE" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="ROHSCERTIFICATE" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="POPULATED" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="CLASS" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
<attribute name="LINK" x="170.18" y="167.64" size="1.27" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R10" gate="G$1" x="233.68" y="180.34" smashed="yes" rot="MR180">
<attribute name="NAME" x="232.41" y="182.118" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="237.236" y="182.245" size="1.27" layer="96" rot="MR0"/>
<attribute name="DESCRIPTION" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="233.68" y="180.34" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R11" gate="G$1" x="233.68" y="172.72" smashed="yes" rot="MR180">
<attribute name="NAME" x="232.918" y="174.752" size="1.27" layer="95" rot="MR0"/>
<attribute name="VALUE" x="237.236" y="174.625" size="1.27" layer="96" rot="MR0"/>
<attribute name="DESCRIPTION" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="233.68" y="172.72" size="1.778" layer="96" display="off"/>
</instance>
<instance part="IC2" gate="G$1" x="187.96" y="30.48" smashed="yes">
<attribute name="NAME" x="180.34" y="39.624" size="1.778" layer="95"/>
<attribute name="VALUE" x="180.34" y="21.844" size="1.778" layer="96"/>
<attribute name="DESCRIPTION" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="187.96" y="30.48" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND10" gate="1" x="172.72" y="20.32" smashed="yes">
<attribute name="VALUE" x="170.18" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="167.64" y="40.64" smashed="yes" rot="R90">
<attribute name="DESCRIPTION" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="167.64" y="40.64" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="165.354" y="38.1" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="171.069" y="38.1" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C13" gate="G$1" x="213.36" y="35.56" smashed="yes">
<attribute name="SOURCE" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="LINK" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DESCRIPTION" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ROHSCERTIFICATE" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATED" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="CLASS" x="213.36" y="35.56" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="NAME" x="214.884" y="39.878" size="1.27" layer="95"/>
<attribute name="VALUE" x="214.63" y="37.592" size="1.27" layer="96"/>
</instance>
<instance part="GND16" gate="1" x="226.06" y="35.56" smashed="yes" rot="R90">
<attribute name="VALUE" x="228.6" y="33.02" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="FID1" gate="G$1" x="297.18" y="30.48">
<attribute name="DESCRIPTION" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="297.18" y="30.48" size="1.27" layer="96" display="off"/>
</instance>
<instance part="FID3" gate="G$1" x="307.34" y="30.48">
<attribute name="DESCRIPTION" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="307.34" y="30.48" size="1.27" layer="96" display="off"/>
</instance>
<instance part="FID2" gate="G$1" x="302.26" y="30.48">
<attribute name="DESCRIPTION" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="302.26" y="30.48" size="1.27" layer="96" display="off"/>
</instance>
<instance part="SW1" gate="G$1" x="325.12" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="316.23" y="60.96" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="331.47" y="70.485" size="1.778" layer="96" rot="R270"/>
<attribute name="DESCRIPTION" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="325.12" y="58.42" size="1.27" layer="96" display="off"/>
</instance>
<instance part="LED3" gate="G$1" x="360.68" y="60.96">
<attribute name="DESCRIPTION" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="360.68" y="60.96" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND30" gate="1" x="360.68" y="43.18"/>
<instance part="LED4" gate="G$1" x="373.38" y="60.96">
<attribute name="DESCRIPTION" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="373.38" y="60.96" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND31" gate="1" x="373.38" y="43.18"/>
<instance part="H301" gate="G$1" x="340.36" y="30.48" smashed="yes">
<attribute name="NAME" x="342.392" y="31.0642" size="1.778" layer="95"/>
<attribute name="VALUE" x="342.392" y="28.0162" size="1.778" layer="96"/>
<attribute name="DESCRIPTION" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="340.36" y="30.48" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R24" gate="G$1" x="330.2" y="27.94" smashed="yes" rot="MR0">
<attribute name="SOURCE" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="LINK" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="DESCRIPTION" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="MANUFACTURER" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="ROHSCERTIFICATE" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="POPULATED" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="CLASS" x="330.2" y="27.94" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="NAME" x="324.866" y="26.416" size="1.27" layer="95" rot="MR180"/>
<attribute name="VALUE" x="332.74" y="26.289" size="1.27" layer="96" rot="MR180"/>
</instance>
<instance part="C21" gate="G$1" x="327.66" y="33.02" smashed="yes">
<attribute name="NAME" x="322.58" y="35.306" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="331.216" y="35.56" size="1.27" layer="96" font="vector"/>
<attribute name="DESCRIPTION" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="MANUFACTURER" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="SOURCE" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="ROHSCERTIFICATE" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="POPULATED" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="CLASS" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
<attribute name="LINK" x="327.66" y="33.02" size="1.778" layer="96" rot="MR180" display="off"/>
</instance>
<instance part="GND32" gate="1" x="320.04" y="30.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="317.5" y="33.02" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R25" gate="G$1" x="101.6" y="147.32" smashed="yes">
<attribute name="NAME" x="99.06" y="144.018" size="1.27" layer="95"/>
<attribute name="VALUE" x="103.124" y="143.891" size="1.27" layer="96"/>
<attribute name="DESCRIPTION" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="101.6" y="147.32" size="1.27" layer="96" display="off"/>
</instance>
<instance part="U4" gate="G$1" x="185.42" y="177.8" smashed="yes">
<attribute name="DESCRIPTION" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="185.42" y="177.8" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="182.88" y="187.96" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND26" gate="1" x="185.42" y="165.1" smashed="yes" rot="MR0">
<attribute name="VALUE" x="187.96" y="162.56" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C22" gate="G$1" x="340.36" y="50.8" smashed="yes" rot="MR90">
<attribute name="NAME" x="337.566" y="48.26" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="337.312" y="54.61" size="1.27" layer="96" font="vector" rot="MR90"/>
<attribute name="DESCRIPTION" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="340.36" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND33" gate="1" x="340.36" y="43.18"/>
<instance part="C23" gate="G$1" x="347.98" y="50.8" smashed="yes" rot="MR90">
<attribute name="NAME" x="345.186" y="48.26" size="1.27" layer="95" font="vector" rot="MR90"/>
<attribute name="VALUE" x="344.932" y="54.61" size="1.27" layer="96" font="vector" rot="MR90"/>
<attribute name="DESCRIPTION" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="SOURCE" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="POPULATED" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="CLASS" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="LINK" x="347.98" y="50.8" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND34" gate="1" x="347.98" y="43.18"/>
<instance part="IC4" gate="G$1" x="205.74" y="220.98">
<attribute name="SOURCEPARTNUMBER" x="205.74" y="220.98" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DESCRIPTION" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="205.74" y="220.98" size="1.27" layer="96" display="off"/>
</instance>
<instance part="IC6" gate="G$1" x="365.76" y="139.7" smashed="yes" rot="MR0">
<attribute name="NAME" x="360.68" y="135.636" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="360.68" y="134.62" size="1.778" layer="96" rot="MR0"/>
<attribute name="DESCRIPTION" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURER" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCE" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ROHSCERTIFICATE" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="POPULATED" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="CLASS" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="LINK" x="365.76" y="139.7" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="JP1" gate="G$1" x="144.78" y="226.06" smashed="yes" rot="R180">
<attribute name="NAME" x="146.05" y="235.585" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="137.16" y="232.41" size="1.778" layer="96" rot="R270"/>
<attribute name="DESCRIPTION" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURER" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCE" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ROHSCERTIFICATE" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="POPULATED" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="CLASS" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="LINK" x="144.78" y="226.06" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="R22" gate="G$1" x="360.68" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="358.394" y="50.8" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="363.855" y="48.26" size="1.27" layer="96" rot="R90"/>
<attribute name="DESCRIPTION" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="360.68" y="50.8" size="1.27" layer="96" display="off"/>
</instance>
<instance part="R23" gate="G$1" x="373.38" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="371.094" y="50.8" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="376.555" y="48.26" size="1.27" layer="96" rot="R90"/>
<attribute name="DESCRIPTION" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="SOURCE" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="ROHSCERTIFICATE" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="POPULATED" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="CLASS" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
<attribute name="LINK" x="373.38" y="50.8" size="1.27" layer="96" display="off"/>
</instance>
<instance part="R26" gate="G$1" x="157.48" y="241.3" smashed="yes">
<attribute name="NAME" x="162.56" y="244.602" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="162.56" y="239.649" size="1.27" layer="96" rot="R180"/>
<attribute name="DESCRIPTION" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURER" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURERPARTNUMBER" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCE" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="SOURCEPARTNUMBER" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ROHSCERTIFICATE" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="POPULATED" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="CLASS" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="LINK" x="157.48" y="241.3" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="3">
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="53.34" y1="53.34" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="27.94" y1="154.94" x2="27.94" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="172.72" y1="55.88" x2="172.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="172.72" y1="63.5" x2="175.26" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="220.98" y1="71.12" x2="215.9" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="297.18" y1="116.332" x2="289.56" y2="116.332" width="0.1524" layer="91"/>
<wire x1="289.56" y1="116.332" x2="289.56" y2="119.38" width="0.1524" layer="91"/>
<wire x1="289.56" y1="119.38" x2="289.56" y2="121.92" width="0.1524" layer="91"/>
<wire x1="312.42" y1="139.7" x2="297.18" y2="139.7" width="0.1524" layer="91"/>
<wire x1="297.18" y1="139.7" x2="297.18" y2="132.08" width="0.1524" layer="91"/>
<wire x1="297.18" y1="132.08" x2="281.94" y2="132.08" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="281.94" y1="132.08" x2="281.94" y2="134.62" width="0.1524" layer="91"/>
<wire x1="281.94" y1="132.08" x2="281.94" y2="119.38" width="0.1524" layer="91"/>
<junction x="281.94" y="132.08"/>
<wire x1="281.94" y1="119.38" x2="281.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="289.56" y1="119.38" x2="281.94" y2="119.38" width="0.1524" layer="91"/>
<junction x="289.56" y="119.38"/>
<junction x="281.94" y="119.38"/>
<pinref part="GND21" gate="1" pin="GND"/>
<pinref part="J1" gate="G$1" pin="4GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="71.12" y1="213.36" x2="73.66" y2="213.36" width="0.1524" layer="91"/>
<wire x1="73.66" y1="213.36" x2="73.66" y2="208.28" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="73.66" y1="208.28" x2="73.66" y2="200.66" width="0.1524" layer="91"/>
<wire x1="73.66" y1="200.66" x2="73.66" y2="195.58" width="0.1524" layer="91"/>
<wire x1="73.66" y1="195.58" x2="71.12" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="71.12" y1="200.66" x2="73.66" y2="200.66" width="0.1524" layer="91"/>
<junction x="73.66" y="200.66"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="71.12" y1="208.28" x2="73.66" y2="208.28" width="0.1524" layer="91"/>
<junction x="73.66" y="208.28"/>
<pinref part="GND6" gate="1" pin="GND"/>
<junction x="73.66" y="195.58"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="322.58" y1="53.34" x2="322.58" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="SW1" gate="G$1" pin="S1"/>
<pinref part="SW1" gate="G$1" pin="S"/>
<wire x1="322.58" y1="50.8" x2="322.58" y2="45.72" width="0.1524" layer="91"/>
<wire x1="325.12" y1="53.34" x2="325.12" y2="50.8" width="0.1524" layer="91"/>
<wire x1="325.12" y1="50.8" x2="322.58" y2="50.8" width="0.1524" layer="91"/>
<junction x="322.58" y="50.8"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="365.76" y1="127" x2="365.76" y2="134.62" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="353.06" y1="152.4" x2="355.6" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="269.24" y1="208.28" x2="269.24" y2="213.36" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="203.2" y1="208.28" x2="203.2" y2="213.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="CDONE" gate="G$1" pin="C"/>
<wire x1="304.8" y1="45.72" x2="304.8" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="8"/>
<wire x1="266.7" y1="63.5" x2="266.7" y2="66.04" width="0.1524" layer="91"/>
<label x="266.7" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="RING"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="347.98" y1="213.36" x2="345.44" y2="213.36" width="0.1524" layer="91"/>
<wire x1="345.44" y1="213.36" x2="345.44" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="-P" pin="GND"/>
<wire x1="269.24" y1="17.78" x2="274.32" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="274.32" y1="17.78" x2="279.4" y2="17.78" width="0.1524" layer="91"/>
<wire x1="279.4" y1="17.78" x2="279.4" y2="22.86" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
<junction x="274.32" y="17.78"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="4GND"/>
<pinref part="J3" gate="G$1" pin="GND"/>
<wire x1="157.48" y1="149.86" x2="167.64" y2="149.86" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="167.64" y1="149.86" x2="175.26" y2="149.86" width="0.1524" layer="91"/>
<junction x="167.64" y="149.86"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="154.94" y1="124.46" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="154.94" y1="116.84" x2="152.4" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="152.4" y1="116.84" x2="149.86" y2="116.84" width="0.1524" layer="91"/>
<junction x="152.4" y="116.84"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="175.26" y1="116.84" x2="177.8" y2="116.84" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="177.8" y1="116.84" x2="180.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="180.34" y1="116.84" x2="180.34" y2="124.46" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
<junction x="177.8" y="116.84"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="175.26" y1="27.94" x2="172.72" y2="27.94" width="0.1524" layer="91"/>
<wire x1="172.72" y1="27.94" x2="172.72" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="223.52" y1="35.56" x2="218.44" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="325.12" y1="27.94" x2="325.12" y2="30.48" width="0.1524" layer="91"/>
<wire x1="325.12" y1="30.48" x2="325.12" y2="33.02" width="0.1524" layer="91"/>
<wire x1="325.12" y1="30.48" x2="322.58" y2="30.48" width="0.1524" layer="91"/>
<junction x="325.12" y="30.48"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<pinref part="GND26" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="340.36" y1="45.72" x2="340.36" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="347.98" y1="45.72" x2="347.98" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="R22" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="R23" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VCC_3V3" class="3">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="167.64" y1="81.28" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<label x="167.64" y="83.82" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="208.28" y1="71.12" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<wire x1="208.28" y1="71.12" x2="208.28" y2="83.82" width="0.1524" layer="91"/>
<junction x="208.28" y="71.12"/>
<label x="208.28" y="83.82" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="231.14" y1="81.28" x2="231.14" y2="83.82" width="0.1524" layer="91"/>
<label x="231.14" y="83.82" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="22.86" y1="203.2" x2="22.86" y2="218.44" width="0.1524" layer="91"/>
<label x="22.86" y="218.44" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U1" gate="G$1" pin="VCC_IO_0"/>
<wire x1="33.02" y1="88.9" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="27.94" y1="88.9" x2="22.86" y2="88.9" width="0.1524" layer="91"/>
<junction x="27.94" y="88.9"/>
<wire x1="22.86" y1="88.9" x2="22.86" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCC_IO_1"/>
<wire x1="33.02" y1="134.62" x2="27.94" y2="134.62" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="27.94" y1="132.08" x2="27.94" y2="134.62" width="0.1524" layer="91"/>
<wire x1="22.86" y1="134.62" x2="27.94" y2="134.62" width="0.1524" layer="91"/>
<junction x="27.94" y="134.62"/>
<wire x1="22.86" y1="134.62" x2="22.86" y2="167.64" width="0.1524" layer="91"/>
<junction x="22.86" y="134.62"/>
<pinref part="U1" gate="G$1" pin="VCC_IO_2"/>
<wire x1="33.02" y1="167.64" x2="27.94" y2="167.64" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="165.1" x2="27.94" y2="167.64" width="0.1524" layer="91"/>
<wire x1="22.86" y1="167.64" x2="27.94" y2="167.64" width="0.1524" layer="91"/>
<junction x="27.94" y="167.64"/>
<wire x1="22.86" y1="167.64" x2="22.86" y2="203.2" width="0.1524" layer="91"/>
<junction x="22.86" y="167.64"/>
<junction x="22.86" y="203.2"/>
<pinref part="U1" gate="G$1" pin="VPP_2V5"/>
<wire x1="48.26" y1="187.96" x2="48.26" y2="203.2" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="38.1" y1="203.2" x2="48.26" y2="203.2" width="0.1524" layer="91"/>
<wire x1="38.1" y1="200.66" x2="38.1" y2="203.2" width="0.1524" layer="91"/>
<junction x="38.1" y="203.2"/>
<wire x1="22.86" y1="203.2" x2="38.1" y2="203.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="363.22" y1="147.32" x2="365.76" y2="147.32" width="0.1524" layer="91"/>
<wire x1="365.76" y1="147.32" x2="365.76" y2="144.78" width="0.1524" layer="91"/>
<wire x1="365.76" y1="147.32" x2="365.76" y2="152.4" width="0.1524" layer="91"/>
<junction x="365.76" y="147.32"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="365.76" y1="152.4" x2="363.22" y2="152.4" width="0.1524" layer="91"/>
<junction x="365.76" y="152.4"/>
<wire x1="365.76" y1="152.4" x2="365.76" y2="157.48" width="0.1524" layer="91"/>
<label x="365.76" y="157.48" size="1.27" layer="95" rot="MR90" xref="yes"/>
<pinref part="IC6" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="284.48" y1="215.9" x2="284.48" y2="226.06" width="0.1524" layer="91"/>
<wire x1="284.48" y1="226.06" x2="289.56" y2="226.06" width="0.1524" layer="91"/>
<junction x="284.48" y="226.06"/>
<label x="289.56" y="226.06" size="1.27" layer="95" xref="yes"/>
<pinref part="IC5" gate="G$1" pin="OUT"/>
<wire x1="279.4" y1="226.06" x2="284.48" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="68.58" y1="40.64" x2="66.04" y2="40.64" width="0.1524" layer="91"/>
<label x="66.04" y="40.64" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="LED1" gate="G$1" pin="BLUE"/>
<wire x1="71.12" y1="43.18" x2="68.58" y2="43.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="43.18" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="GRN"/>
<wire x1="71.12" y1="40.64" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
<junction x="68.58" y="40.64"/>
<pinref part="LED1" gate="G$1" pin="RED"/>
<wire x1="71.12" y1="38.1" x2="68.58" y2="38.1" width="0.1524" layer="91"/>
<wire x1="68.58" y1="38.1" x2="68.58" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="304.8" y="73.66" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="304.8" y1="73.66" x2="304.8" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="284.48" y1="63.5" x2="284.48" y2="66.04" width="0.1524" layer="91"/>
<label x="284.48" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="VCC"/>
<wire x1="330.2" y1="228.6" x2="330.2" y2="231.14" width="0.1524" layer="91"/>
<label x="330.2" y="231.14" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC3" gate="-P" pin="VCC"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="269.24" y1="33.02" x2="279.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="279.4" y1="33.02" x2="279.4" y2="30.48" width="0.1524" layer="91"/>
<label x="274.32" y="33.02" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VCC"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="210.82" y1="35.56" x2="208.28" y2="35.56" width="0.1524" layer="91"/>
<wire x1="208.28" y1="35.56" x2="205.74" y2="35.56" width="0.1524" layer="91"/>
<wire x1="208.28" y1="35.56" x2="208.28" y2="40.64" width="0.1524" layer="91"/>
<junction x="208.28" y="35.56"/>
<label x="208.28" y="40.64" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="167.64" y1="45.72" x2="167.64" y2="48.26" width="0.1524" layer="91"/>
<label x="167.64" y="48.26" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="A"/>
<wire x1="360.68" y1="63.5" x2="360.68" y2="66.04" width="0.1524" layer="91"/>
<label x="360.68" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="106.68" y1="147.32" x2="109.22" y2="147.32" width="0.1524" layer="91"/>
<label x="109.22" y="147.32" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="340.36" y1="55.88" x2="340.36" y2="66.04" width="0.1524" layer="91"/>
<label x="340.36" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="347.98" y1="55.88" x2="347.98" y2="66.04" width="0.1524" layer="91"/>
<label x="347.98" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="VBUS_HOST" class="3">
<segment>
<wire x1="312.42" y1="147.32" x2="304.8" y2="147.32" width="0.1524" layer="91"/>
<wire x1="304.8" y1="147.32" x2="304.8" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="281.94" y1="154.94" x2="281.94" y2="157.48" width="0.1524" layer="91"/>
<label x="281.94" y="162.56" size="1.27" layer="95" rot="R90" xref="yes"/>
<wire x1="281.94" y1="157.48" x2="281.94" y2="162.56" width="0.1524" layer="91"/>
<wire x1="281.94" y1="157.48" x2="304.8" y2="157.48" width="0.1524" layer="91"/>
<junction x="281.94" y="157.48"/>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="J1" gate="G$1" pin="1VCC"/>
</segment>
<segment>
<label x="157.48" y="223.52" size="1.27" layer="95" xref="yes"/>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="157.48" y1="223.52" x2="144.78" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_D_N" class="1">
<segment>
<label x="228.6" y="149.86" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="228.6" y1="149.86" x2="231.14" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="96.52" y="162.56" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="IOB_8A"/>
<wire x1="96.52" y1="162.56" x2="88.9" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB_D_P" class="1">
<segment>
<label x="228.6" y="142.24" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="228.6" y1="142.24" x2="231.14" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="96.52" y="160.02" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="IOB_9B"/>
<wire x1="96.52" y1="160.02" x2="88.9" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB0_D_N" class="1">
<segment>
<wire x1="312.42" y1="144.78" x2="294.64" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2D-"/>
<pinref part="U2" gate="G$1" pin="2B"/>
<wire x1="292.1" y1="147.32" x2="294.64" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB1_D_P" class="1">
<segment>
<wire x1="266.7" y1="142.24" x2="266.7" y2="134.62" width="0.1524" layer="91"/>
<wire x1="266.7" y1="134.62" x2="261.62" y2="134.62" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="1A"/>
<wire x1="266.7" y1="142.24" x2="271.78" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="264.16" y1="142.24" x2="266.7" y2="142.24" width="0.1524" layer="91"/>
<junction x="266.7" y="142.24"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="304.8" y1="116.332" x2="309.88" y2="116.332" width="0.1524" layer="91"/>
<wire x1="309.88" y1="116.332" x2="309.88" y2="121.92" width="0.1524" layer="91"/>
<junction x="309.88" y="121.92"/>
<wire x1="309.88" y1="121.92" x2="309.88" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="GND@1"/>
<wire x1="309.88" y1="132.08" x2="309.88" y2="134.62" width="0.1524" layer="91"/>
<wire x1="309.88" y1="134.62" x2="314.96" y2="134.62" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="GND@2"/>
<wire x1="314.96" y1="132.08" x2="309.88" y2="132.08" width="0.1524" layer="91"/>
<junction x="309.88" y="132.08"/>
</segment>
</net>
<net name="CDONE" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="CDONE"/>
<wire x1="88.9" y1="154.94" x2="96.52" y2="154.94" width="0.1524" layer="91"/>
<label x="96.52" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="304.8" y1="58.42" x2="302.26" y2="58.42" width="0.1524" layer="91"/>
<label x="302.26" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="CDONE" gate="G$1" pin="A"/>
<wire x1="304.8" y1="55.88" x2="304.8" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="304.8" y1="60.96" x2="304.8" y2="58.42" width="0.1524" layer="91"/>
<junction x="304.8" y="58.42"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="6"/>
<wire x1="271.78" y1="63.5" x2="271.78" y2="66.04" width="0.1524" layer="91"/>
<label x="271.78" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="!CRESET" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="!CRESET_B"/>
<wire x1="88.9" y1="152.4" x2="93.98" y2="152.4" width="0.1524" layer="91"/>
<label x="96.52" y="152.4" size="1.27" layer="95" xref="yes"/>
<wire x1="93.98" y1="152.4" x2="96.52" y2="152.4" width="0.1524" layer="91"/>
<wire x1="93.98" y1="152.4" x2="93.98" y2="147.32" width="0.1524" layer="91"/>
<junction x="93.98" y="152.4"/>
<wire x1="93.98" y1="147.32" x2="96.52" y2="147.32" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="7"/>
<wire x1="269.24" y1="63.5" x2="269.24" y2="66.04" width="0.1524" layer="91"/>
<label x="269.24" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="CLK_12M" class="0">
<segment>
<wire x1="88.9" y1="86.36" x2="96.52" y2="86.36" width="0.1524" layer="91"/>
<label x="96.52" y="86.36" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="IOT_46B_G0"/>
</segment>
<segment>
<wire x1="358.14" y1="139.7" x2="347.98" y2="139.7" width="0.1524" layer="91"/>
<label x="347.98" y="139.7" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="IC6" gate="G$1" pin="OUT"/>
</segment>
</net>
<net name="!IOB_35B_SPI_SS" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOB_35B_SPI_SS"/>
<wire x1="88.9" y1="114.3" x2="96.52" y2="114.3" width="0.1524" layer="91"/>
<label x="96.52" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="!CS"/>
<wire x1="175.26" y1="71.12" x2="167.64" y2="71.12" width="0.1524" layer="91"/>
<label x="154.94" y="71.12" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="167.64" y1="71.12" x2="154.94" y2="71.12" width="0.1524" layer="91"/>
<junction x="167.64" y="71.12"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="5"/>
<wire x1="274.32" y1="63.5" x2="274.32" y2="66.04" width="0.1524" layer="91"/>
<label x="274.32" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="IOB_34A_SPI_SCK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOB_34A_SPI_SCK"/>
<wire x1="88.9" y1="116.84" x2="96.52" y2="116.84" width="0.1524" layer="91"/>
<label x="96.52" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SCK"/>
<wire x1="205.74" y1="66.04" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<label x="236.22" y="66.04" size="1.27" layer="95" xref="yes"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="231.14" y1="66.04" x2="236.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="231.14" y1="71.12" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<junction x="231.14" y="66.04"/>
<pinref part="IC2" gate="G$1" pin="SCK"/>
<wire x1="205.74" y1="30.48" x2="231.14" y2="30.48" width="0.1524" layer="91"/>
<wire x1="231.14" y1="30.48" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="4"/>
<wire x1="276.86" y1="63.5" x2="276.86" y2="66.04" width="0.1524" layer="91"/>
<label x="276.86" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="IOB_33B_SPI_SI" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOB_33B_SPI_SI"/>
<wire x1="88.9" y1="119.38" x2="96.52" y2="119.38" width="0.1524" layer="91"/>
<label x="96.52" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="3"/>
<wire x1="279.4" y1="63.5" x2="279.4" y2="66.04" width="0.1524" layer="91"/>
<label x="279.4" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SO(D1)"/>
<wire x1="175.26" y1="68.58" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<label x="154.94" y="68.58" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC2" gate="G$1" pin="SO(D1)"/>
<wire x1="162.56" y1="68.58" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="175.26" y1="33.02" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<wire x1="162.56" y1="33.02" x2="162.56" y2="68.58" width="0.1524" layer="91"/>
<junction x="162.56" y="68.58"/>
</segment>
</net>
<net name="VCC_1V2" class="3">
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="58.42" y1="187.96" x2="58.42" y2="208.28" width="0.1524" layer="91"/>
<wire x1="58.42" y1="208.28" x2="58.42" y2="213.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="213.36" x2="58.42" y2="215.9" width="0.1524" layer="91"/>
<wire x1="58.42" y1="215.9" x2="53.34" y2="215.9" width="0.1524" layer="91"/>
<junction x="53.34" y="215.9"/>
<wire x1="53.34" y1="215.9" x2="53.34" y2="218.44" width="0.1524" layer="91"/>
<label x="53.34" y="218.44" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="63.5" y1="208.28" x2="58.42" y2="208.28" width="0.1524" layer="91"/>
<junction x="58.42" y="208.28"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="63.5" y1="213.36" x2="58.42" y2="213.36" width="0.1524" layer="91"/>
<junction x="58.42" y="213.36"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="53.34" y1="213.36" x2="53.34" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="220.98" y="226.06" size="1.27" layer="95" xref="yes"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="218.44" y1="226.06" x2="220.98" y2="226.06" width="0.1524" layer="91"/>
<wire x1="218.44" y1="215.9" x2="218.44" y2="226.06" width="0.1524" layer="91"/>
<junction x="218.44" y="226.06"/>
<pinref part="IC4" gate="G$1" pin="OUT"/>
<wire x1="213.36" y1="226.06" x2="218.44" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="259.08" y1="220.98" x2="248.92" y2="220.98" width="0.1524" layer="91"/>
<label x="248.92" y="220.98" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC5" gate="G$1" pin="EN"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="63.5" y1="200.66" x2="53.34" y2="200.66" width="0.1524" layer="91"/>
<wire x1="53.34" y1="195.58" x2="53.34" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="63.5" y1="195.58" x2="53.34" y2="195.58" width="0.1524" layer="91"/>
<junction x="53.34" y="195.58"/>
<pinref part="U1" gate="G$1" pin="VCC_PLL"/>
<wire x1="53.34" y1="187.96" x2="53.34" y2="195.58" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="53.34" y1="203.2" x2="53.34" y2="200.66" width="0.1524" layer="91"/>
<junction x="53.34" y="200.66"/>
</segment>
</net>
<net name="BOOT_SW" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOT_36B"/>
<wire x1="88.9" y1="109.22" x2="96.52" y2="109.22" width="0.1524" layer="91"/>
<label x="96.52" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="322.58" y1="66.04" x2="322.58" y2="73.66" width="0.1524" layer="91"/>
<label x="322.58" y="73.66" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="SW1" gate="G$1" pin="P"/>
<wire x1="325.12" y1="63.5" x2="325.12" y2="66.04" width="0.1524" layer="91"/>
<wire x1="325.12" y1="66.04" x2="322.58" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SW1" gate="G$1" pin="P1"/>
<wire x1="322.58" y1="63.5" x2="322.58" y2="66.04" width="0.1524" layer="91"/>
<junction x="322.58" y="66.04"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RGB0"/>
<wire x1="88.9" y1="71.12" x2="111.76" y2="71.12" width="0.1524" layer="91"/>
<wire x1="111.76" y1="38.1" x2="96.52" y2="38.1" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="RGND"/>
<wire x1="111.76" y1="71.12" x2="111.76" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="358.14" y1="142.24" x2="353.06" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="353.06" y1="142.24" x2="353.06" y2="147.32" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="EN"/>
</segment>
</net>
<net name="UART_TX0" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="MIDDLE"/>
<pinref part="U3" gate="G$1" pin="1A"/>
<wire x1="347.98" y1="215.9" x2="340.36" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RGB2"/>
<wire x1="88.9" y1="66.04" x2="101.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="101.6" y1="43.18" x2="96.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="BGND"/>
<wire x1="101.6" y1="66.04" x2="101.6" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IOB_32A_SPI_SO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOB_32A_SPI_SO"/>
<wire x1="88.9" y1="121.92" x2="96.52" y2="121.92" width="0.1524" layer="91"/>
<label x="96.52" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="SI(D0)"/>
<wire x1="205.74" y1="63.5" x2="233.68" y2="63.5" width="0.1524" layer="91"/>
<label x="236.22" y="63.5" size="1.27" layer="95" xref="yes"/>
<pinref part="IC2" gate="G$1" pin="SI(D0)"/>
<wire x1="233.68" y1="63.5" x2="236.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="205.74" y1="27.94" x2="233.68" y2="27.94" width="0.1524" layer="91"/>
<wire x1="233.68" y1="27.94" x2="233.68" y2="63.5" width="0.1524" layer="91"/>
<junction x="233.68" y="63.5"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="281.94" y1="63.5" x2="281.94" y2="66.04" width="0.1524" layer="91"/>
<label x="281.94" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RGB1"/>
<wire x1="88.9" y1="68.58" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
<wire x1="106.68" y1="40.64" x2="96.52" y2="40.64" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="GGND"/>
<wire x1="106.68" y1="68.58" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBUS_DUT" class="3">
<segment>
<pinref part="J2" gate="G$1" pin="1VCC"/>
<pinref part="J3" gate="G$1" pin="VBUS"/>
<wire x1="157.48" y1="157.48" x2="160.02" y2="157.48" width="0.1524" layer="91"/>
<wire x1="160.02" y1="157.48" x2="175.26" y2="157.48" width="0.1524" layer="91"/>
<wire x1="160.02" y1="157.48" x2="160.02" y2="185.42" width="0.1524" layer="91"/>
<junction x="160.02" y="157.48"/>
<label x="160.02" y="187.96" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U4" gate="G$1" pin="VCC"/>
<wire x1="160.02" y1="185.42" x2="160.02" y2="187.96" width="0.1524" layer="91"/>
<wire x1="185.42" y1="187.96" x2="185.42" y2="190.5" width="0.1524" layer="91"/>
<wire x1="185.42" y1="190.5" x2="165.1" y2="190.5" width="0.1524" layer="91"/>
<wire x1="165.1" y1="190.5" x2="165.1" y2="185.42" width="0.1524" layer="91"/>
<wire x1="165.1" y1="185.42" x2="160.02" y2="185.42" width="0.1524" layer="91"/>
<junction x="160.02" y="185.42"/>
</segment>
<segment>
<label x="157.48" y="228.6" size="1.27" layer="95" xref="yes"/>
<pinref part="JP1" gate="G$1" pin="3"/>
<wire x1="157.48" y1="228.6" x2="144.78" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="152.4" y1="241.3" x2="149.86" y2="241.3" width="0.1524" layer="91"/>
<label x="149.86" y="241.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3_N" class="1">
<segment>
<pinref part="J2" gate="G$1" pin="2D-"/>
<pinref part="J3" gate="G$1" pin="D_N"/>
<wire x1="157.48" y1="154.94" x2="170.18" y2="154.94" width="0.1524" layer="91"/>
<wire x1="170.18" y1="154.94" x2="175.26" y2="154.94" width="0.1524" layer="91"/>
<wire x1="170.18" y1="154.94" x2="170.18" y2="162.56" width="0.1524" layer="91"/>
<junction x="170.18" y="154.94"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$3_P" class="1">
<segment>
<pinref part="J2" gate="G$1" pin="3D+"/>
<pinref part="J3" gate="G$1" pin="D_P"/>
<wire x1="157.48" y1="152.4" x2="165.1" y2="152.4" width="0.1524" layer="91"/>
<wire x1="165.1" y1="152.4" x2="175.26" y2="152.4" width="0.1524" layer="91"/>
<wire x1="165.1" y1="152.4" x2="165.1" y2="162.56" width="0.1524" layer="91"/>
<junction x="165.1" y="152.4"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="175.26" y1="137.16" x2="180.34" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="180.34" y1="137.16" x2="180.34" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="SHIELD"/>
<wire x1="175.26" y1="147.32" x2="175.26" y2="137.16" width="0.1524" layer="91"/>
<junction x="175.26" y="137.16"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="GND@1"/>
<pinref part="J2" gate="G$1" pin="GND@2"/>
<wire x1="154.94" y1="144.78" x2="154.94" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="149.86" y1="137.16" x2="154.94" y2="137.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="137.16" x2="154.94" y2="132.08" width="0.1524" layer="91"/>
<wire x1="154.94" y1="142.24" x2="154.94" y2="137.16" width="0.1524" layer="91"/>
<junction x="154.94" y="142.24"/>
<junction x="154.94" y="137.16"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="165.1" y1="180.34" x2="165.1" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="2A"/>
<wire x1="165.1" y1="180.34" x2="175.26" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC3" gate="-G2" pin="Y"/>
<wire x1="218.44" y1="172.72" x2="228.6" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC3" gate="-G1" pin="Y"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="226.06" y1="180.34" x2="228.6" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TAP_D_P" class="1">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="238.76" y1="180.34" x2="241.3" y2="180.34" width="0.1524" layer="91"/>
<label x="241.3" y="180.34" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<label x="96.52" y="76.2" size="1.27" layer="95" xref="yes"/>
<wire x1="96.52" y1="76.2" x2="88.9" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="IOT_51A"/>
</segment>
</net>
<net name="TAP_D_N" class="1">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="238.76" y1="172.72" x2="241.3" y2="172.72" width="0.1524" layer="91"/>
<label x="241.3" y="172.72" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<label x="96.52" y="81.28" size="1.27" layer="95" xref="yes"/>
<wire x1="96.52" y1="81.28" x2="88.9" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="IOT_49A"/>
</segment>
</net>
<net name="!RAM_CS" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="!CS"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="175.26" y1="35.56" x2="167.64" y2="35.56" width="0.1524" layer="91"/>
<wire x1="167.64" y1="35.56" x2="154.94" y2="35.56" width="0.1524" layer="91"/>
<junction x="167.64" y="35.56"/>
<label x="154.94" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="96.52" y="129.54" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="IOB_25B_G3"/>
<wire x1="96.52" y1="129.54" x2="88.9" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="TIP"/>
<pinref part="U3" gate="G$1" pin="2A"/>
<wire x1="347.98" y1="220.98" x2="340.36" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VIN" class="3">
<segment>
<label x="157.48" y="226.06" size="1.27" layer="95" xref="yes"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="157.48" y1="226.06" x2="144.78" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="185.42" y="226.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="187.96" y1="226.06" x2="185.42" y2="226.06" width="0.1524" layer="91"/>
<wire x1="187.96" y1="215.9" x2="187.96" y2="226.06" width="0.1524" layer="91"/>
<junction x="187.96" y="226.06"/>
<pinref part="IC4" gate="G$1" pin="IN"/>
<wire x1="187.96" y1="226.06" x2="190.5" y2="226.06" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="EN"/>
<wire x1="190.5" y1="226.06" x2="193.04" y2="226.06" width="0.1524" layer="91"/>
<wire x1="193.04" y1="220.98" x2="190.5" y2="220.98" width="0.1524" layer="91"/>
<wire x1="190.5" y1="220.98" x2="190.5" y2="226.06" width="0.1524" layer="91"/>
<junction x="190.5" y="226.06"/>
</segment>
<segment>
<wire x1="259.08" y1="226.06" x2="256.54" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="256.54" y1="215.9" x2="256.54" y2="226.06" width="0.1524" layer="91"/>
<junction x="256.54" y="226.06"/>
<wire x1="256.54" y1="226.06" x2="248.92" y2="226.06" width="0.1524" layer="91"/>
<label x="248.92" y="226.06" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC5" gate="G$1" pin="IN"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="A"/>
<wire x1="373.38" y1="63.5" x2="373.38" y2="66.04" width="0.1524" layer="91"/>
<label x="373.38" y="66.04" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="162.56" y1="241.3" x2="165.1" y2="241.3" width="0.1524" layer="91"/>
<label x="165.1" y="241.3" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="1B"/>
<wire x1="320.04" y1="215.9" x2="314.96" y2="215.9" width="0.1524" layer="91"/>
<label x="314.96" y="215.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="96.52" y="104.14" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="IOT_38B"/>
<wire x1="96.52" y1="104.14" x2="88.9" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="2B"/>
<wire x1="320.04" y1="220.98" x2="314.96" y2="220.98" width="0.1524" layer="91"/>
<label x="314.96" y="220.98" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="88.9" y1="101.6" x2="96.52" y2="101.6" width="0.1524" layer="91"/>
<label x="96.52" y="101.6" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="IOT_39A"/>
</segment>
</net>
<net name="VBUS_DETECT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOB_4A"/>
<wire x1="88.9" y1="170.18" x2="96.52" y2="170.18" width="0.1524" layer="91"/>
<label x="96.52" y="170.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="USB_DET" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="251.46" y1="134.62" x2="241.3" y2="134.62" width="0.1524" layer="91"/>
<label x="241.3" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="88.9" y1="165.1" x2="96.52" y2="165.1" width="0.1524" layer="91"/>
<label x="96.52" y="165.1" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="IOB_6A"/>
</segment>
</net>
<net name="IOB_31B_SPI_D2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOB_31B"/>
<wire x1="88.9" y1="124.46" x2="96.52" y2="124.46" width="0.1524" layer="91"/>
<label x="96.52" y="124.46" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="!WP(D2)"/>
<wire x1="175.26" y1="66.04" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<label x="154.94" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="IC2" gate="G$1" pin="!WP(D2)"/>
<wire x1="160.02" y1="66.04" x2="154.94" y2="66.04" width="0.1524" layer="91"/>
<wire x1="175.26" y1="30.48" x2="160.02" y2="30.48" width="0.1524" layer="91"/>
<wire x1="160.02" y1="30.48" x2="160.02" y2="66.04" width="0.1524" layer="91"/>
<junction x="160.02" y="66.04"/>
</segment>
</net>
<net name="IOB_29B_SPI_D3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="IOB_29B"/>
<wire x1="88.9" y1="127" x2="96.52" y2="127" width="0.1524" layer="91"/>
<label x="96.52" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="!HOLD(D3)"/>
<wire x1="205.74" y1="33.02" x2="220.98" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="!HOLD(D3)"/>
<wire x1="205.74" y1="68.58" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
<label x="236.22" y="68.58" size="1.27" layer="95" xref="yes"/>
<wire x1="220.98" y1="68.58" x2="236.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="220.98" y1="33.02" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
<junction x="220.98" y="68.58"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="335.28" y1="27.94" x2="335.28" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="335.28" y1="30.48" x2="335.28" y2="33.02" width="0.1524" layer="91"/>
<wire x1="335.28" y1="33.02" x2="332.74" y2="33.02" width="0.1524" layer="91"/>
<wire x1="335.28" y1="30.48" x2="337.82" y2="30.48" width="0.1524" layer="91"/>
<junction x="335.28" y="30.48"/>
<pinref part="H301" gate="G$1" pin="MOUNT"/>
</segment>
</net>
<net name="USB0_D_P" class="1">
<segment>
<pinref part="U2" gate="G$1" pin="1B"/>
<pinref part="J1" gate="G$1" pin="3D+"/>
<wire x1="292.1" y1="142.24" x2="312.42" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB1_D_N" class="1">
<segment>
<pinref part="L1" gate="G$1" pin="4"/>
<pinref part="U2" gate="G$1" pin="2A"/>
<wire x1="264.16" y1="149.86" x2="271.78" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17_D_N" class="1">
<segment>
<pinref part="L1" gate="G$1" pin="3"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="243.84" y1="149.86" x2="241.3" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17_D_P" class="1">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="243.84" y1="142.24" x2="241.3" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="170.18" y1="172.72" x2="170.18" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="1A"/>
<wire x1="170.18" y1="175.26" x2="175.26" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2B"/>
<pinref part="IC3" gate="-G1" pin="A"/>
<wire x1="195.58" y1="180.34" x2="210.82" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1B"/>
<pinref part="IC3" gate="-G2" pin="A"/>
<wire x1="195.58" y1="175.26" x2="200.66" y2="175.26" width="0.1524" layer="91"/>
<wire x1="200.66" y1="175.26" x2="200.66" y2="172.72" width="0.1524" layer="91"/>
<wire x1="200.66" y1="172.72" x2="203.2" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="C"/>
<pinref part="R22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="C"/>
<pinref part="R23" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,53.34,187.96,U1,VCC_PLL,N$52,,,"/>
<approved hash="104,1,58.42,187.96,U1,VCC,VCC_1V2,,,"/>
<approved hash="104,1,48.26,187.96,U1,VPP_2V5,VCC_3V3,,,"/>
<approved hash="104,1,33.02,134.62,U1,VCC_IO_1,VCC_3V3,,,"/>
<approved hash="104,1,33.02,88.9,U1,VCC_IO_0,VCC_3V3,,,"/>
<approved hash="104,1,33.02,167.64,U1,VCC_IO_2,VCC_3V3,,,"/>
<approved hash="104,1,205.74,71.12,IC1,VCC,VCC_3V3,,,"/>
<approved hash="104,1,259.08,226.06,IC5,IN,VIN,,,"/>
<approved hash="104,1,279.4,226.06,IC5,OUT,VCC_3V3,,,"/>
<approved hash="104,1,96.52,38.1,LED1,RGND,N$63,,,"/>
<approved hash="104,1,96.52,40.64,LED1,GGND,N$1,,,"/>
<approved hash="104,1,96.52,43.18,LED1,BGND,N$45,,,"/>
<approved hash="104,1,269.24,33.02,IC3-P,VCC,VCC_3V3,,,"/>
<approved hash="104,1,205.74,35.56,IC2,VCC,VCC_3V3,,,"/>
<approved hash="104,1,193.04,226.06,IC4,IN,VIN,,,"/>
<approved hash="104,1,213.36,226.06,IC4,OUT,VCC_1V2,,,"/>
<approved hash="106,1,88.9,170.18,VBUS_DETECT,,,,,"/>
<approved hash="113,1,193.571,130.071,FRAME1,,,,,"/>
<approved hash="113,1,275.59,58.3777,JP2,,,,,"/>
<approved hash="113,1,150.801,225.179,JP1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
