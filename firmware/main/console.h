/*
 * console.h
 *
 * Copyright (C) 2019-2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

void console_init(void);
void console_flush(void);

char getchar(void);
int  getchar_nowait(void);
void putchar(char c);
void puts(const char *p);
int  printf(const char *fmt, ...);
