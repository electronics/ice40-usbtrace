BOARD ?= ice40-usbtrace
CROSS ?= riscv-none-elf-
CC = $(CROSS)gcc
OBJCOPY = $(CROSS)objcopy
ICEPROG = iceprog
DFU_UTIL = dfu-util

TAG_PREFIX = ice40-usbtrace-fw
GITVER = $(shell git describe --match '$(TAG_PREFIX)*' --dirty)
TARGET = $(GITVER)

BOARD_DEFINE=BOARD_$(shell echo $(BOARD) | tr a-z\- A-Z_)
CFLAGS=-Wall -Wextra -Wno-unused-parameter -Os -march=rv32i -mabi=ilp32 -ffreestanding -flto -nostartfiles -fomit-frame-pointer -Wl,--gc-section --specs=nano.specs -D$(BOARD_DEFINE) -I. -I../common
CFLAGS += -DBUILD_INFO="\"$(GITVER) built $(shell date) on $(shell hostname)\""

NO2USB_FW_VERSION=0
include ../../gateware/cores/no2usb/fw/fw.mk
CFLAGS += $(INC_no2usb)

LNK=lnk-app.lds

HEADERS=\
	config.h \
	console.h \
	led.h \
	mini-printf.h \
	misc.h \
	spi.h \
	usb_desc_ids.h \
	usb_tap.h \
	usb_str_app.gen.h \
	utils.h \
	$(NULL)

SOURCES=\
	start.S \
	console.c \
	led.c \
	main.c \
	mini-printf.c  \
	misc.c \
	spi.c \
	usb_desc_app.c \
	usb_tap.c \
	utils.c \
	$(NULL)

HEADERS += $(HEADERS_no2usb)
SOURCES += $(SOURCES_no2usb)


all: $(GITVER).bin $(TAG_PREFIX).bin $(TAG_PREFIX).elf


$(GITVER).elf: $(LNK) $(HEADERS) $(SOURCES)
	$(CC) $(CFLAGS) -Wl,-Bstatic,-T,$(LNK),--strip-debug -o $@ $(SOURCES)


%.bin: %.elf
	$(OBJCOPY) -O binary $< $@

$(TAG_PREFIX).bin: $(GITVER).bin
	ln -sf $< $@

$(TAG_PREFIX).elf: $(GITVER).elf
	ln -sf $< $@

prog: $(GITVER).bin
	$(ICEPROG) -o 640k $<

dfuprog: $(GITVER).bin
ifeq ($(DFU_SERIAL),)
	$(DFU_UTIL) -R -d 1d50:617e,1d50:617d -a 1 -D $<
else
	$(DFU_UTIL) -R -S $(DFU_SERIAL) -a 1 -D $<
endif


clean:
	rm -f *.bin *.hex *.elf *.o *.gen.h

.PHONY: prog dfuprog clean
