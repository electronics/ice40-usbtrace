/*
 * misc.c
 *
 * Copyright (C) 2019-2020  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdbool.h>
#include <stdint.h>

#include "config.h"
#include "misc.h"


struct wb_misc {
	uint32_t csr;
} __attribute__((packed,aligned(4)));

#define MISC_CSR_WARMBOOT_NOW		(1 << 2)
#define MISC_CSR_WARMBOOT_SEL(x)	(x)

static volatile struct wb_misc * const misc_regs = (void*)(MISC_BASE);


void
reboot(int fw)
{
	misc_regs->csr = MISC_CSR_WARMBOOT_NOW | MISC_CSR_WARMBOOT_SEL(fw);
}
